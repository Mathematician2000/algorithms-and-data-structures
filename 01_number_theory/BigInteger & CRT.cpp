#include <algorithm>
#include <deque>
#include <iomanip>
#include <iostream>
#include <limits>
#include <stdexcept>
#include <utility>
#include <vector>

template <typename T>
inline T SafeGet(const std::deque<T>& arr, size_t idx) {
    return (idx < arr.size() ? arr[idx] : T{});
}

// Non-negative (natural) integers
class BigInteger {
    static constexpr int kBase = 1'000'000'000;
    static constexpr int kLog = 9;
    std::deque<int> parts_;

    void RemoveLeadingZeros() {
        while (parts_.size() > 1 && !parts_.back()) {
            parts_.pop_back();
        }
        if (parts_.empty()) {
            parts_.push_back(0);
        }
    }

    BigInteger(const std::deque<int>& parts) : parts_(parts) {
        RemoveLeadingZeros();
    }

    int Size() const {
        return parts_.size();
    }

    int operator[](int idx) const {
        return parts_[idx];
    }

    friend int BinarySearchDigit(const BigInteger& left, const BigInteger& right) {
        int from = 0, to = kBase;
        while (from + 1 < to) {
            int mid = (from + to) / 2;
            if (left < right * mid) {
                to = mid;
            } else {
                from = mid;
            }
        }
        return from;
    }

    void AppendDigit(int digit) {
        parts_.push_front(digit);
        RemoveLeadingZeros();
    }

public:
    BigInteger(int number = 0) {
        while (number >= kBase) {
            parts_.push_back(number % kBase);
            number /= kBase;
        }
        parts_.push_back(number);
    }

    static const BigInteger FromString(const std::string& line) {
        BigInteger integer;
        integer.parts_.clear();

        int size = line.length();
        std::string part;
        for (int idx = size; idx > 0; idx -= kLog) {
            if (idx < kLog) {
                part = line.substr(0, idx);
            } else {
                part = line.substr(idx - kLog, kLog);
            }
            integer.parts_.emplace_back(std::stoi(part));
        }

        integer.RemoveLeadingZeros();
        return integer;
    }

    const std::string ToString() const {
        std::stringstream out;
        int size = Size();
        out << parts_.back() << std::setfill('0');
        for (int idx = size - 2; idx >= 0; --idx) {
            out << std::setw(kLog) << parts_[idx];
        }
        return out.str();
    }

    friend std::istream& operator>>(std::istream& in, BigInteger& integer) {
        std::string line;
        in >> line;
        integer = FromString(line);
        return in;
    }

    friend std::ostream& operator<<(std::ostream& out, const BigInteger& integer) {
        return out << integer.ToString();
    }

    explicit operator bool() const {
        return parts_.back();
    }

    explicit operator int() const {
        if (parts_.size() == 1) {
            return parts_.back();
        }
        int64_t value = parts_[1] * 1LL * kBase + parts_[0];
        if (parts_.size() > 2 || value > std::numeric_limits<int>::max()) {
            throw std::overflow_error("Cannot cast BigInteger to int: the value is too big");
        }
        return value;
    }

    friend bool IsOdd(const BigInteger& integer) {
        return integer.parts_.front() & 1;
    }

    friend const BigInteger operator+(const BigInteger& left, const BigInteger& right) {
        int left_size = left.Size(), right_size = right.Size();
        int out_size = std::max(left_size, right_size);
        std::deque<int> parts(out_size);

        bool carry = false;
        for (int idx = 0; idx < out_size || carry; ++idx) {
            parts[idx] = carry + SafeGet(left.parts_, idx) + SafeGet(right.parts_, idx);
            if ((carry = (parts[idx] >= kBase))) {
                parts[idx] -= kBase;
            }
        }

        return {parts};
    }

    // Assume left >= right
    friend const BigInteger operator-(const BigInteger& left, const BigInteger& right) {
        int right_size = right.Size();
        std::deque<int> parts(left.parts_);

        bool carry = false;
        for (int idx = 0; idx < right_size || carry; ++idx) {
            parts[idx] -= carry + SafeGet(right.parts_, idx);
            if ((carry = (parts[idx] < 0))) {
                parts[idx] += kBase;
            }
        }

        return {parts};
    }

    friend const BigInteger operator*(const BigInteger& left, const BigInteger& right) {
        int left_size = left.Size(), right_size = right.Size();
        int out_size = left_size + right_size;
        std::deque<int> parts(out_size);

        for (int i = 0; i < left_size; ++i) {
            for (int j = 0, carry = 0; j < right_size || carry; ++j) {
                int64_t mult = parts[i + j] + left[i] * 1LL * SafeGet(right.parts_, j) + carry;
                parts[i + j] = mult % kBase;
                carry = mult / kBase;
            }
        }

        return {parts};
    }

    friend auto DivMod(const BigInteger& left, const BigInteger& right) {
        int left_size = left.Size(), right_size = right.Size();
        std::deque<int> first_digits = {left.parts_.end() - right_size + 1, left.parts_.end()};

        BigInteger quotient = 0, remainder(first_digits);
        for (int idx = left_size - right_size; idx >= 0; --idx) {
            remainder.AppendDigit(left[idx]);
            int digit = BinarySearchDigit(remainder, right);
            if (digit) {
                remainder -= right * digit;
            }
            quotient.AppendDigit(digit);
        }

        return std::make_pair(quotient, remainder);
    }

    friend const BigInteger operator/(const BigInteger& left, const BigInteger& right) {
        if (left < right) {
            return 0;
        }

        auto [quotient, remainder] = DivMod(left, right);
        return quotient;
    }

    friend const BigInteger operator%(const BigInteger& left, const BigInteger& right) {
        if (left < right) {
            return left;
        }

        auto [quotient, remainder] = DivMod(left, right);
        return remainder;
    }

    friend const BigInteger operator^(const BigInteger& left, const BigInteger& right) {
        if (!right) {
            return 1;
        }
        if (right == 1) {
            return left;
        }

        BigInteger sqr = (left * left) ^ (right / 2);
        if (IsOdd(right)) {
            sqr *= left;
        }
        return sqr;
    }

    friend const BigInteger PowMod(const BigInteger& left, const BigInteger& right, const BigInteger& mod) {
        if (!right) {
            return 1 % mod;
        }
        if (right == 1) {
            return left % mod;
        }

        BigInteger sqr = PowMod((left * left) % mod, right / 2, mod);
        if (IsOdd(right)) {
            sqr = (sqr * left) % mod;
        }
        return sqr;
    }

    friend bool operator==(const BigInteger& left, const BigInteger& right) {
        return left.parts_ == right.parts_;
    }

    friend bool operator!=(const BigInteger& left, const BigInteger& right) {
        return !(left == right);
    }

    friend bool operator<(const BigInteger& left, const BigInteger& right) {
        int left_size = left.Size(), right_size = right.Size();
        if (left_size != right_size) {
            return left_size < right_size;
        }

        for (int idx = left_size - 1; idx >= 0; --idx) {
            if (left[idx] != right[idx]) {
                return left[idx] < right[idx];
            }
        }

        return false;
    }

    friend bool operator>(const BigInteger& left, const BigInteger& right) {
        return right < left;
    }

    friend bool operator<=(const BigInteger& left, const BigInteger& right) {
        return !(left > right);
    }

    friend bool operator>=(const BigInteger& left, const BigInteger& right) {
        return !(left < right);
    }

    friend BigInteger& operator+=(BigInteger& left, const BigInteger& right) {
        return left = left + right;
    }

    friend BigInteger& operator-=(BigInteger& left, const BigInteger& right) {
        return left = left - right;
    }

    friend BigInteger& operator*=(BigInteger& left, const BigInteger& right) {
        return left = left * right;
    }

    friend BigInteger& operator/=(BigInteger& left, const BigInteger& right) {
        return left = left / right;
    }

    friend BigInteger& operator%=(BigInteger& left, const BigInteger& right) {
        return left = left % right;
    }

    friend BigInteger& operator^=(BigInteger& left, const BigInteger& right) {
        return left = left ^ right;
    }

    friend const BigInteger Sqrt(const BigInteger& integer) {
        BigInteger approx = integer / 2;
        if (!approx) {
            return integer;
        }

        BigInteger mean = (approx + integer / approx) / 2;
        while (mean < approx) {
            approx = mean;
            mean = (approx + integer / approx) / 2;
        }
        return approx;
    }
};

const BigInteger Factorial(int number) {
    BigInteger factorial = 1;
    for (int k = 2; k <= number; ++k) {
        factorial *= k;
    }
    return factorial;
}

bool IsPrime(int number) {
    for (int k = 3; k * k <= number; k += 2) {
        if (!(number % k)) {
            return false;
        }
    }
    return true;
}

inline int AddMod(int left, int right, int mod) {
    int sum = left + right;
    return sum < mod ? sum : sum - mod;
}

inline int DiffMod(int left, int right, int mod) {
    int diff = left - right;
    return diff < 0 ? diff + mod : diff;
}

inline int MultMod(int64_t left, int right, int mod) {
    return (left * right) % mod;
}

int BinPow(int base, int power, int mod) {
    if (power <= 1) {
        return power ? base % mod : 1;
    }

    int sqr = BinPow(MultMod(base, base, mod), power / 2, mod);
    if (power & 1) {
        sqr = MultMod(sqr, base, mod);
    }
    return sqr;
}

class CRT {
    static constexpr int kMods = 100, kLowerModBound = 1'000'000'001;
    static std::vector<int> mods;
    static std::vector<std::vector<int>> invs;

    std::vector<int> rems_;

public:
    static void Initialize() {
        // 100 prime mods near 1e9
        for (int number = kLowerModBound, cnt = 0; cnt < kMods; number += 2) {
            mods.reserve(kMods);
            if (IsPrime(number)) {
                mods.push_back(number);
                ++cnt;
            }
        }

        invs.assign(kMods, std::vector<int>(kMods));
        for (int i = 0; i < kMods; ++i) {
            for (int j = i + 1; j < kMods; ++j) {
                invs[i][j] = BinPow(mods[i], mods[j] - 2, mods[j]);
            }
        }
    }

    CRT() : rems_(kMods) {
    }

    explicit CRT(const BigInteger& integer) {
        rems_.reserve(kMods);
        for (auto mod : mods) {
            rems_.emplace_back(integer % mod);
        }
    }

    friend std::istream& operator>>(std::istream& in, CRT& crt) {
        BigInteger integer;
        in >> integer;
        crt = CRT(integer);
        return in;
    }

    friend std::ostream& operator<<(std::ostream& out, const CRT& crt) {
        return out << crt.ToInteger();
    }

    friend const CRT operator+(const CRT& left, const CRT& right) {
        CRT sum;
        for (int idx = 0; idx < kMods; ++idx) {
            sum.rems_[idx] = AddMod(left.rems_[idx], right.rems_[idx], mods[idx]);
        }
        return sum;
    }

    friend const CRT operator-(const CRT& left, const CRT& right) {
        CRT diff;
        for (int idx = 0; idx < kMods; ++idx) {
            diff.rems_[idx] = DiffMod(left.rems_[idx], right.rems_[idx], mods[idx]);
        }
        return diff;
    }

    friend const CRT operator*(const CRT& left, const CRT& right) {
        CRT prod;
        for (int idx = 0; idx < kMods; ++idx) {
            prod.rems_[idx] = MultMod(left.rems_[idx], right.rems_[idx], mods[idx]);
        }
        return prod;
    }

    operator bool() const {
        for (auto rem : rems_) {
            if (rem) {
                return true;
            }
        }
        return false;
    }

    // Garner's algorithm
    const BigInteger ToInteger() const {
        BigInteger result = 0, prod = 1;
        std::vector<int> xs(kMods);
        for (int i = 0; i < kMods; ++i) {
            xs[i] = rems_[i];
            for (int j = 0; j < i; ++j) {
                xs[i] = MultMod(DiffMod(xs[i], xs[j], mods[i]), invs[j][i], mods[i]);
            }
            result += prod * xs[i];
            prod *= mods[i];
        }
        return result;
    }
};

std::vector<int> CRT::mods;
std::vector<std::vector<int>> CRT::invs;

int main() {
    CRT::Initialize();

    // Case 1: basic BigInteger operations
    {
        BigInteger left, right;
        std::cin >> left >> right;

        std::cout << left << " + " << right << " = " << left + right << '\n';
        if (left < right) {
            std::cout << left << " < " << right << '\n';
        } else {
            std::cout << left << " - " << right << " = " << left - right << '\n';
        }
        std::cout << left << " * " << right << " = " << left * right << '\n';

        auto [quotient, remainder] = DivMod(left, right);  // more efficient than using / and % separately
        std::cout << left << " / " << right << " = " << quotient << '\n';
        std::cout << left << " % " << right << " = " << remainder << "\n\n";
    }

    // Case 2: binary exponentiation
    {
        BigInteger left, right;
        std::cin >> left >> right;

        std::cout << left << " ^ " << right << " = " << (left ^ right) << "\n\n";
    }

    // Case 3: binary exponentiation + modulo
    {
        BigInteger left, right, mod;
        std::cin >> left >> right >> mod;

        std::cout << left << " ^ " << right << " = " << PowMod(left, right, mod) << " (mod " << mod << ")\n\n";
    }

    // Case 4: Newton's method for square root function
    {
        BigInteger integer;
        std::cin >> integer;

        std::cout << "sqrt(" << integer << ") = " << Sqrt(integer) << "\n\n";
    }

    // Case 5: factorial
    {
        int fact;
        std::cin >> fact;

        std::cout << fact << "! = " << Factorial(fact) << "\n\n";
    }

    // Case 6: Garner's algorithm based on Chinese Remainder Theorem
    {
        CRT left, right;
        std::cin >> left >> right;

        std::cout << left << " + " << right << " = " << left + right << '\n';
        std::cout << left << " - " << right << " = " << left - right << '\n';
        std::cout << left << " * " << right << " = " << left * right << '\n';
    }

    return 0;
}
