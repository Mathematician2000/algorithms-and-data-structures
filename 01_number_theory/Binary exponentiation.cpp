#include <iostream>

// Binary exponentiation: O(log_2 power)
int BinPow(int base, int power, int mod = 0) {
    if (!power) {
        return 1;
    }
    if (power == 1) {
        return mod ? base % mod : base;
    }
    int sqr = BinPow(base, power / 2, mod);
    sqr = mod ? (sqr * sqr) % mod : sqr * sqr;
    if (power & 1) {
        sqr = mod ? (sqr * base) % mod : sqr * base;
    }
    return sqr;
}

// Binary addition: O(log_2 mult)
int BinSum(int base, int mult, int mod = 0) {
    if (!mult) {
        return 0;
    }
    if (mult == 1) {
        return mod ? base % mod : base;
    }
    int sqr = BinSum(base, mult / 2, mod);
    sqr = mod ? (sqr + sqr) % mod : sqr + sqr;
    if (mult & 1) {
        sqr = mod ? (sqr + base) % mod : sqr + base;
    }
    return sqr;
}

int main() {
    // Case #1: calculate x^n
    {
        int x, n;
        std::cin >> x >> n;
        std::cout << BinPow(x, n) << '\n';
    }

    // Case #2: calculate x^n (mod m)
    {
        int x, n, m;
        std::cin >> x >> n >> m;
        std::cout << BinPow(x, n, m) << '\n';
    }

    // Case #3: calculate x * n
    {
        int x, n;
        std::cin >> x >> n;
        std::cout << BinSum(x, n) << '\n';
    }

    // Case #4: calculate x * n (mod m)
    {
        int x, n, m;
        std::cin >> x >> n >> m;
        std::cout << BinSum(x, n, m) << '\n';
    }

    return 0;
}
