#include <algorithm>
#include <cmath>
#include <iostream>
#include <unordered_map>
#include <vector>

constexpr int kNone = -1;

template <typename K, typename V>
using HashMap = std::unordered_map<K, V>;

// Euclidean algorithm: O(log min{a, b})
inline int GCD(int a, int b) {
    if (a < b) {
        std::swap(a, b);
    }
    while (b) {
        a %= b;
        std::swap(a, b);
    }
    return a;
}

// Binary exponentiation: O(log_2 power)
int BinPow(int base, int power, int mod) {
    if (power <= 1) {
        return power ? base % mod : 1;
    }

    int sqr = BinPow((base * 1LL * base) % mod, power / 2, mod);
    if (power & 1) {
        sqr = (sqr * 1LL * base) % mod;
    }
    return sqr;
}

// Shanks algorithm (baby-step-giant-step): O(sqrt(mod))
// Solve equation: a^x = b (mod m)
int DiscreteLogarithm(int a, int b, int mod) {
    a %= mod, b %= mod;

    int left = 1, add = 0, gcd;
    while ((gcd = GCD(a, mod)) > 1) {
        if (left == b) {
            return add;
        }
        if (b % gcd) {
            return kNone;
        }

        b /= gcd, mod /= gcd;
        ++add;
        left = (left * 1LL * a / gcd) % mod;
    }

    int root = std::sqrt(mod) + 1;
    HashMap<int, int> rights;
    for (int right = b, q = 0; q <= root; ++q) {
        rights[right] = q;
        right = (right * 1LL * a) % mod;
    }

    int power = BinPow(a, root, mod);
    for (int p = 1; p <= root; ++p) {
        left = (left * 1LL * power) % mod;
        auto iter = rights.find(left);
        if (iter != rights.end()) {
            return p * root - iter->second + add;
        }
    }

    return kNone;
}

// Find all prime divisors of n: O(sqrt(n))
auto GetPrimes(int n) {
    std::vector<int> primes;
    if (!(n & 1)) {
        primes.push_back(2);
        do {
            n /= 2;
        } while (!(n & 1));
    }

    for (int k = 3; k * k <= n; k += 2) {
        if (!(n % k)) {
            primes.push_back(k);
            do {
                n /= k;
            } while (!(n % k));
        }
    }

    if (n > 1) {
        primes.push_back(n);
    }
    return primes;
}

// Find any generator (primitive root) modulo n: O(ans * log^2 n)
// for all a s.t. (a, n) = 1 there exists k s.t. g^k = a (mod n) [+ assuming n is prime]
/*
* PS: phi function is needed for composite n
* PPS: n can be any of these numbers: 1, 2, 4, q^k, 2 * q^k for odd prime q
* PPPS: there are phi(phi(n)) generators
*/
int PrimitiveRoot(int n) {
    int phi = n - 1;  // O(sqrt(n))!
    auto primes = GetPrimes(phi);  // O(sqrt(phi))!
    for (int gen = 1; gen <= n; ++gen) {
        bool ok = true;
        for (auto p : primes) {
            if (BinPow(gen, phi / p, n) == 1) {
                ok = false;
                break;
            }
        }
        if (ok) {
            return gen;
        }
    }
    return kNone;
}

// Find any discrete root assuming modulo is prime: O(sqrt(mod))
// Solve equation: x^k = a (mod m)
int DiscreteRoot(int a, int k, int mod) {
    int gen = PrimitiveRoot(mod);
    int power = BinPow(gen, k, mod);
    int log = DiscreteLogarithm(power, a, mod);
    return BinPow(gen, log, mod);
}

int main() {
    // Case 1: discrete logarithm
    {
        int a, b, mod;
        std::cin >> a >> b >> mod;

        int log = DiscreteLogarithm(a, b, mod);
        if (log == kNone) {
            std::cout << "No solution :(\n\n";
        } else {
            std::cout << a << " ^ " << log << " = " << b << " (mod " << mod << ")\n\n";
        }
    }

    // Case 2: primitive root
    {
        int n;
        std::cin >> n;

        std::cout << PrimitiveRoot(n) << "\n\n";
    }

    // Case 3: discrete root
    {
        int a, k, mod;
        std::cin >> a >> k >> mod;

        int root = DiscreteRoot(a, k, mod);
        std::cout << root << " ^ " << k << " = " << a << " (mod " << mod << ")\n";
    }

    return 0;
}
