#include <iostream>
#include <utility>
#include <vector>

// Eratosthenes sieve for [1; n]: O(n log log n)
auto Sieve(int n) {
    std::vector<int> primes = {2};  // assume n >= 2
    std::vector<bool> is_prime(n + 1);
    is_prime[2] = true;
    for (int k = 3; k <= n; k += 2) {
        is_prime[k] = true;
    }
    for (int k = 3; k <= n; k += 2) {
        if (is_prime[k]) {
            primes.push_back(k);
            for (int64_t j = k * 1LL * k; j <= n; j += 2 * k) {
                is_prime[j] = false;
            }
        }
    }
    return std::make_pair(primes, is_prime);
}

// Linear sieve for [1; n]: O(n)
auto LinearSieve(int n) {
    std::vector<int> primes, least_prime(n + 1);
    for (int k = 2; k <= n; ++k) {
        if (!least_prime[k]) {
            least_prime[k] = k;
            primes.push_back(k);
        }
        for (auto p : primes) {
            if (p > least_prime[k] || p * k > n) {
                break;
            }
            least_prime[p * k] = p;
        }
    }
    return std::make_pair(primes, least_prime);
}

// Factorization using least_prime[] from linear sieve: O(|divs|)
auto Factorize(int n, const std::vector<int>& least_prime) {
    std::vector<int> divs;
    while (n != 1) {
        divs.push_back(least_prime[n]);
        n /= least_prime[n];
    }
    return divs;
}

int main() {
    // Case #1: build sieve for [1; n] in O(n log log n)
    {
        int n;
        std::cin >> n;  // n >= 2
        auto [primes, is_prime] = Sieve(n);
        std::cout << "primes: ";
        for (auto p : primes) {
            std::cout << p << ' ';
        }
        std::cout << '\n';
        for (int k = 2; k <= n; ++k) {
            std::cout << k << '\t';
        }
        std::cout << '\n';
        for (int k = 2; k <= n; ++k) {
            std::cout << is_prime[k] << '\t';
        }
        std::cout << "\n\n";
    }

    // Case #2: build sieve for [1; n] in O(n) + find least prime factors
    {
        int n;
        std::cin >> n;  // n >= 2
        auto [primes, least_prime] = LinearSieve(n);
        std::cout << "primes: ";
        for (auto p : primes) {
            std::cout << p << ' ';
        }
        std::cout << '\n';
        for (int k = 2; k <= n; ++k) {
            std::cout << k << '\t';
        }
        std::cout << '\n';
        for (int k = 2; k <= n; ++k) {
            std::cout << least_prime[k] << '\t';
        }
        std::cout << "\n\n";
    }

    // Case #3: build sieve for [1; n] in O(n) + Factorize arbitrary k \in [1; n]
    {
        int n, k;
        std::cin >> n >> k;  // n >= max(2, k)
        auto [primes, least_prime] = LinearSieve(n);
        auto divs = Factorize(k, least_prime);
        for (auto d : divs) {
            std::cout << d << ' ';
        }
    }

    return 0;
}
