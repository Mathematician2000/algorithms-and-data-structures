#include <iostream>

// Euclidean algorithm: O(log min{a, b})
int GCD(int a, int b) {
    if (a < b) {
        std::swap(a, b);
    }
    while (b) {
        a %= b;
        std::swap(a, b);
    }
    return a;
}

// Bezout's identity: ax + by = 1, O(log min{a, b})
int BezoutGCD(int a, int b, int& x, int& y) {
    if (!a) {
        x = 0, y = 1;
        return b;
    }
    int x1, y1;
    int gcd = BezoutGCD(b % a, a, x1, y1);
    x = y1 - x1 * (b / a), y = x1;
    return gcd;
}

int main() {
    // Case #1: find GCD
    {
        int a, b;
        std::cin >> a >> b;
        int gcd = GCD(a, b);
        std::cout << gcd << '\n';
    }

    // Case #2: find LCM
    {
        int a, b;
        std::cin >> a >> b;
        int gcd = GCD(a, b);
        std::cout << a / gcd * b << '\n';
    }

    // Case #3: solve Bezout's identity
    {
        int a, b, x, y;
        std::cin >> a >> b;
        int gcd = BezoutGCD(a, b, x, y);
        printf("%d * a + %d * b = %d\n", x, y, gcd);
    }

    // Case #4: find all solutions of Bezout's identity
    {
        int a, b, x, y;
        std::cin >> a >> b;
        int gcd = BezoutGCD(a, b, x, y);
        printf("(%d + k * %d) * a + (%d - k * %d) * b = %d\n", x, b / gcd, y, a / gcd, gcd);
    }

    return 0;
}
