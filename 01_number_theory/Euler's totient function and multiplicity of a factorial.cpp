#include <iostream>
#include <utility>
#include <vector>

// Euler's totient function: O(sqrt(n))
int Phi(int n) {
    int res = n;
    if (!(n & 1)) {
        res -= res / 2;
        do {
            n >>= 1;
        } while (!(n & 1));
    }
    for (int k = 3; k * k <= n; k += 2) {
        if (!(n % k)) {
            res -= res / k;
            do {
                n /= k;
            } while (!(n % k));
        }
    }
    if (n > 1) {
        res -= res / n;
    }
    return res;
}

// Factorization: O(sqrt(n))
auto Factorize(int n) {
    std::vector<std::pair<int, int>> divs;
    if (!(n & 1)) {
        int k = 0;
        do {
            ++k, n >>= 1;
        } while (!(n & 1));
        divs.emplace_back(2, k);
    }
    for (int d = 3; d * d <= n; d += 2) {
        if (!(n % d)) {
            int k = 0;
            do {
                ++k, n /= d;
            } while (!(n % d));
            divs.emplace_back(d, k);
        }
    }
    if (n > 1) {
        divs.emplace_back(n, 1);
    }
    return divs;
}

// Binary exponentiation
int BinPow(int a, int n) {
    if (!n) {
        return 1;
    }
    if (n == 1) {
        return a;
    }
    int sqr = BinPow(a, n / 2);
    sqr *= sqr;
    if (n & 1) {
        sqr *= a;
    }
    return sqr;
}

// Multiplicity of p in n! (Legendre's formula): O(log_p n)
int FactPow(int n, int p) {
    int ans = 0;
    while (n) {
        n /= p;
        ans += n;
    }
    return ans;
}

int main() {
    // Case #1: calculate phi(n)
    {
        int n;
        std::cin >> n;
        std::cout << Phi(n) << "\n\n";
    }

    // Case #2: calculate phi(n) using prime factors
    {
        int n;
        std::cin >> n;
        auto primes = Factorize(n);
        for (auto [p, k] : primes) {
            printf("%d^%d ", p, k);
        }
        std::cout << '\n';
        int phi = 1;
        for (auto [p, k] : primes) {
            phi *= (p - 1) * BinPow(p, k - 1);
        }
        std::cout << phi << "\n\n";
    }

    // Case #3: find number of trailing zeroes of n! in decimal number system
    {
        int n;
        std::cin >> n;
        std::cout << FactPow(n, 5) << "\n\n";  // 10 == 2 * 5 && FactPow(n, 2) >= FactPow(n, 5)
    }

    return 0;
}
