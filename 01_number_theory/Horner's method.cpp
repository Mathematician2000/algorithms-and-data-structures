#include <iostream>
#include <vector>

// Horner's method: O(n)
int Horner(const std::vector<int>& poly, int x) {
    int n = poly.size() - 1, res = poly[0];
    for (int i = 1; i <= n; ++i) {
        res = res * x + poly[i];
    }
    return res;
}

int main() {
    // Case #1: calculate polynomial f(x) = a0 * x^n + a1 * x^(n - 1) + a2 * x^(n - 2) + ... + an
    {
        int n, x;
        std::cin >> n;
        std::vector<int> poly(n + 1);
        for (int i = 0; i <= n; ++i) {
            std::cin >> poly[i];
        }
        std::cin >> x;
        printf("f(%d) = %d\n", x, Horner(poly, x));
    }

    return 0;
}
