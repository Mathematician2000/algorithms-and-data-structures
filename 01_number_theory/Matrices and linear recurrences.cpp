#include <iostream>
#include <stdexcept>
#include <vector>

class Matrix {
    std::vector<std::vector<int>> matrix_;
    int rows_, cols_;

public:
    // Zero matrix
    Matrix(int rows, int cols) : matrix_(rows, std::vector<int>(cols)), rows_(rows), cols_(cols) {
    }

    // Identity matrix
    explicit Matrix(int size) : Matrix(size, size) {
        for (int i = 0; i < size; ++i) {
            matrix_[i][i] = 1;
        }
    }

    Matrix(const std::vector<std::vector<int>>& matrix)
        : matrix_(matrix), rows_(matrix.size()), cols_(matrix.front().size()) {
    }

    int NumRows() const {
        return rows_;
    }

    int NumCols() const {
        return cols_;
    }

    friend std::istream& operator>>(std::istream& in, Matrix& matrix) {
        for (int i = 0; i < matrix.NumRows(); ++i) {
            for (int j = 0; j < matrix.NumCols(); ++j) {
                in >> matrix.matrix_[i][j];
            }
        }
        return in;
    }

    friend std::ostream& operator<<(std::ostream& out, const Matrix& matrix) {
        for (int i = 0; i < matrix.NumRows(); ++i) {
            out << '|' << matrix.matrix_[i][0];
            for (int j = 1; j < matrix.NumCols(); ++j) {
                out << '\t' << matrix.matrix_[i][j];
            }
            out << "|\n";
        }
        return out;
    }

    std::vector<int>& operator[](size_t idx) {
        return matrix_[idx];
    }

    const std::vector<int>& operator[](size_t idx) const {
        return matrix_[idx];
    }

    friend Matrix operator%(const Matrix& matrix, int mod) {
        Matrix res = matrix;
        if (mod) {
            for (int i = 0; i < matrix.NumRows(); ++i) {
                for (int j = 0; j < matrix.NumCols(); ++j) {
                    res[i][j] %= mod;
                }
            }
        }
        return res;
    }

    friend Matrix Mult(const Matrix& left, const Matrix& right, int mod = 0) {
        if (left.NumCols() != right.NumRows()) {
            throw std::invalid_argument("Matrices should have corresponding shapes");
        }
        int n = left.NumRows(), m = left.NumCols(), k = right.NumCols();
        Matrix res(n, k);  // (n x m) * (m x k) => (n x k)
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < k; ++j) {
                for (int p = 0; p < m; ++p) {
                    res[i][j] += left[i][p] * right[p][j];  // overflow danger!
                }
            }
        }
        return res % mod;
    }

    friend Matrix BinPow(const Matrix& matrix, int power, int mod = 0) {
        int size = matrix.NumRows();
        if (size != matrix.NumCols()) {
            throw std::invalid_argument("Only square matrices can be exponentiated\n");
        }
        if (!power) {
            return Matrix(size);
        }
        if (power == 1) {
            return matrix % mod;
        }
        Matrix res = BinPow(matrix, power / 2, mod);
        res = Mult(res, res, mod);
        if (power & 1) {
            res = Mult(res, matrix, mod);
        }
        return res;
    }
};

int main() {
    // Case #1: calculate A * B, B * A
    {
        int n, m, k;
        std::cin >> n >> m >> k;
        Matrix A(n, m), B(m, k);
        std::cin >> A >> B;
        std::cout << Mult(A, B) << '\n' << Mult(B, A) << '\n';
    }

    // Case #2: calculate A * B (mod p)
    {
        int n, m, k, p;
        std::cin >> n >> m >> k >> p;
        Matrix A(n, m), B(m, k);
        std::cin >> A >> B;
        std::cout << Mult(A, B, p) << '\n';
    }

    // Case #3: calculate A ^ k
    {
        int n, k;
        std::cin >> n >> k;
        Matrix A(n);
        std::cin >> A;
        std::cout << BinPow(A, k) << '\n';
    }

    // Case #4: calculate A ^ k (mod p)
    {
        int n, k, p;
        std::cin >> n >> k >> p;
        Matrix A(n);
        std::cin >> A;
        std::cout << BinPow(A, k, p) << '\n';
    }

    // Case #5: find n-th Fibonacci number modulo 10^9 + 7
    {
        int n, mod = 1'000'000'007;
        std::cin >> n;
        Matrix A = {{
            {0, 1},
            {1, 1}
        }};
        Matrix b = {{
            {0},
            {1}
        }};
        std::cout << Mult(BinPow(A, n, mod), b, mod)[0][0] << '\n';
    }

    return 0;
}
