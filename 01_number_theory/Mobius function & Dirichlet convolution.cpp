#include <algorithm>
#include <cmath>
#include <iostream>
#include <unordered_map>
#include <vector>

template <typename K, typename V>
using HashMap = std::unordered_map<K, V>;

// Square function: O(1)
inline int64_t Sqr(int64_t n) {
    return n * n;
}

// Binomial coefficient C_n^2: O(1)
inline int64_t Cn2(int64_t n) {
    return n * (n - 1) / 2;
}

// Mobius function: O(sqrt(n))
int Mobius(int n) {
    int ans = 1;
    while (!(n % 2)) {
        n /= 2;
        ans = -ans;
        if (!(n % 2)) {
            return 0;
        }
    }

    for (int p = 3; p * p <= n; p += 2) {
        while (!(n % p)) {
            n /= p;
            ans = -ans;
            if (!(n % p)) {
                return 0;
            }
        }
    }

    if (n != 1) {
        ans = -ans;
    }
    return ans;
}

// Least prime factors of all integers in range[2..n]: O(n)
auto LinearSieve(int n) {
    std::vector<int> primes, least_prime(n + 1);
    least_prime[1] = 1;
    for (int k = 2; k <= n; ++k) {
        if (!least_prime[k]) {
            least_prime[k] = k;
            primes.push_back(k);
        }
        for (auto prime : primes) {
            if (prime > least_prime[k] || prime * k > n) {
                break;
            }
            least_prime[prime * k] = prime;
        }
    }
    return least_prime;
}

// Sieve-like computation of Mobius function for all integers in range [1..n]: O(n)
auto PrefixMobius(int n) {
    auto least_prime = LinearSieve(n);
    std::vector<int> mobius(n + 1);
    mobius[1] = 1;
    for (int k = 2; k <= n; ++k) {
        int prime = least_prime[k];
        if (least_prime[k / prime] == prime) {
            mobius[k] = 0;
        } else {
            mobius[k] = -mobius[k / prime];
        }
    }
    return mobius;
}

// Sieve-like computation of phi function for all integers in range [1..n]: O(n)
auto PrefixPhi(int n) {
    auto least_prime = LinearSieve(n);
    std::vector<int> phi(n + 1);
    phi[1] = 1;
    for (int k = 2; k <= n; ++k) {
        int prime = least_prime[k];
        phi[k] = phi[k / prime];
        if (least_prime[k / prime] != prime) {
            phi[k] *= prime - 1;
        } else {
            phi[k] *= prime;
        }
    }
    return phi;
}

// Sieve-like computation of h(n) = sum_{k|n} mu(k) * nk: O(n)
auto LcmMultFunc(int n) {
    auto least_prime = LinearSieve(n);
    std::vector<int64_t> func(n + 1);
    func[1] = 1;
    for (int k = 2; k <= n; ++k) {
        int prime = least_prime[k];
        func[k] = func[k / prime] * prime;
        if (least_prime[k / prime] != prime) {
            func[k] *= 1 - prime;
        }
    }
    return func;
}

// Cache-based optimization for partial sums of Mobius function in range [1..n]: O(x^{2/3}) for query
// s_f(n) = (s_{f*g}(n) - sum_{d=2}^n s_f(n/d) * g(d)) / g(1)
class CachedPrefixMobius {
    HashMap<int64_t, int64_t> cache_;
    std::vector<int64_t> prefix_;
    int64_t threshold_;

public:
    CachedPrefixMobius(int64_t n, int64_t threshold = 0) : threshold_(threshold) {
        if (!threshold_) {
            threshold_ = Sqr(std::cbrt(n));
        }

        prefix_.assign(threshold_ + 1, 0);
        auto mobius = PrefixMobius(threshold_);
        for (int64_t k = 1; k <= threshold_; ++k) {
            prefix_[k] = prefix_[k - 1] + mobius[k];
        }
    }

    int64_t Calculate(int64_t x) {
        if (x <= threshold_) {
            return prefix_[x];
        }
        auto iter = cache_.find(x);
        if (iter != cache_.end()) {
            return iter->second;
        }

        int64_t ans = 0;
        for (int64_t from = 2, to; from <= x; from = to + 1) {
            to = x / (x / from);
            ans += (to - from + 1) * Calculate(x / from);
        }
        return cache_[x] = 1 - ans;
    }

    int64_t operator()(int64_t x) {
        return Calculate(x);
    }
};

int main() {
    // Case 1: compute Mobius function mu(n): O(sqrt(n))
    {
        int n;
        std::cin >> n;

        std::cout << "Mu(" << n << ") = " << Mobius(n) << "\n\n";
    }

    // Case 2: find mu(k) for all k in range [1..n]: O(n)
    {
        int n;
        std::cin >> n;

        auto mobius = PrefixMobius(n);

        for (int k = 1; k <= n; ++k) {
            std::cout << "Mu(" << k << ") = " << mobius[k] << '\n';
        }
        std::cout << '\n';
    }

    // Case 3: find phi(k) for all k in range [1..n]: O(n)
    {
        int n;
        std::cin >> n;

        auto phi = PrefixPhi(n);

        for (int k = 1; k <= n; ++k) {
            std::cout << "Phi(" << k << ") = " << phi[k] << '\n';
        }
        std::cout << '\n';
    }

    // Case 4: find number of coprime pairs of integers in range [1..n]: O(n)
    {
        int n;
        std::cin >> n;

        auto mobius = PrefixMobius(n);

        int64_t ans = 0;
        for (int64_t d = 1; d <= n; ++d) {
            ans += mobius[d] * Sqr(n / d);
        }
        std::cout << ans << "\n\n";
    }

    // Case 5: find sum of GCD for all pairs of integers in range [1..n]: O(n)
    {
        int n;
        std::cin >> n;

        auto phi = PrefixPhi(n);

        int64_t ans = 0;
        for (int64_t d = 1; d <= n; ++d) {
            ans += phi[d] * Sqr(n / d);
        }
        std::cout << ans << "\n\n";
    }

    // Case 6: find sum of LCM for all pairs of integers in range [1..n]: O(n)
    {
        int n;
        std::cin >> n;

        auto lcm_mult_func = LcmMultFunc(n);

        int64_t ans = 0;
        for (int64_t d = 1; d <= n; ++d) {
            ans += Sqr(Cn2(n / d + 1)) * lcm_mult_func[d];  // overflow danger for n > 77'935
        }
        std::cout << ans << "\n\n";
    }

    // Case 7: optimization for partial sum calculation of Mobius function: O(x^{2/3}) for query
    {
        int64_t n, qs, x;
        std::cin >> n >> qs;

        CachedPrefixMobius cache(n);
        while (qs--) {
            std::cin >> x;  // <= n
            std::cout << cache(x) << '\n';
        }
    }

    return 0;
}
