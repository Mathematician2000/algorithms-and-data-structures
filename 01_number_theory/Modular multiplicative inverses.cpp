#include <iostream>
#include <vector>

// Euclidean algorithm & Bezout's identity
int BezoutGCD(int a, int b, int& x, int& y) {
    if (!a) {
        x = 0, y = 1;
        return b;
    }
    int x1, y1;
    int gcd = BezoutGCD(b % a, a, x1, y1);
    x = y1 - x1 * (b / a), y = x1;
    return gcd;
}

// Euler's totient function
int Phi(int n) {
    int res = n;
    if (!(n & 1)) {
        res -= res / 2;
        while (!(n & 1)) {
            n >>= 1;
        }
    }
    for (int p = 3; p * p <= n; p += 2) {
        if (!(n % p)) {
            res -= res / p;
            while (!(n % p)) {
                n /= p;
            }
        }
    }
    if (n > 1) {
        res -= res / n;
    }
    return res;
}

// Binary exponentiation
int BinPow(int base, int power, int mod) {
    if (!power) {
        return 1;
    }
    if (power == 1) {
        return base % mod;
    }
    int sqr = BinPow(base, power / 2, mod);
    sqr = (sqr * sqr) % mod;
    if (power & 1) {
        sqr = (sqr * base) % mod;
    }
    return sqr;
}

int main() {
    // Case #1: calculate a^-1 (mod m) - extended Euclidean algorithm
    {
        int a, m, x, y;
        std::cin >> a >> m;  // a and m should be coprime!
        BezoutGCD(a, m, x, y);
        std::cout << (x % m + m) % m << '\n';
    }

    // Case #2: calculate a^-1 (mod m) - Euler's totient function & binary exponentiation
    {
        int a, m;
        std::cin >> a >> m;
        std::cout << BinPow(a, Phi(m) - 1, m) << '\n';
    }

    // Case #3: calculate a^-1 (mod p) - Fermat's little theorem & binary exponentiation
    {
        int a, p;
        std::cin >> a >> p;  // p should be prime!
        std::cout << BinPow(a, p - 2, p) << '\n';
    }

    // Case #4: calculate i^-1 (mod p) for i \in [1; p - 1] in O(p)
    {
        int p;
        std::cin >> p;  // p should be prime!
        std::vector<int> inv(p);
        inv[1] = 1;
        std::cout << "inv[1] = 1\n";
        for (int k = 2; k < p; ++k) {
            inv[k] = (p - (inv[p % k] * (p / k)) % p) % p;
            printf("inv[%d] = %d\n", k, inv[k]);
        }
    }

    return 0;
}
