#include <algorithm>
#include <iostream>
#include <random>
#include <utility>
#include <vector>

// Simple primality test (trial divisions): O(sqrt(n))
bool IsPrime(int n) {
    if (n == 2) {
        return true;
    }
    if (!(n & 1)) {
        return false;
    }
    for (int k = 3; k * k <= n; k += 2) {
        if (!(n % k)) {
            return false;
        }
    }
    return true;
}

// Factorization in O(sqrt(n))
auto Factorize(int n) {
    std::vector<int> prime_divs;
    while (!(n & 1)) {
        prime_divs.push_back(2);
        n >>= 1;
    }
    for (int k = 3; k * k <= n; k += 2) {
        while (!(n % k)) {
            prime_divs.push_back(k);
            n /= k;
        }
    }
    if (n > 1) {
        prime_divs.push_back(n);
    }
    return prime_divs;
}

// Find all divisors: O(sqrt(n))
auto GetDivs(int n) {
    std::vector<int> divs;
    int k;
    for (k = 1; k * k < n; ++k) {
        if (!(n % k)) {
            divs.push_back(k);
            divs.push_back(n / k);
        }
    }
    if (k * k == n) {
        divs.push_back(k);
    }
    return divs;
}

// Euclidean algorithm: O(log min{a, b})
int GCD(int a, int b) {
    if (a < b) {
        std::swap(a, b);
    }
    while (b) {
        a %= b;
        std::swap(a, b);
    }
    return a;
}

// Binary exponentiation: O(log_2 n)
int BinPow(int base, int power, int mod) {
    if (!power) {
        return 1;
    }
    if (power == 1) {
        return base % mod;
    }
    int sqr = BinPow(base, power / 2, mod);
    sqr = (sqr * sqr) % mod;
    if (power & 1) {
        sqr = (sqr * base) % mod;
    }
    return sqr;
}

// Random numbers generator for range [from, to)
// Consider using std::mt19937 instead
inline int GetRand(int from, int to) {
    return from + rand() / (RAND_MAX + .0) * (to - from);
}

// Fermat's primality test
bool Fermat(int n) {
    if (n == 2) {
        return true;
    }
    static constexpr int MAX_ATTEMPTS = 100;
    std::mt19937 gen;  // warning: a heavy object, consider having no copies of this
    std::uniform_int_distribution<int> distr(1, n - 1);  // [1; n - 1]
    for (int i = 0; i < MAX_ATTEMPTS; ++i) {
        int div = distr(gen);  // old way: GetRand(1, n)
        if (BinPow(div, n - 1, n) != 1) {
            return false;
        }
    }
    return true;
}

// Rho-Pollard algorithm (splitting)
auto Pollard(int n) {
    if (!(n & 1)) {
        return std::make_pair(2, n / 2);
    }

    static constexpr int MAX_TRIVIAL_BOUND = 20;  // ~1e12
    if (n <= MAX_TRIVIAL_BOUND) {
        for (int k = 3; k * k <= n; k += 2) {
            if (!(n % k)) {
                return std::make_pair(k, n / k);
            }
        }
        return std::make_pair(1, n);  // exhaustive search failed
    }

    auto func = [](int x, int n) { return ((x * x) % n + n - 1) % n; };
    // consider using std::mt19937 instead
    // using GetRand here for example purposes
    int x = GetRand(1, n - 1), y = 1;
    int iter = 0, stage = 2, div = GCD(n, std::abs(x - y));
    while (div == 1 || div == n) {
        if (iter++ == stage) {
            y = x, stage <<= 1;
        }
        x = func(x, n);
        div = GCD(n, std::abs(x - y));
    }
    return std::make_pair(div, n / div);
}

int main() {
    // Case #1: check whether n is prime
    {
        int n;
        std::cin >> n;  // n >= 2
        std::cout << (IsPrime(n) ? "prime\n\n" : "composite\n\n");
    }

    // Case #2: factorize n (into prime factors)
    {
        int n;
        std::cin >> n;
        auto divs = Factorize(n);
        for (auto d : divs) {
            std::cout << d << ' ';
        }
        std::cout << "\n\n";
    }

    // Case #3: find all divisors of n
    {
        int n;
        std::cin >> n;
        auto divs = GetDivs(n);
        for (auto d : divs) {
            std::cout << d << ' ';
        }
        std::cout << "\n\n";
    }

    // Case #4: check whether n is prime (Fermat's test)
    {
        int n;
        std::cin >> n;  // n >= 2
        std::cout << (Fermat(n) ? "prime\n\n" : "composite\n\n");  // overflow danger!
    }

    // Case #5: split n into 2 factors (Rho-Pollard algorithm)
    {
        int n;
        std::cin >> n;
        auto [p, q] = Pollard(n);
        std::cout << p << ' ' << q << '\n';  // overflow danger!
    }

    return 0;
}
