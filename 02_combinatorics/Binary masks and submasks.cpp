#include <iostream>
#include <string>

// Construct binary representation of a mask: O(log_2 n)
auto BinaryRepr(int mask, int n) {
    std::string res;
    while (n--) {
        res += '0' + ((mask >> n) & 1);
    }
    return res;
}

// Generate all binary masks of length n: O(2^n) (+ representation => x log_2 n)
void AllMasks(int n) {
    for (int mask = 0; mask < (1 << n); ++mask) {
        // process mask
        std::cout << BinaryRepr(mask, n) << '\n';
    }
}

// Generate all binary masks of length n along with all submasks: O(3^n) (+ representation => x log_2 n)
void AllSubmasks(int n) {
    for (int mask = 0; mask < (1 << n); ++mask) {
        // process mask
        for (int submask = mask; submask; submask = (submask - 1) & mask) {
            // process submask
            std::cout << BinaryRepr(mask, n) << " -> " << BinaryRepr(submask, n) << '\n';
        }
        // process '0..0' submask
        std::cout << BinaryRepr(mask, n) << " -> " << BinaryRepr(0, n) << '\n';
    }
}

int main() {
    // Case #1: generate all binary masks of length n
    {
        int n;
        std::cin >> n;
        AllMasks(n);
    }

    // Case 2: generate all binary masks of length n and their submasks
    {
        int n;
        std::cin >> n;
        AllSubmasks(n);
    }

    return 0;
}
