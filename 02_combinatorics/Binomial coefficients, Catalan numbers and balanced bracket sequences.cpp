#include <algorithm>
#include <iostream>
#include <stack>
#include <string>
#include <vector>

// Binomial coefficient: O(min{k, n - k})
int BinomialCoefficient(int n, int k) {
    if (k < 0 || k > n) {
        return 0;
    }
    if (k > n - k) {
        k = n - k;
    }
    int ans = 1;
    for (int i = 1; i <= k; ++i) {
        ans *= (n - i + 1);
        ans /= i;
    }
    return ans;
}

// Binomial coefficients using dp: O(nk)
auto BinomialCoefficientDP(int n, int k) {
    std::vector<std::vector<int>> dp(n + 1, std::vector<int>(k + 1));
    k = std::min(n, k);
    dp[0][0] = 1;
    for (int i = 1; i <= n; ++i) {
        dp[i][0] = 1;
        for (int j = 1; j <= std::min(k, i); ++j) {
            dp[i][j] = dp[i - 1][j] + dp[i - 1][j - 1];
        }
    }
    return dp;
}

// Catalan number: O(n)
inline int Catalan(int n) {
    return BinomialCoefficient(2 * n, n) / (n + 1);
}

// Catalan number using dp: O(n^2)
auto CatalanDP(int n) {
    std::vector<int> dp(n + 1);
    dp[0] = 1;
    for (int i = 1; i <= n; ++i) {
        for (int j = 0; j < i; ++j) {
            dp[i] += dp[j] * dp[i - j - 1];
        }
    }
    return dp;
}

// Catalan number using dp: O(n)
int LinearCatalan(int n) {
    int ans = 1;
    for (int k = 0; k < n; ++k) {
        ans *= 4 * k + 2;
        ans /= k + 2;
    }
    return ans;
}

// Binary exponentiation
int BinPow(int base, int power) {
    if (power <= 1) {
        return power ? base : 1;
    }
    int sqr = BinPow(base, power / 2);
    sqr *= sqr;
    if (power & 1) {
        sqr *= base;
    }
    return sqr;
}

// Number of BBS of length n with k different types of brackets: O(n)
inline int CountBBS(int n, int k) {
    return LinearCatalan(n) * BinPow(k, n);
}

// Validate a string as a BBS with 4 different types of brackets: O(|s|)
bool IsBBS(const std::string& s) {
    std::stack<char> st;
    for (auto c : s) {
        switch (c) {
        case '(':
        case '[':
        case '{':
        case '<':
            st.push(c);
            break;
        case ')':
            if (st.empty() || st.top() != '(') {
                return false;
            }
            st.pop();
            break;
        case ']':
            if (st.empty() || st.top() != '[') {
                return false;
            }
            st.pop();
            break;
        case '}':
            if (st.empty() || st.top() != '{') {
                return false;
            }
            st.pop();
            break;
        case '>':
            if (st.empty() || st.top() != '<') {
                return false;
            }
            st.pop();
            break;
        default:
            break;
        }
    }
    return st.empty();
}

// Generate all BBS of length 2 * n using binary masks: O(4^n / (n sqrt(n)))
void GenerateBBS(int n) {
    std::string res;
    for (int mask = 0; mask < (1 << (2 * n)); ++mask) {
        res.clear();
        int balance = 0;
        for (int i = 0; i < 2 * n; ++i) {
            int bit = (mask >> i) & 1;
            balance += (bit ? -1 : +1);
            if (balance < 0) {
                break;
            }
            res += (bit ? ')' : '(');
        }
        if (!balance) {
            std::cout << res << '\n';
        }
    }
}

// Find lexicographically next BBS: O(n)
bool NextBBS(std::string& s) {
    int n = s.length(), balance = 0;
    for (int i = n - 1; i >= 0; --i) {
        if (s[i] == '(') {
            --balance;
            if (balance > 0) {
                --balance;
                int open = (n - i - 1 - balance) / 2, close = n - i - 1 - open;
                s = s.substr(0, i) + ')' + std::string(open, '(') + std::string(close, ')');
                return true;
            }
        } else {
            ++balance;
        }
    }
    return false;
}

// Find BBS index with k different types of brackets using dp: O(n^2)
int IndexOfBBS(const std::string& s, int k = 1) {
    int n = s.length() / 2;
    std::vector<std::vector<int>> dp(2 * n + 1, std::vector<int>(2 * n + 1));
    dp[0][0] = 1;
    for (int i = 1; i <= 2 * n; ++i) {
        dp[i][0] = dp[i - 1][1];
        for (int j = 1; j < 2 * n; ++j) {
            dp[i][j] = dp[i - 1][j - 1] + dp[i - 1][j + 1];
        }
        dp[i][2 * n] = dp[i - 1][2 * n - 1];
    }

    int idx = 0;
    for (int i = 0, balance = 0; i < 2 * n; ++i) {
        if (s[i] == '(') {
            ++balance;
        } else {
            int pow = (2 * n - i - 1 - balance) / 2;
            idx += dp[2 * n - i - 1][balance + 1] * (k == 1 ? 1 : BinPow(k, pow));
            --balance;
        }
    }
    return idx + 1;
}

// Find k-th BBS using dp: O(n^2)
auto GetKthBBS(int n, int k) {
    std::vector<std::vector<int>> dp(2 * n + 1, std::vector<int>(2 * n + 1));
    dp[0][0] = 1;
    for (int i = 1; i <= 2 * n; ++i) {
        dp[i][0] = dp[i - 1][1];
        for (int j = 1; j < 2 * n; ++j) {
            dp[i][j] = dp[i - 1][j - 1] + dp[i - 1][j + 1];
        }
        dp[i][2 * n] = dp[i - 1][2 * n - 1];
    }

    std::string res;
    for (int i = 0, balance = 0; i < 2 * n; ++i) {
        if (dp[2 * n - i - 1][balance + 1] >= k) {
            res += '(';
            ++balance;
        } else {
            k -= dp[2 * n - i - 1][balance + 1];
            res += ')';
            --balance;
        }
    }
    return res;
}

int main() {
    // Case #1: calculate C_n^k in O(min{k, n - k})
    {
        int n, k;
        std::cin >> n >> k;
        std::cout << BinomialCoefficient(n, k) << "\n\n";
    }

    // Case #2: calculate and store table of C_n^k in O(nk)
    {
        int n, k;
        std::cin >> n >> k;
        auto table = BinomialCoefficientDP(n, k);
        std::cout << table[n][k] << "\n\n";
    }

    // Case #3: calculate C_n in O(n) using binomial coefficients,
    //          O(n^2) with dp and O(n) with recurrence
    {
        int n;
        std::cin >> n ;
        std::cout << Catalan(n) << '\n';
        auto table = CatalanDP(n);
        std::cout << table[n] << '\n';
        std::cout << LinearCatalan(n) << "\n\n";
    }

    // Case #4: find number of [valid] balanced bracket sequences (BBS)
    //          of length 2 * n with k different types of brackets
    {
        int n, k;
        std::cin >> n >> k;
        std::cout << CountBBS(n, k) << "\n\n";
    }

    // Case #5: validate a string as a BBS with 4 different types of brackets
    {
        std::string s;
        std::cin >> s;
        std::cout << (IsBBS(s) ? "YES\n\n" : "NO\n\n");
    }

    // Case 6: find next BBS with round brackets only in O(n)
    {
        std::string s;
        std::cin >> s;
        if (NextBBS(s)) {
            std::cout << s << '\n';
        }
        std::cout << '\n';
    }

    // Case 7: generate all BBS of length 2 * n in O(4^n)
    {
        int n;
        std::cin >> n;
        GenerateBBS(n);
        std::cout << '\n';
    }

    // Case 8: generate all BBS of length 2 * n in O(n * C_n)
    {
        int n;
        std::cin >> n;
        std::string s = std::string(n, '(') + std::string(n, ')');
        do {
            std::cout << s << '\n';
        } while (NextBBS(s));
        std::cout << '\n';
    }

    // Case #9: find a BBS index with k different types of brackets in O(n^2) (1-indexed)
    {
        std::string s;
        int k;
        std::cin >> s >> k;
        std::cout << IndexOfBBS(s, k) << "\n\n";
    }

    // Case #10: find k-th BBS of length 2 * n with round brackets only in O(n^2)
    {
        int n, k;
        std::cin >> n >> k;
        std::cout << GetKthBBS(n, k) << '\n';
    }

    return 0;
}
