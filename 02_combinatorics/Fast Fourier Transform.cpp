#include <algorithm>
#include <cmath>
#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

constexpr double kPi = 3.14159265358979323846264;

template<typename T>
class Complex {
    T x, y;
public:
    Complex(T x = {}, T y = {}) : x(x), y(y) {
    }

    T Real() const {
        return x;
    }

    T Imag() const {
        return y;
    }

    friend auto ToString(const Complex& c) {
        auto sign = c.y >= 0 ? "+" : "";
        return std::to_string(c.x) + sign + std::to_string(c.y) + 'i';
    }

    friend std::istream& operator>>(std::istream& in, Complex& c) {
        return in >> c.x >> c.y;
    }

    friend std::ostream& operator<<(std::ostream& out, const Complex& c) {
        return out << ToString(c) << '\n';
    }

    Complex Conj() const {
        return {x, -y};
    }

    double Abs() const {
        return std::sqrt(x * x + y * y);
    }

    operator bool() const {
        return x || y;
    }

    double Arg() const {
        if (y) {
            return 2 * std::atan((Abs() - x) / y);
        } else if (x > 0) {
            return 0;
        } else if (x < 0) {
            return kPi;
        } else {
            throw std::invalid_argument("Calculating principal argument value for zero :(");
        }
    }

    friend Complex operator+(const Complex& c1, const Complex& c2) {
        return {c1.x + c2.x, c1.y + c2.y};
    }

    friend Complex operator-(const Complex& c) {
        return {-c.x, -c.y};
    }

    friend Complex operator-(const Complex& c1, const Complex& c2) {
        return c1 + (-c2);
    }

    friend Complex operator*(const Complex& c1, const Complex& c2) {
        return {c1.x * c2.x - c1.y * c2.y, c1.x * c2.y + c2.x * c1.y};
    }

    friend Complex operator/(const Complex& c1, T c2) {
        if (!c2) {
            throw std::invalid_argument("Zero division error :(");
        }
        return {c1.x / c2, c1.y / c2};
    }

    friend Complex& operator+=(Complex& c1, const Complex& c2) {
        return c1 = c1 + c2;
    }

    friend Complex& operator-=(Complex& c1, const Complex& c2) {
        return c1 = c1 - c2;
    }

    friend Complex& operator*=(Complex& c1, const Complex& c2) {
        return c1 = c1 * c2;
    }

    friend Complex operator/=(Complex& c1, T c2) {
        return c1 = c1 / c2;
    }

    friend Complex& operator/=(Complex& c1, const Complex& c2) {
        return c1 = c1 / c2;
    }
};

using Point = Complex<double>;

// Fast Fourier Transform (FFT): O(n log n)
void FFT(std::vector<Point>& poly, bool inverse = false) {
    int n = poly.size();
    for (int idx = 0, i = 1; i < n; ++i) {
        int bit = n >> 1;
        while (idx >= bit) {
            idx -= bit;
            bit >>= 1;
        }
        idx += bit;
        if (i < idx) {
            std::swap(poly[i], poly[idx]);
        }
    }

    for (int level = 2; level <= n; level *= 2) {
        double phi = 2 * kPi / level * (inverse ? -1 : +1);
        Point root = {std::cos(phi), std::sin(phi)};
        for (int i = 0; i < n; i += level) {
            Point one(1);
            for (int j = 0; j < level / 2; ++j) {
                Point u = poly[i + j], v = poly[i + j + level / 2] * one;
                poly[i + j] = u + v;
                poly[i + j + level / 2] = u - v;
                one *= root;
            }
        }
    }
    if (inverse) {
        for (int i = 0; i < n; ++i) {
            poly[i] /= n;
        }
    }
}

// Multiply two polynomials
auto Convolve(const std::vector<int>& lhs, const std::vector<int>& rhs) {
    int n = 1, mx = std::max(lhs.size(), rhs.size());
    while (n < mx) {
        n *= 2;
    }
    n *= 2;
    std::vector<Point> left(lhs.begin(), lhs.end());
    std::vector<Point> right(rhs.begin(), rhs.end());
    left.resize(n); right.resize(n);
    FFT(left), FFT(right);
    for (int i = 0; i < n; ++i) {
        left[i] *= right[i];
    }
    FFT(left, true);
    std::vector<int> res(n);
    for (int i = 0; i < n; ++i) {
        res[i] = left[i].Real() + 0.5;
    }
    return res;
}

constexpr int kMod = 7'340'033;       // prime
constexpr int kRoot = 5;              // 5 ^ (1 << 20) = 1 (mod kMod)
constexpr int kInvRoot = 4'404'020;  // 5 ^ (-1) (mod kMod)
constexpr int kRootPow = 1 << 20;    // ~ maximum polynomial length
// (!) Other combination: (998'244'353, 31, 128'805'723, 1 << 23)

// Binary exponentiation
int BinPow(int base, int power) {
    if (power <= 1) {
        return power ? base % kMod : 1;
    }
    int sqr = BinPow((base * 1LL * base) % kMod, power / 2);
    if (power & 1) {
        sqr = (sqr * 1LL * base) % kMod;
    }
    return sqr;
}

// Number Theoretic Transform (NTT): O(n log n)
void NTT(std::vector<int>& poly, bool inverse = false) {
    int n = poly.size();
    for (int i = 1, j = 0; i < n; ++i) {
        int bit = n >> 1;
        while (j >= bit) {
            j -= bit;
            bit >>= 1;
        }
        j += bit;
        if (i < j) {
            std::swap(poly[i], poly[j]);
        }
    }

    for (int level = 2; level <= n; level *= 2) {
        int root = (inverse ? kInvRoot : kRoot);
        for (int i = level; i < kRootPow; i *= 2) {
            root = (root * 1LL * root) % kMod;
        }
        for (int i = 0; i < n; i += level) {
            int64_t one = 1;
            for (int j = 0; j < level / 2; ++j) {
                int u = poly[i + j], v = (one * poly[i + j + level / 2]) % kMod;
                poly[i + j] = (u + v < kMod ? u + v : u + v - kMod);
                poly[i + j + level / 2] = (u - v < 0 ? u - v + kMod : u - v);
                one = (one * root) % kMod;
            }
        }
    }
    if (inverse) {
        int64_t inv_n = BinPow(n, kMod - 2);
        for (int i = 0; i < n; ++i) {
            poly[i] = (poly[i] * 1LL * inv_n) % kMod;
        }
    }
}

// Multiply two polynomials modulo kMod
auto ConvolveMod(std::vector<int> lhs, std::vector<int> rhs) {
    int n = 1, mx = std::max(lhs.size(), rhs.size());
    while (n < mx) {
        n *= 2;
    }
    n *= 2;
    lhs.resize(n), rhs.resize(n);
    NTT(lhs), NTT(rhs);
    for (int i = 0; i < n; ++i) {
        lhs[i] = (lhs[i] * 1LL * rhs[i]) % kMod;
    }
    NTT(lhs, true);
    return lhs;
}

int main() {
    // Case #1: operations on complex numbers
    {
        Complex<int> c1, c2;
        std::cin >> c1 >> c2;

        std::cout << c1 + c2;
        std::cout << c1 - c2;
        std::cout << c1 * c2;
        std::cout << c1 / c2;
        std::cout << c1.Abs() << '\n';
        std::cout << c1.Conj();
        std::cout << c1.Real() << ' ' << c1.Imag() << '\n';
        std::cout << c1.Arg() << "\n\n";
    }

    // Case #2: apply FFT to a polynomial
    {
        int n, sz = 1;
        std::cin >> n;
        std::vector<int> poly(n);
        for (int i = 0; i < n; ++i) {
            std::cin >> poly[i];
        }

        while (sz < n) {
            sz *= 2;
        }
        sz *= 2;
        std::vector<Point> res(poly.begin(), poly.end());
        res.resize(sz);

        FFT(res);
        for (int i = 0; i < sz; ++i) {
            std::cout << res[i];
        }
        std::cout << "\n\n";
    }

    // Case #3: multiply two polynomials using FFT
    {
        int n, m;
        std::cin >> n >> m;
        std::vector<int> lhs(n), rhs(m);
        for (int i = 0; i < n; ++i) {
            std::cin >> lhs[i];
        }
        for (int i = 0; i < m; ++i) {
            std::cin >> rhs[i];
        }

        auto res = Convolve(lhs, rhs);
        for (auto elem : res) {
            std::cout << elem << ' ';
        }
        std::cout << "\n\n";
    }

    // Case #4: find all scalar products (A, cyclic shift of B)
    {
        int n;
        std::cin >> n;
        std::vector<int> a(2 * n), b(2 * n);
        for (int i = 0; i < n; ++i) {
            std::cin >> a[n - i - 1];  // reversed a + [0, ..., 0]
        }

        for (int i = 0; i < n; ++i) {
            std::cin >> b[i];
            b[n + i] = b[i];  // b + b
        }
        auto res = Convolve(a, b);
        for (int i = n; i < 2 * n; ++i) {
            std::cout << res[i] << ' ';
        }
        std::cout << "\n\n";
    }

    // Case #5: multiply two polynomials using NTT (need CRT to restore values)
    {
        int n, m;
        std::cin >> n >> m;
        std::vector<int> lhs(n), rhs(m);
        for (int i = 0; i < n; ++i) {
            std::cin >> lhs[i];
        }
        for (int i = 0; i < m; ++i) {
            std::cin >> rhs[i];
        }

        auto res = ConvolveMod(lhs, rhs);
        for (auto elem : res) {
            std::cout << elem << ' ';
        }
        std::cout << "\n\n";
    }

    return 0;
}
