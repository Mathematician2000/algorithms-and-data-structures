#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

// Binary representation of n: O(log_2 n)
auto BinaryRepr(int n) {
    std::string res;
    while (n) {
        res += '0' + (n & 1);
        n >>= 1;
    }
    std::reverse(res.begin(), res.end());
    return res;
}

// Convert binary string to a number: O(|s|)
int BinaryEval(const std::string& s) {
    int n = 0;
    for (auto c : s) {
        n = (n << 1) | (c - '0');
    }
    return n;
}

// Gray code by #n: O(1)
inline int GrayCodeByIndex(int n) {
    return n ^ (n >> 1);
}

// #n of Gray code: O(log_2 code)
inline int IndexOfGrayCode(int code) {
    int n = 0;
    while (code) {
        n ^= code;
        code >>= 1;
    }
    return n;
}

// Count 1-bits: O(|ans|)
// May come in handy:  pragma GCC target("popcnt")
// C++20:              std::popcnt from <bit>
inline int PopCount(int n) {
    int ans = 0;
    while (n) {
        n &= n - 1;
        ++ans;
    }
    return ans;
}

// Generate next k-combination of [1; n]: O(n) average
// Returns false if the last one is reached
bool NextCombination(std::vector<int>& comb, int n) {
    int k = comb.size();
    for (int i = k - 1; i >= 0; --i) {
        if (comb[i] < n + i - k + 1) {
            ++comb[i];
            for (int j = i + 1; j < k; ++j) {
                comb[j] = comb[j - 1] + 1;
            }
            return true;
        }
    }
    return false;
}

// Generate all k-combinations of [1; n] using Gray codes: O(2^n)
void AllCombinations(int n, int k) {
    for (int i = 0; i < (1 << n); ++i) {
        int code = GrayCodeByIndex(i);
        if (PopCount(code) == k) {
            // process combination
            for (int i = 0; i < n; ++i) {
                if ((code >> i) & 1) {
                    std::cout << i + 1 << ' ';
                }
            }
            std::cout << '\n';
        }
    }
}

// Generate all k-combinations of [1; n] using recursion: O(n C_n^k)
void GenerateCombinations(std::vector<bool>& comb, int k, int idx = 0, bool rev = false) {
    int n = comb.size();
    if (k < 0 || k > n - idx) {
        return;
    }
    if (!k) {
        // process combination
        for (int i = 0; i < idx; ++i) {
            if (comb[i]) {
                std::cout << i + 1 << ' ';
            }
        }
        std::cout << '\n';
        return;
    }
    comb[idx] = rev;
    GenerateCombinations(comb, k - rev, idx + 1, false);
    comb[idx] = !rev;
    GenerateCombinations(comb, k - !rev, idx + 1, true);
}

int main() {
    // Case #1: generate Gray code by number n
    {
        int n;
        std::cin >> n;
        std::cout << BinaryRepr(GrayCodeByIndex(n)) << "\n\n";
    }

    // Case #2: find original number n by Gray code
    {
        std::string code;
        std::cin >> code;
        std::cout << IndexOfGrayCode(BinaryEval(code)) << "\n\n";
    }

    // Case #3: generate next combination in O(k)
    {
        int n, k;
        std::cin >> n >> k;
        std::vector<int> comb(k);
        for (int i = 0; i < k; ++i) {
            std::cin >> comb[i];
        }
        if (NextCombination(comb, n)) {
            for (auto elem : comb) {
                std::cout << elem << ' ';
            }
            std::cout << "\n\n";
        } else {
            std::cout << "This is the last k-combination\n\n";
        }
    }

    // Case #4: generate all k-combinations in O(k * C_n^k)
    {
        int n, k;
        std::cin >> n >> k;
        std::vector<int> comb(k);
        for (int i = 0; i < k; ++i) {
            comb[i] = i + 1;
        }
        do {
            for (auto elem : comb) {
                std::cout << elem << ' ';
            }
            std::cout << '\n';
        } while (NextCombination(comb, n));
        std::cout << '\n';
    }

    // Case #5: generate all k-combinations using Gray codes in O(2^n)
    {
        int n, k;
        std::cin >> n >> k;
        AllCombinations(n, k);  // adjacent combinations differ by one element only!
    }

    // Case #6: generate all k-combinations using recursion in O(n * C_n^k)
    {
        int n, k;
        std::cin >> n >> k;
        std::vector<bool> comb(n);
        GenerateCombinations(comb, k);  // adjacent combinations differ by one element only!
    }

    return 0;
}
