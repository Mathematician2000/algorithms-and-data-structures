#include <algorithm>
#include <cmath>
#include <ctime>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

using Board = std::vector<std::vector<bool>>;

// 'Pretty' print for a chess board with n queens
void PrettyPrint(const Board& board) {
    int size = board.size();
    for (int row = 0; row < size; ++row) {
        for (int col = 0; col < size; ++col) {
            std::cout << (board[row][col] ? 'Q' : '*');
        }
        std::cout << '\n';
    }
    std::cout << '\n';
}

// Check if the new queen is placed correctly
bool CheckOneQueen(const Board& board, int i, int j) {
    int size = board.size();
    for (int row = 0; row < i; ++row) {
        if (board[row][j]) {
            return false;
        }
    }
    for (int col = 0; col < j; ++col) {
        if (board[i][col]) {
            return false;
        }
    }
    for (int diag = 1; diag <= i; ++diag) {
        if ((j >= diag && board[i - diag][j - diag]) ||
            (j + diag < size && board[i - diag][j + diag])) {
                return false;
        }
    }
    return true;
}

// Check if the n-queens puzzle is correct
bool CheckAllQueens(const Board& board) {
    int size = board.size();
    for (int row = 0; row < size; ++row) {
        for (int col = 0; col < size; ++col) {
            if (board[row][col] && !CheckOneQueen(board, row, col)) {
                return false;
            }
        }
    }
    return true;
}

// Convert a number in range [0; size^2) into chess board position
inline auto NumToCoord(int pos, int size) {
    return std::make_pair(pos / size, pos % size);
}

// Binary exponentiation
int BinPow(int base, int power) {
    if (power <= 1) {
        return power ? base : 1;
    }
    int sqr = BinPow(base, power / 2);
    sqr *= sqr;
    if (power & 1) {
        sqr *= base;
    }
    return sqr;
}

// Generate next k-combination of [1; n]
bool NextCombination(std::vector<int>& comb, int n) {
    int k = comb.size();
    for (int i = k - 1; i >= 0; --i) {
        if (comb[i] < n - k + 1 + i) {
            ++comb[i];
            for (int j = i + 1; j < k; ++j) {
                comb[j] = comb[j - 1] + 1;
            }
            return true;
        }
    }
    return false;
}

// Generate correct n-queens puzzle using backtracking
bool Backtracking(Board& board, int row = 0) {
    int size = board.size();
    if (row == size) {
        return true;
    }
    for (int col = 0; col < size; ++col) {
        board[row][col] = true;
        if (CheckOneQueen(board, row, col) && Backtracking(board, row + 1)) {
            return true;
        }
        board[row][col] = false;
    }
    return false;
}

// Generate random integer in [from; to)
inline int GetRand(int from, int to) {
    return from + rand() / (RAND_MAX + .0) * (to - from);
}

//  Sample random variable ~ Be(p)
inline bool RollDice(double prob) {
    return rand() / (RAND_MAX + .0) < prob;
}

// Knuth algorithm for random permutation: O(n)
void RandomPermutation(std::vector<int>& pos) {
    int n = pos.size();
    for (int i = 0; i < n; ++i) {
        pos[i] = i;
    }
    for (int i = 0; i < n - 1; ++i) {
        int j = GetRand(i, n);
        if (j != i) {
            std::swap(pos[i], pos[j]);
        }
    }
}

// Calculate number of queen diagonal conflicts
int CalculateEntropy(const std::vector<int>& pos) {
    int loss = 0, n = pos.size();
    std::vector<int> cnt_left(2 * n), cnt_right(2 * n);
    for (int i = 0; i < n; ++i) {
        ++cnt_right[pos[i] + i];
        ++cnt_left[pos[i] - i + n];
    }
    for (int i = 0; i < 2 * n; ++i) {
        loss += cnt_right[i] * (cnt_right[i] - 1) / 2;
        loss += cnt_left[i] * (cnt_left[i] - 1) / 2;
    }
    return loss;
}

// Generate correct n-queens puzzle using simulated annealing
bool SimulatedAnnealing(std::vector<int>& pos) {
    static constexpr int kMaxAttempts = 20;
    static constexpr double kTempInit = 1, kTempMin = 1e-7, kTempAlpha = 0.9999;
    srand(time(nullptr));
    int n = pos.size();
    for (int t = 0; t < kMaxAttempts; ++t) {
        RandomPermutation(pos);
        int ent = CalculateEntropy(pos);
        for (double Q = kTempInit; ent && Q > kTempMin; Q *= kTempAlpha) {
            int r1 = GetRand(0, n), r2 = GetRand(0, n);
            if (r1 != r2) {
                std::swap(pos[r1], pos[r2]);
                int new_ent = CalculateEntropy(pos);
                if (new_ent < ent || RollDice(std::exp((ent - new_ent) / Q))) {
                    ent = new_ent;
                } else {
                    std::swap(pos[r1], pos[r2]);
                }
            }
        }
        if (!ent) {
            return true;
        }
    }
    return false;
}

// Individual class for genetic algorithm
class Individual {
    std::vector<int> pos;
    int n, ent;

    void RandomPermutation() {
        for (int i = 0; i < n; ++i) {
            pos[i] = i;
        }
        for (int i = 0; i < n - 1; ++i) {
            int j = GetRand(i, n);
            if (j != i) {
                std::swap(pos[i], pos[j]);
            }
        }
    }

    friend int CalculateEntropy(const Individual& indiv) {
        int loss = 0, n = indiv.n;
        const auto& pos = indiv.pos;
        std::vector<int> cnt_left(2 * n), cnt_right(2 * n);
        for (int i = 0; i < n; ++i) {
            ++cnt_left[pos[i] - i + n];
            ++cnt_right[pos[i] + i];
        }
        for (int i = 0; i < 2 * n; ++i) {
            loss += cnt_left[i] * (cnt_left[i] - 1) / 2;
            loss += cnt_right[i] * (cnt_right[i] - 1) / 2;
        }
        return loss;
    }

public:
    Individual(int n) : pos(n), n(n) {
        RandomPermutation();
        ent = CalculateEntropy(*this);
    }

    void PrettyPrint() const {
        for (int i = 0; i < n; ++i) {
            std::string line(n, '.');
            line[pos[i]] = '#';
            std::cout << line << '\n';
        }
    }

    void Print() const {
        for (auto elem : pos) {
            std::cout << elem + 1 << ' ';
        }
        std::cout << '\n';
    }

    int& operator[](size_t idx) {
        return pos[idx];
    }

    int operator[](size_t idx) const {
        return pos[idx];
    }

    int GetEntropy() const {
        return ent;
    }

    friend bool operator<(const Individual& ind1, const Individual& ind2) {
        return ind1.GetEntropy() < ind2.GetEntropy();
    }

    void Mutate() {
        int r1 = GetRand(0, n), r2 = GetRand(0, n);
        if (r1 != r2) {
            std::swap(pos[r1], pos[r2]);
            ent = CalculateEntropy(*this);
        }
    }

    friend Individual Crossover(const Individual& p1, const Individual& p2) {
        Individual child(p1);
        int n = p1.n;
        std::vector<int> inv(n);
        for (int i = 0; i < n; ++i) {
            inv[child[i]] = i;
        }
        for (int i = 0; i < n; ++i) {
            if (child[i] != p2[i] && RollDice(0.5)) {
                int i1 = i, i2 = inv[p2[i]];
                std::swap(child[i1], child[i2]);
                inv[child[i1]] = i1, inv[child[i2]] = i2;
            }
        }
        child.ent = CalculateEntropy(child);
        return child;
    }
};

// Generate correct n-queens puzzle using simulated annealing
Individual GeneticAlgorithm(int n,
                            int max_pop,
                            int iters,
                            double mutate_prob) {
    srand(time(nullptr));
    std::vector<Individual> population(max_pop, n);
    while (population.front().GetEntropy() && iters--) {
        int n = population.size();

        // Crossover phase
        for (int i = 0; i < n - 1; ++i) {
            for (int j = i + 1; j < n; ++j) {
                population.push_back(Crossover(population[i], population[j]));
            }
        }
        n = population.size();

        // Mutation phase
        for (int i = 0; i < n; ++i) {
            if (RollDice(mutate_prob)) {
                population[i].Mutate();
            }
        }

        // Selection phase
        std::sort(population.begin(), population.end());
        if (max_pop < n) {
            population.erase(population.begin() + max_pop, population.end());
        }
    }
    return population.front();
}

int main() {
    // Case #1: n-queens puzzle using n-combinations (n <= 7) in O(n^2 * C_{n^2}^n)
    {
        int n;
        std::cin >> n;
        Board board(n, std::vector<bool>(n));
        std::vector<int> comb(n);
        for (int i = 0; i < n; ++i) {
            comb[i] = i + 1;
        }
        do {
            for (auto elem : comb) {
                auto [r, c] = NumToCoord(elem - 1, n);
                board[r][c] = true;
            }
            if (CheckAllQueens(board)) {
                PrettyPrint(board);
                break;
            }
            for (auto elem : comb) {
                auto [r, c] = NumToCoord(elem - 1, n);
                board[r][c] = false;
            }
        }  while (NextCombination(comb, n * n));
    }

    // Case #2: n-queens puzzle (n <= 9) in O(n^2 * n^n)
    {
        int n;
        std::cin >> n;
        Board board(n, std::vector<bool>(n));
        for (int mask = 0; mask < BinPow(n, n); ++mask) {
            int m = mask;
            for (int i = 0; i < n; ++i) {
                board[i][m % n] = true;
                m /= n;
            }
            if (CheckAllQueens(board)) {
                PrettyPrint(board);
                break;
            }
            m = mask;
            for (int i = 0; i < n; ++i) {
                board[i][m % n] = false;
                m /= n;
            }
        }
    }

    // Case #3: n-queens puzzle using n-permutations (n <= 13) in O(n^2 * n!)
    {
        int n;
        std::cin >> n;
        Board board(n, std::vector<bool>(n));
        std::vector<int> perm(n);
        for (int i = 0; i < n; ++i) {
            perm[i] = i;
        }
        do {
            for (int i = 0; i < n; ++i) {
                board[i][perm[i]] = true;
            }
            if (CheckAllQueens(board)) {
                PrettyPrint(board);
                break;
            }
            for (int i = 0; i < n; ++i) {
                board[i][perm[i]] = false;
            }
        }  while (std::next_permutation(perm.begin(), perm.end()));
    }

    // Case #4: n-queens puzzle using backtracking (n <= 28)
    {
        int n;
        std::cin >> n;
        Board board(n, std::vector<bool>(n));
        if (Backtracking(board)) {
            PrettyPrint(board);
        }
    }

    // Case 5: n-queens puzzle using genetic algorithm (n ~ 10^2)
    {
        int n;
        std::cin >> n;
        auto pos = GeneticAlgorithm(n, 25, 10000, 0.9);
        printf("Entropy = %d\n\n", CalculateEntropy(pos));
        Board board(n, std::vector<bool>(n));
        for (int i = 0; i < n; ++i) {
            board[i][pos[i]] = true;
        }
        PrettyPrint(board);
        std::cout << '\n';
    }

    // Case 6: n-queens puzzle using simulated annealing (n ~ 10^3)
    {
        int n;
        std::cin >> n;
        std::vector<int> pos(n);
        SimulatedAnnealing(pos);
        printf("Entropy = %d\n\n", CalculateEntropy(pos));
        Board board(n, std::vector<bool>(n));
        for (int i = 0; i < n; ++i) {
            board[i][pos[i]] = true;
        }
        PrettyPrint(board);
    }

    return 0;
}
