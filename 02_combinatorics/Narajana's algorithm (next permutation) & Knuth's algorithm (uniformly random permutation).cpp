#include <algorithm>
#include <iostream>
#include <vector>

// Generate next permutation (returns false if the last one is reached)
bool Narajana(std::vector<int>& perm) {
    int n = perm.size(), j = n - 1, k = n - 1;
    while (j && perm[j - 1] > perm[j]) {
        --j;
    }
    if (!j) {
        return false;
    }
    --j;
    while (perm[k] < perm[j]) {
        --k;
    }
    std::swap(perm[j], perm[k]);
    for (int i1 = j + 1, i2 = n - 1; i1 < i2; ++i1, --i2) {
        std::swap(perm[i1], perm[i2]);
    }
    return true;
}

// Generate random integer in [from; to)
inline int GetRand(int from, int to) {
    return from + rand() / (RAND_MAX + .0) * (to - from);
}

// Generate uniformly random permutation (starting from an arbitrary permutation)
void KnuthGenerate(std::vector<int>& perm) {
    int n = perm.size();
    for (int i = 1; i < n; ++i) {
        int j = GetRand(0, i + 1);
        std::swap(perm[i], perm[j]);
    }
}

int main() {
    // Case #1: generate next permutation in O(n)
    {
        int n;
        std::cin >> n;
        std::vector<int> perm(n);
        for (int i = 0; i < n; ++i) {
            std::cin >> perm[i];
        }

        if (Narajana(perm)) {
            for (auto elem : perm) {
                std::cout << elem << ' ';
            }
            std::cout << "\n\n";
        } else {
            std::cout << "This is the last permutation\n\n";
        }
    }

    // Case #2: generate next permutation using std::next_permutation (the same algorithm)
    {
        int n;
        std::cin >> n;
        std::vector<int> perm(n);
        for (int i = 0; i < n; ++i) {
            std::cin >> perm[i];
        }

        if (std::next_permutation(perm.begin(), perm.end())) {
            for (auto elem : perm) {
                std::cout << elem << ' ';
            }
            std::cout << "\n\n";
        } else {
            std::cout << "This is the last permutation\n\n";
        }
    }

    // Case #3: generate all permutations in O(n * n!)
    {
        int n;
        std::cin >> n;
        std::vector<int> perm(n);
        for (int i = 0; i < n; ++i) {
            perm[i] = i + 1;
        }
        do {
            for (auto elem : perm) {
                std::cout << elem << ' ';
            }
            std::cout << '\n';
        } while (Narajana(perm));
    }

    // Case #4: generate random permutation in O(n)
    {
        int n;
        std::cin >> n;
        std::vector<int> perm(n);
        for (int i = 0; i < n; ++i) {
            perm[i] = i + 1;
        }

        KnuthGenerate(perm);
        for (auto elem : perm) {
            std::cout << elem << ' ';
        }
    }

    return 0;
}
