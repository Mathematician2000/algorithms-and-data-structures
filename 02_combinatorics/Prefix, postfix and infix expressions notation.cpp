#include <cmath>
#include <iostream>
#include <stack>
#include <stdexcept>
#include <string>
#include <vector>

// Check if the token is a digit
inline bool IsDigit(char c) {
    return '0' <= c && c <= '9';
}

// Check if the token is an alpha character
inline bool IsAlpha(char c) {
    return 'a' <= c && c <= 'z';
}

// Check if the token is an operation
inline bool IsOperation(char c) {
    return c == '+' || c == '-' || c == '*' || c == '/';
}

// Check if the token is a function
inline bool IsFunction(const std::string& s) {
    return s == "sin" || s == "cos" || s == "exp" || s == "log" || s == "sqrt";
}

// Get operation priority
inline int GetPriority(char c) {
    switch (c) {
    case '+':
    case '-':
        return 1;
    case '*':
    case '/':
        return 2;
    default:
        return 0;
    }
}

// Parse float number
inline double ParseNumber(const std::string& token) {
    return std::stod(token);
}

// Evaluate operation
double EvaluateOperation(char op, double x, double y) {
    switch (op) {
    case '+':
        return x + y;
    case '-':
        return x - y;
    case '*':
        return x * y;
    case '/':
        return x / y;
    default:
        throw std::invalid_argument("Unknown operation '" + std::string(1, op) + "'");
    }
}

// Evaluate function
double EvaluateFunction(const std::string& func, double x) {
    if (func == "sin") {
        return std::sin(x);
    } else if (func == "cos") {
        return std::cos(x);
    } else if (func == "log") {
        return std::log(x);
    } else if (func == "exp") {
        return std::exp(x);
    } else if (func == "sqrt") {
        return std::sqrt(x);
    } else {
        throw std::invalid_argument("Unknown function '" + func + "'");
    }
}

// Tokenize expression
auto Tokenize(const std::string& expr, char delim = ' ') {
    try {
        std::vector<std::string> tokens;
        int len = expr.length();
        for (int idx = 0; idx < len; ++idx) {
            if (expr[idx] == delim) {
                continue;
            } else if (IsDigit(expr[idx])) {
                std::string token(1, expr[idx++]);
                while (idx < len && IsDigit(expr[idx])) {
                    token += expr[idx++];
                }
                if (idx < len && expr[idx] == '.') {
                    ++idx;
                    while (idx < len && IsDigit(expr[idx])) {
                        token += expr[idx];
                        ++idx;
                    }
                }
                tokens.push_back(token);
                --idx;
            } else if (expr[idx] == '(' || expr[idx] == ')' || IsOperation(expr[idx])) {
                tokens.emplace_back(1, expr[idx]);
            } else {
                std::string func;
                while (idx < len && IsAlpha(expr[idx])) {
                    func += expr[idx++];
                }
                if (!IsFunction(func)) {
                    throw std::invalid_argument("Unknown operation");
                }
                tokens.push_back(func);
                --idx;
            }
        }
        return tokens;
    } catch (std::exception& err) {
        throw std::invalid_argument("Expression is invalid: " + std::string(err.what()));
    }
}

// Evaluate expression in postfix notation (RPN)
double EvaluatePostfix(const std::vector<std::string>& tokens) {
    try {
        std::stack<double> st;
        for (auto token : tokens) {
            if (IsDigit(token[0])) {
                st.push(ParseNumber(token));
            } else if (IsOperation(token[0])) {
                double rhs = st.top(); st.pop();
                double lhs = st.top(); st.pop();
                st.push(EvaluateOperation(token[0], lhs, rhs));
            } else if (IsFunction(token)) {
                double x = st.top(); st.pop();
                st.push(EvaluateFunction(token, x));
            } else {
                throw std::invalid_argument("Unknown token '" + token + "'");
            }
        }
        if (st.size() != 1) {
            throw std::invalid_argument("Wrong number of arguments");
        }
        return st.top();
    } catch (std::exception& err) {
        throw std::invalid_argument("Expression is invalid: " + std::string(err.what()));
    }
}

// Evaluate expression in prefix notation
double EvaluatePrefix(const std::vector<std::string>& tokens) {
    try {
        std::stack<std::string> st;
        for (auto token : tokens) {
            if (IsDigit(token[0])) {
                double rhs = ParseNumber(token);
                while (!st.empty() && IsDigit(st.top()[0])) {
                    double lhs = ParseNumber(st.top()); st.pop();
                    auto op = st.top(); st.pop();
                    if (IsOperation(op[0])) {
                        rhs = EvaluateOperation(op[0], lhs, rhs);
                    } else if (IsFunction(op)) {
                        rhs = EvaluateFunction(op, lhs);
                    }
                }
                st.push(std::to_string(rhs));
            } else {
                st.push(token);
            }
        }
        if (st.size() != 1) {
            throw std::invalid_argument("Wrong number of arguments");
        }
        return std::stoi(st.top());
    } catch (std::exception& err) {
        throw std::invalid_argument("Expression is invalid: " + std::string(err.what()));
    }
}

// Dijkstra's shunting-yard algorithm (infix => postfix)
auto TokenizeInfix(const std::vector<std::string>& tokens) {
    try {
        std::vector<std::string> output;
        std::stack<std::string> st;
        for (auto token : tokens) {
            if (IsDigit(token[0])) {
                output.push_back(token);
            } else if (IsFunction(token)) {
                st.push(token);
            } else if (IsOperation(token[0])) {
                while (!st.empty() &&
                       IsOperation(st.top()[0]) &&
                       GetPriority(st.top()[0]) >= GetPriority(token[0])) {
                    output.push_back(st.top());
                    st.pop();
                }
                st.push(token);
            } else if (token == "(") {
                st.push("(");
            } else if (token == ")") {
                while (!st.empty() && st.top() != "(") {
                    output.push_back(st.top());
                    st.pop();
                }
                if (st.empty()) {
                    throw std::invalid_argument("An opening bracket is missing");
                }
                st.pop();
                if (!st.empty() && IsFunction(st.top())) {
                    output.push_back(st.top());
                    st.pop();
                }
            } else {
                throw std::invalid_argument("Unknown token '" + token + "'");
            }
        }
        while (!st.empty()) {
            auto op = st.top();
            st.pop();
            if (op == "(") {
                throw std::invalid_argument("A closing bracket is missing");
            }
            if (op == ")") {
                throw std::invalid_argument("A opening bracket is missing");
            }
            output.push_back(op);
        }
        return output;
    } catch (std::exception& e) {
        throw std::invalid_argument("Expression is invalid: " + std::string(e.what()));
    }
}

// Evaluate expression in infix notation
double EvaluateInfix(const std::vector<std::string>& tokens) {
    return EvaluatePostfix(TokenizeInfix(tokens));
}

int main() {
    // Case #1: evaluate expression in postfix notation
    {
        std::string expr;
        std::getline(std::cin, expr);
        std::cout << "= " << EvaluatePostfix(Tokenize(expr)) << "\n\n";
    }

    // Case #2: evaluate expression in prefix notation
    {
        std::string expr;
        std::getline(std::cin, expr);
        std::cout << "= " << EvaluatePrefix(Tokenize(expr)) << "\n\n";
    }

    // Case #3: parse expression
    {
        std::string expr;
        std::getline(std::cin, expr);
        std::cout << "<=> ";
        for (auto token : Tokenize(expr)) {
            std::cout << ' ' << token;
        }
        std::cout << "\n\n";
    }

    // Case #4: convert expression from infix in postfix notation
    {
        std::string expr;
        std::getline(std::cin, expr);
        std::cout << "<=> ";
        for (auto token : TokenizeInfix(Tokenize(expr))) {
            std::cout << ' ' << token;
        }
        std::cout << "\n\n";
    }

    // Case #5: evaluate expression in infix notation
    {
        std::string expr;
        std::getline(std::cin, expr);
        std::cout << "= " << EvaluateInfix(Tokenize(expr)) << "\n\n";
    }

    return 0;
}
