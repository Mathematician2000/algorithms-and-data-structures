#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <utility>
#include <vector>

constexpr int kInf = 1e9;
constexpr double kDefaultEps = 1e-6;

// Array printing
void PrettyPrint(const std::vector<int>& arr) {
    for (auto elem : arr) {
        std::cout << elem << ' ';
    }
    std::cout << '\n';
}

// Lower bound (binary search): O(log_2 n)
int LowerBound(const std::vector<int>& arr, int x) {
    int size = arr.size();
    int left = 0, right = size;
    while (left != right) {
        int mid = (left + right) / 2;
        if (arr[mid] >= x) {
            right = mid;
        } else {
            left = mid + 1;
        }
    }
    return left;
}

// Upper bound (binary search): O(log_2 n)
int UpperBound(const std::vector<int>& arr, int x) {
    int size = arr.size();
    int left = 0, right = size;
    while (left != right) {
        int mid = (left + right) / 2;
        if (arr[mid] > x) {
            right = mid;
        } else {
            left = mid + 1;
        }
    }
    return left;
}

// Equal range (binary search): O(log_2 n)
auto EqualRange(const std::vector<int>& arr, int x) {
    return std::make_pair(LowerBound(arr, x), UpperBound(arr, x));
}

// Binary search a ascending function: O(log_2((to - from) / eps))
template<typename T1, typename T2>
T1 BinarySearch(const std::function<T2(T1)>& func, const T2& val, T1 from, T1 to, T1 eps = kDefaultEps) {
    while (to - from > eps) {
        T1 mid = (from + to) / 2;
        if (func(mid) < val) {
            from = mid;
        } else {
            to = mid;
        }
    }
    return (from + to) / 2;
}

// Ternary search a convex function (concave up => minimization): O(log_2((to - from) / eps))
template<typename T1, typename T2>
T1 TernarySearch(const std::function<T2(T1)>& func, T1 from, T1 to, T1 eps = kDefaultEps) {
    while (to - from > eps) {
        T1 delta = (to - from) / 3, l = from + delta, r = to - delta;
        if (func(l) > func(r)) {
            from = l;
        } else {
            to = r;
        }
    }
    return (from + to) / 2;
}

// Binary search the answer: O(log_2(to - from))
template<typename T>
T BinarySearchAnswer(const std::function<bool(T)>& check, T from, T to) {
    while (from != to) {
        T mid = (from + to) / 2;
        if (check(mid)) {
            to = mid;
        } else {
            from = mid + 1;
        }
    }
    return from;
}

// Find k-th order statistics: O(n) average
int GetKthElement(std::vector<int>& arr, int order) {
    int size = arr.size(), left = 0, right = size - 1;
    while (true) {
        if (left + 1 >= right) {
            if (left + 1 == right && arr[left] > arr[right]) {
                std::swap(arr[left], arr[right]);
            }
            return arr[order];
        }

        int mid = (left + right) / 2;
        std::swap(arr[left + 1], arr[mid]);
        if (arr[left] > arr[right]) {
            std::swap(arr[left], arr[right]);
        }
        if (arr[left + 1] > arr[right]) {
            std::swap(arr[left + 1], arr[right]);
        }
        if (arr[left] > arr[left + 1]) {
            std::swap(arr[left], arr[left + 1]);
        }

        int left_ptr = left + 1, right_ptr = right, elem = arr[left_ptr];
        while (true) {
            while (arr[++left_ptr] < elem);
            while (arr[--right_ptr] > elem);
            if (left_ptr > right_ptr) {
                break;
            }
            std::swap(arr[left_ptr], arr[right_ptr]);
        }

        arr[left + 1] = arr[right_ptr];
        arr[right_ptr] = elem;

        if (right_ptr >= order) {
            right = right_ptr - 1;
        }
        if (right_ptr <= order) {
            left = left_ptr;
        }
    }
}

int main() {
    // Case 1: custom lower_bound()
    {
        int size, queries, x;
        std::cin >> size >> queries;
        std::vector<int> arr(size);
        for (auto& elem : arr) {
            std::cin >> elem;
        }

        std::sort(arr.begin(), arr.end());  // or stable_sort()
        if (size <= 20) {
            std::cout << "Array: ";
            PrettyPrint(arr);
        }
        while (queries--) {
            std::cin >> x;
            int idx = LowerBound(arr, x);
            if (idx == size) {
                std::cout << "Not found :(\n";
            } else {
                printf("Found! idx = %d\n", idx);
            }
        }
        std::cout << '\n';
    }

    // Case 2: custom upper_bound()
    {
        int size, queries, x;
        std::cin >> size >> queries;
        std::vector<int> arr(size);
        for (auto& elem : arr) {
            std::cin >> elem;
        }
        std::sort(arr.begin(), arr.end());
        if (size <= 20) {
            std::cout << "Array: ";
            PrettyPrint(arr);
        }
        while (queries--) {
            std::cin >> x;
            int idx = UpperBound(arr, x);
            if (idx == size) {
                std::cout << "Not found :(\n";
            } else {
                printf("Found! idx = %d\n", idx);
            }
        }
        std::cout << '\n';
    }

    // Case 3: custom equal_range()
    {
        int size, queries, x;
        std::cin >> size >> queries;
        std::vector<int> arr(size);
        for (auto& elem : arr) {
            std::cin >> elem;
        }
        std::sort(arr.begin(), arr.end());
        if (size <= 20) {
            std::cout << "Array: ";
            PrettyPrint(arr);
        }
        while (queries--) {
            std::cin >> x;
            auto [from, to] = EqualRange(arr, x);
            if (from == to) {
                std::cout << "Not found :(\n";
            } else {
                printf("Found! i = %d, j = %d\n", from, to);
            }
        }
        std::cout << '\n';
    }

    // Case 4: binary search on ascending function
    {
        double y, left, right;
        std::cin >> y >> left >> right;
        auto lambda = [](auto x) { return std::sqrt(x); };
        double x = BinarySearch<double, double>(lambda, y, left, right);
        printf("sqrt(%lf) = %lf\n\n", x, y);
    }

    // Case 5: ternary search on convex function
    {
        double a, b, c;  // ax^2 + bx + c, a > 0
        std::cin >> a >> b >> c;
        auto lambda = [a, b, c](auto x) { return (a * x + b) * x + c; };
        double x = TernarySearch<double, double>(lambda, -kInf, kInf);
        printf("func(%lf) = %lf\n\n", x, lambda(x));
    }

    // Case 6: binary search the answer
    {
        int y, left, right;
        std::cin >> y >> left >> right;
        auto lambda = [y](auto x) -> bool { return x > y; };
        int x = BinarySearchAnswer<int>(lambda, left, right);
        printf("Minimum possible answer on [%d; %d] for %d is %d\n\n", left, right, y, x);
    }

    // Case 7: find k-th element (order statistics) using partitions
    {
        int size, queries, order;
        std::cin >> size >> queries;
        std::vector<int> arr(size);
        for (auto& elem : arr) {
            std::cin >> elem;
        }
        while (queries--) {
            std::cin >> order;
            printf("K-th element is %d\n", GetKthElement(arr, order));  // see std::nth_element()
            if (size <= 20) {
                std::cout << "Array: ";
                PrettyPrint(arr);
            }
        }
        std::cout << '\n';
    }

    return 0;
}
