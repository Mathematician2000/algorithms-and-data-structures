#include <algorithm>
#include <chrono>
#include <iostream>
#include <iterator>
#include <queue>
#include <vector>

constexpr int kNone = -1;

template<typename T>
using MinHeap = std::priority_queue<T, std::vector<T>, std::greater<T>>;

using Clock = std::chrono::high_resolution_clock;

// Duration in seconds
inline double ToSeconds(const std::chrono::duration<double, std::milli>& delta) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(delta).count() / 1000.;
}

// Array printing
template<typename T>
void PrettyPrint(const std::vector<T>& arr) {
    for (auto elem : arr) {
        std::cout << elem << ' ';
    }
    std::cout << '\n';
}

// Bubble sort: O(n^2)
template <typename T>
void BubbleSort(std::vector<T>& arr) {
    int size = arr.size();
    bool sorted = false;
    while (!sorted) {
        sorted = true;
        for (int i = 0; i < size - 1; ++i) {
            if (arr[i + 1] < arr[i]) {
                sorted = false;
                std::swap(arr[i], arr[i + 1]);
            }
        }
    }
}

// Insertion sort: O(n^2)
template <typename T>
void InsertionSort(std::vector<T>& arr) {
    int size = arr.size();
    for (int i = 1; i < size; ++i) {
        int j = i;
        T elem = arr[i];
        while (j && elem < arr[j - 1]) {
            arr[j] = arr[j - 1];
            --j;
        }
        arr[j] = elem;
    }
}

// Selection sort: O(n^2)
template <typename T>
void SelectionSort(std::vector<T>& arr) {
    int size = arr.size();
    for (int i = 0; i < size - 1; ++i) {
        int idx = i;
        for (int j = i + 1; j < size; ++j) {
            if (arr[j] < arr[idx]) {
                idx = j;
            }
        }
        if (i != idx) {
            std::swap(arr[i], arr[idx]);
        }
    }
}

// Shell sort: O(n^{3/2})
template <typename T>
void ShellSort(std::vector<T>& arr) {
    int size = arr.size(), step = 1;
    while (9 * step <= size) {
        step = 3 * step + 1;
    }
    while (step > 0) {
        for (int i = step; i < size; ++i) {
            int j = i;
            T elem = arr[i];
            while (j >= step && elem < arr[j - step]) {
                arr[j] = arr[j - step];
                j -= step;
            }
            arr[j] = elem;
        }
        step /= 3;
    }
}

// Quick sort: O(n^2) worst, O(n log n) average
template <typename T>
void QuickSort(std::vector<T>& arr, int left = 0, int right = kNone) {
    int size = arr.size();
    if (right == kNone) {
        right = size - 1;
    }
    T pivot = arr[left + rand() % (right - left + 1)];  // random pivot
    int left_ptr = left, right_ptr = right;
    while (true) {
        while (left_ptr <= right && arr[left_ptr] < pivot) {
            ++left_ptr;
        }
        while (right_ptr >= left && pivot < arr[right_ptr]) {
            --right_ptr;
        }
        if (left_ptr > right_ptr) {
            break;
        }
        std::swap(arr[left_ptr++], arr[right_ptr--]);
    }
    if (left < right_ptr) {
        QuickSort(arr, left, right_ptr);
    }
    if (right > left_ptr) {
        QuickSort(arr, left_ptr, right);
    }
}

// Merge sort: O(n log n)
template <typename T>
void MergeSort(std::vector<T>& arr) {
    int size = arr.size();
    if (size == 1) {
        return;
    }
    int half = (size + 1) / 2;
    std::vector<T> left(arr.begin(), arr.begin() + half), right(arr.begin() + half, arr.end());
    MergeSort(left), MergeSort(right);
    arr.clear();
    arr.reserve(size);
    std::merge(left.begin(), left.end(), right.begin(), right.end(), std::back_inserter(arr));
}

// Heap sort: O(n log n)
template <typename T>
void HeapSort(std::vector<T>& arr) {
    int size = arr.size();
    MinHeap<T> heap(arr.begin(), arr.end());
    for (int i = 0; i < size; ++i) {
        arr[i] = heap.top();
        heap.pop();
    }
}

// Count sort: O(n + k), 0 <= a[i] <= k for all i
// Assume a[i] >= 0 for all i
void CountSort(std::vector<unsigned>& arr) {
    int size = arr.size();
    std::vector<int> cnt;
    for (auto elem : arr) {
        if (elem >= cnt.size()) {
            cnt.resize(elem + 1);
        }
        ++cnt[elem];
    }
    unsigned elem = 0;
    for (int i = 0; i < size; ++elem) {
        for (int j = 0; j < cnt[elem]; ++j) {
            arr[i++] = elem;
        }
    }
}

// Radix sort: O(nk)
// Assume a[i] >= 0 for all i
// Assume a_i has <= k digits for all i
void RadixSort(std::vector<unsigned>& arr) {
    static constexpr size_t kCntSize = 1 << 16;
    static constexpr size_t kInitSize = 1 << 8;
    std::vector<std::vector<unsigned>> lsb(kCntSize);
    std::vector<std::vector<unsigned>> msb(kCntSize);
    for (size_t i = 0; i < kCntSize; ++i) {  // to optimize push_back()
        lsb[i].reserve(kInitSize);
        msb[i].reserve(kInitSize);
    }

    // step 1: 2 least significant bytes
    for (auto elem : arr) {
        lsb[elem & 0xFFFFU].push_back(elem);
    }
    size_t idx = 0;
    for (unsigned dbyte = 0; dbyte < kCntSize; ++dbyte) {
        for (auto elem : lsb[dbyte]) {
            arr[idx++] = elem;
        }
    }

    // step 2: 2 most significant bytes
    for (auto elem : arr) {
        msb[(elem >> 16) & 0xFFFFU].push_back(elem);
    }
    idx = 0;
    for (unsigned dbyte = 0; dbyte < kCntSize; ++dbyte) {
        for (auto elem : msb[dbyte]) {
            arr[idx++] = elem;
        }
    }
}

int main() {
    // Case 1: bubble sort
    {
        int size;
        std::cin >> size;
        std::vector<int> arr(size);
        for (auto& elem : arr) {
            std::cin >> elem;
        }
        auto before = Clock::now();
        BubbleSort(arr);
        auto after = Clock::now();
        if (size <= 20) {
            PrettyPrint(arr);
        }
        std::cout << "Bubble sort: " << ToSeconds(after - before) << "\n\n";
    }

    // Case 2: insertion sort
    {
        int size;
        std::cin >> size;
        std::vector<int> arr(size);
        for (auto& elem : arr) {
            std::cin >> elem;
        }
        auto before = Clock::now();
        InsertionSort(arr);
        auto after = Clock::now();
        if (size <= 20) {
            PrettyPrint(arr);
        }
        std::cout << "Insertion sort: " << ToSeconds(after - before) << "\n\n";
    }

    // Case 3: selection sort
    {
        int size;
        std::cin >> size;
        std::vector<int> arr(size);
        for (auto& elem : arr) {
            std::cin >> elem;
        }
        auto before = Clock::now();
        SelectionSort(arr);
        auto after = Clock::now();
        if (size <= 20) {
            PrettyPrint(arr);
        }
        std::cout << "Selection sort: " << ToSeconds(after - before) << "\n\n";
    }

    // Case 4: shell sort
    {
        int size;
        std::cin >> size;
        std::vector<int> arr(size);
        for (auto& elem : arr) {
            std::cin >> elem;
        }
        auto before = Clock::now();
        ShellSort(arr);
        auto after = Clock::now();
        if (size <= 20) {
            PrettyPrint(arr);
        }
        std::cout << "Shell sort: " << ToSeconds(after - before) << "\n\n";
    }

    // Case 5: quick sort (random pivots)
    {
        int size;
        std::cin >> size;
        std::vector<int> arr(size);
        for (auto& elem : arr) {
            std::cin >> elem;
        }
        auto before = Clock::now();
        QuickSort(arr);
        auto after = Clock::now();
        if (size <= 20) {
            PrettyPrint(arr);
        }
        std::cout << "Quick sort: " << ToSeconds(after - before) << "\n\n";
    }

    // Case 6: merge sort
    {
        int size;
        std::cin >> size;
        std::vector<int> arr(size);
        for (auto& elem : arr) {
            std::cin >> elem;
        }
        auto before = Clock::now();
        MergeSort(arr);
        auto after = Clock::now();
        if (size <= 20) {
            PrettyPrint(arr);
        }
        std::cout << "Merge sort: " << ToSeconds(after - before) << "\n\n";
    }

    // Case 7: heap sort
    {
        int size;
        std::cin >> size;
        std::vector<int> arr(size);
        for (auto& elem : arr) {
            std::cin >> elem;
        }
        auto before = Clock::now();
        HeapSort(arr);
        auto after = Clock::now();
        if (size <= 20) {
            PrettyPrint(arr);
        }
        std::cout << "Heap sort: " << ToSeconds(after - before) << "\n\n";
    }

    // Case 8: count sort
    {
        int size;
        std::cin >> size;
        std::vector<unsigned> arr(size);
        for (auto& elem : arr) {
            std::cin >> elem;
        }
        auto before = Clock::now();
        CountSort(arr);
        auto after = Clock::now();
        if (size <= 20) {
            PrettyPrint(arr);
        }
        std::cout << "Count sort: " << ToSeconds(after - before) << "\n\n";
    }

    // Case 9: radix sort
    {
        int size;
        std::cin >> size;
        std::vector<unsigned> arr(size);
        for (auto& elem : arr) {
            std::cin >> elem;
        }
        auto before = Clock::now();
        RadixSort(arr);
        auto after = Clock::now();
        if (size <= 20) {
            PrettyPrint(arr);
        }
        std::cout << "Radix sort: " << ToSeconds(after - before) << "\n\n";
    }

    return 0;
}
