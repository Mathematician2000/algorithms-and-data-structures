#include <algorithm>
#include <climits>
#include <iostream>
#include <stack>
#include <string>
#include <tuple>
#include <utility>
#include <vector>

template<typename T>
using Table = std::vector<std::vector<T>>;

constexpr int kInf = INT_MAX;
constexpr int kNone = -1;

// Maximum subarray sum: O(n)
int64_t Kadane(const std::vector<int>& arr) {
    int64_t ans = 0, cur = 0;
    for (auto elem : arr) {
        cur += elem;
        ans = std::max(ans, cur);
        if (cur < 0) {
            cur = 0;
        }
    }
    return ans;
}

// Find nearest less elements: O(n)
auto FindNearestLess(const std::vector<int>& arr) {
    int size = arr.size();
    std::vector<int> left(size), right(size);
    std::stack<int> st;
    for (int i = 0; i < size; ++i) {
        while (!st.empty() && arr[st.top()] >= arr[i]) {
            st.pop();
        }
        left[i] = (st.empty() ? 0 : st.top() + 1);
        st.push(i);
    }
    while (!st.empty()) {
        st.pop();
    }
    for (int i = size - 1; i >= 0; --i) {
        while (!st.empty() && arr[st.top()] >= arr[i]) {
            st.pop();
        }
        right[i] = (st.empty() ? size - 1 : st.top() - 1);
        st.push(i);
    }
    return std::make_pair(left, right);
}

// Maximum (by area) True submatrix of boolean matrix: O(mn)
auto MaximumSubmatrix(const Table<bool>& table) {
    int rows = table.size(), cols = table.front().size();
    std::vector<int> heights(cols);
    int lx = 0, ly = 0, rx = 0, ry = 0, mx = kNone;
    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            heights[j] = (table[i][j] ? heights[j] + 1 : 0);
        }
        auto [left, right] = FindNearestLess(heights);
        for (int j = 0; j < cols; ++j) {
            int val = heights[j] * (right[j] - left[j] + 1);
            if (val > mx) {
                mx = val;
                lx = left[j], rx = right[j];
                ly = i - heights[j] + 1, ry = i;
            }
        }
    }
    return std::make_tuple(ly, lx, ry, rx);
}

// Levenshtein (edit) distance: O(mn)
auto LevenshteinDistance(const std::string& lhs, const std::string& rhs) {
    int left_sz = lhs.length(), right_sz = rhs.length();
    Table<int> dist(left_sz + 1, std::vector<int>(right_sz + 1));
    for (int i = 1; i <= left_sz; ++i) {
        dist[i][0] = i;
    }
    for (int j = 1; j <= right_sz; ++j) {
        dist[0][j] = j;
    }
    for (int i = 1; i <= left_sz; ++i) {
        for (int j = 1; j <= right_sz; ++j) {
            dist[i][j] = std::min(std::min(dist[i - 1][j], dist[i][j - 1]) + 1,
                                  dist[i - 1][j - 1] + (lhs[i - 1] != rhs[j - 1]));
        }
    }

    std::vector<std::string> ops;
    std::string emp;
    int i = left_sz, j = right_sz;
    while (i || j) {
        if (i && dist[i][j] == dist[i - 1][j] + 1) {
            ops.push_back(emp + lhs[i - 1] + " -> .");
            --i;
        } else if (j && dist[i][j] == dist[i][j - 1] + 1) {
            ops.push_back(emp + ". -> " + rhs[j - 1]);
            --j;
        } else {
            ops.push_back(emp + lhs[i - 1] + (lhs[i - 1] == rhs[j - 1] ? " = " : " -> ") + rhs[j - 1]);
            --i, --j;
        }
    }
    std::reverse(ops.begin(), ops.end());

    return std::make_pair(dist[left_sz][right_sz], ops);
}

// Longest increasing subsequence: O(n log n)
auto LIS(const std::vector<int>& arr) {
    int size = arr.size();
    std::vector<int> dp(size + 1, kInf), parent(size + 1, kNone), prev(size, kNone);
    dp[0] = -kInf;
    for (int i = 0; i < size; ++i) {
        int j = std::lower_bound(dp.begin(), dp.end(), arr[i]) - dp.begin();
        dp[j] = arr[i];
        parent[j] = i;
        prev[i] = parent[j - 1];
    }

    int len = std::lower_bound(dp.begin(), dp.end(), kInf) - dp.begin() - 1;
    int idx = parent[len];
    std::vector<int> seq = {arr[idx]};
    while (prev[idx] != kNone) {
        idx = prev[idx];
        seq.push_back(arr[idx]);
    }
    std::reverse(seq.begin(), seq.end());
    return seq;
}

int main() {
    // Case #1: maximum subarray sum in O(n)
    {
        int n;
        std::cin >> n;
        std::vector<int> arr(n);
        for (int i = 0; i < n; ++i) {
            std::cin >> arr[i];
        }
        std::cout << Kadane(arr) << "\n\n";
    }

    // Case #2: maximum True submatrix of boolean matrix MxN in O(MN)
    {
        int m, n;
        std::cin >> m >> n;
        Table<bool> table(m, std::vector<bool>(n));
        char elem;
        for (int i = 0; i < m; ++i) {
            for (int j = 0; j < n; ++j) {
                std::cin >> elem;
                table[i][j] = (elem - '0');
            }
        }
        auto [ly, lx, ry, rx] = MaximumSubmatrix(table);
        printf("(%d, %d) - (%d, %d) submatrix contains %d elements\n\n",
               ly + 1, lx + 1, ry + 1, rx + 1, (ry - ly + 1) * (rx - lx + 1));
    }

    // Case #3: edit distance between two strings in O(mn)
    {
        std::string lhs, rhs;
        std::cin >> lhs >> rhs;
        auto [dist, ops] = LevenshteinDistance(lhs, rhs);
        std::cout << dist << " operations needed:\n";
        for (auto op : ops) {
            std::cout << op << '\n';
        }
        std::cout << '\n';
    }

    // Case #4: longest increasing subsequence in O(n log n)
    {
        int n;
        std::cin >> n;
        std::vector<int> arr(n);
        for (int i = 0; i < n; ++i) {
            std::cin >> arr[i];
        }
        auto seq = LIS(arr);
        int k = seq.size();
        std::cout << '[' << seq[0];
        for (int i = 1; i < k; ++i) {
            std::cout << ", " << seq[i];
        }
        std::cout << "]\n";
    }

    return 0;
}
