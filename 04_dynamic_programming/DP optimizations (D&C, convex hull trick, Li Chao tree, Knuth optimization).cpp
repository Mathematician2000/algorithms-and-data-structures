#include <algorithm>
#include <cmath>
#include <complex>
#include <functional>
#include <iostream>
#include <vector>

using Integer = int64_t;
using Point = std::complex<Integer>;

constexpr Point kImaginaryUnit = {0, 1};
constexpr Integer kNone = -1;
constexpr Integer kInf = 1e9;

// Square function
inline Integer Sqr(Integer x) {
    return x * x;
}

// Baseline solution for 'squared lengths of segments' problem in O(m n^2)
// where n is size of array, m is number of segments required
Integer SquaredLengthBaselineDP(const std::function<Integer(int, int)>& cost,
                                int size, int segments) {
    std::vector<std::vector<Integer>> dp(segments + 1, std::vector<Integer>(size + 1, kInf));
    for (int i = 0; i <= segments; ++i) {
        dp[i][0] = 0;
    }
    for (int i = 1; i <= segments; ++i) {
        for (int j = 1; j <= size; ++j) {
            for (int k = 0; k < j; ++k) {
                dp[i][j] = std::min(dp[i][j], dp[i - 1][k] + cost(k + 1, j));
            }
        }
    }
    return dp[segments][size];
}

// Calculate one row (iteration) of DP in O(n log n)
void OneRowDC(const std::vector<Integer>& dp_old,
              std::vector<Integer>& dp_new,
              const std::function<Integer(int, int)>& cost,
              int i, int j, int left, int right) {
    int mid = (i + j) / 2, opt_idx = mid;
    Integer mn_cost = kNone;
    for (int idx = left; idx <= std::min(mid, right); ++idx) {
        Integer val = dp_old[idx] + cost(idx + 1, mid);
        if (mn_cost == kNone || mn_cost > val) {
            mn_cost = val;
            opt_idx = idx;
        }
    }
    dp_new[mid] = mn_cost;
    if (i < mid) {
        OneRowDC(dp_old, dp_new, cost, i, mid - 1, left, opt_idx);
    }
    if (j > mid) {
        OneRowDC(dp_old, dp_new, cost, mid + 1, j, opt_idx, right);
    }
}

// D&C optimization: O(mn log n)
// where n is size of array, m is number of DP iterations (rows)
Integer DivideAndConquer(const std::function<Integer(int, int)>& cost,
                         int size, int iters) {
    std::vector<Integer> dp_old(size + 1), dp_new(size + 1);
    for (int j = 1; j <= size; ++j) {
        dp_new[j] = cost(1, j);
    }
    for (int i = 2; i <= iters; ++i) {
        dp_old.swap(dp_new);
        OneRowDC(dp_old, dp_new, cost, 1, size, 1, size);
    }
    return dp_new.back();
}

// Dot (scalar) product of two vectors
inline Integer Dot(const Point& lhs, const Point& rhs) {
    return (std::conj(lhs) * rhs).real();
}

// Cross (vector) product of two vectors
inline Integer Cross(const Point& lhs, const Point& rhs) {
    return (std::conj(lhs) * rhs).imag();
}

// Calculate dot product (linear function)
inline int Apply(Point point, int x) {
    return Dot(point, {x, 1});
}

// Add a line to convex hull: assume scope is increasing
void ConvexHullAddLine(std::vector<Point>& hull,
                       std::vector<Point>& norms,
                       Integer scope, Integer bias) {
    Point x = {scope, bias};
    while (!norms.empty() && Dot(norms.back(), x - hull.back()) < 0) {
        hull.pop_back();
        norms.pop_back();
    }
    if (!hull.empty()) {
        norms.push_back(kImaginaryUnit * (x - hull.back()));
    }
    hull.push_back(x);
}

// Get minimum value at point x in the convex hull: assume k is increasing
Integer ConvexHullGetMinimum(const std::vector<Point>& hull,
                             const std::vector<Point>& norms,
                             Integer x) {
    Point line = {x, 1};
    auto idx = std::lower_bound(norms.begin(), norms.end(), line,
                                [](auto a, auto b) { return Cross(a, b) > 0; })
               - norms.begin();
    return Dot(line, hull[idx]);
}

// Convex hull trick in O(n log n)
auto ConvexHullTrick(const std::vector<Integer>& scopes,
                     const std::vector<Integer>& biases,
                     const std::vector<Integer>& xs) {
    int size = scopes.size();
    std::vector<Integer> dp(size);
    std::vector<Point> hull, norms;
    for (int i = 0; i < size; ++i) {
        ConvexHullAddLine(hull, norms, scopes[i], biases[i]);
        dp[i] = ConvexHullGetMinimum(hull, norms, xs[i]);
    }
    return dp;
}

// Li Chao tree for minimization problems (linear case)
class LiChaoTree {
    std::vector<Point> tree_;
    int size_;

public:
    explicit LiChaoTree(int size) {
        size_ = 1;
        while (size_ < size) {
            size_ *= 2;
        }
        tree_.assign(2 * size_ - 1, {0, kInf});
    }

    void AddLine(Point point, int x = 0, int left = 0, int right = kNone) {
        if (right == kNone) {
            right = size_;
        }
        int mid = (left + right) / 2;
        bool cmp_left = (Apply(point, left) < Apply(tree_[x], left));
        bool cmp_mid = (Apply(point, mid) < Apply(tree_[x], mid));
        if (cmp_mid) {
            std::swap(tree_[x], point);
        }
        if (left + 1 != right) {
            if (cmp_left != cmp_mid) {
                AddLine(point, 2 * x + 1, left, mid);
            } else {
                AddLine(point, 2 * x + 2, mid, right);
            }
        }
    }

    Integer GetMinimum(int point, int x = 0, int left = 0, int right = kNone) const {
        if (right == kNone) {
            right = size_;
        }
        Integer ans = Apply(tree_[x], point);
        if (left + 1 != right) {
            int mid = (left + right) / 2;
            Integer val = x < mid
                          ? GetMinimum(point, 2 * x + 1, left, mid)
                          : GetMinimum(point, 2 * x + 2, mid, right);
            ans = std::min(ans, val);
        }
        return ans;
    }
};

// Knuth optimization: O(mn)
// where n is size of array, m is number of DP iterations
Integer KnuthOptimization(const std::function<Integer(int, int)>& cost, int size) {
    std::vector<std::vector<Integer>> dp(size + 1, std::vector<Integer>(size + 1, kInf));
    std::vector<std::vector<int>> opt(size + 1, std::vector<int>(size + 1));
    for (int i = 1; i <= size; ++i) {
        dp[i][i] = cost(i, i);
        opt[i][i] = i;
    }
    for (int len = 2; len <= size; ++len) {
        for (int i = 1, j = len; j <= size; ++i, ++j) {
            for (int k = opt[i][j - 1]; k <= std::min(opt[i + 1][j], j - 1); ++k) {
                Integer val = dp[i][k] + dp[k + 1][j];
                if (val < dp[i][j]) {
                    dp[i][j] = val, opt[i][j] = k;
                }
                dp[i][j] += cost(i, j);
            }
        }
    }
    return dp[1][size];
}

int main() {
    // Case #1: baseline solution
    {
        // minimize sum of squared lengths of m segments covering n points
        int n, m;
        std::cin >> n >> m;
        std::vector<Integer> xs(n);
        for (int i = 0; i < n; ++i) {
            std::cin >> xs[i];
        }
        std::sort(xs.begin(), xs.end());
        auto cost = [&xs](int i, int j) -> Integer { return Sqr(xs[j - 1] - xs[i - 1]); };
        std::cout << SquaredLengthBaselineDP(cost, n, m) << "\n\n";
    }

    // Case #2: divide and conquer
    {
        // minimize sum of squared lengths of m segments covering n points
        int n, m;
        std::cin >> n >> m;
        std::vector<Integer> xs(n);
        for (int i = 0; i < n; ++i) {
            std::cin >> xs[i];
        }
        std::sort(xs.begin(), xs.end());
        auto cost = [&xs](int i, int j) -> Integer { return Sqr(xs[j - 1] - xs[i - 1]); };
        std::cout << DivideAndConquer(cost, n, m) << "\n\n";
    }

    // Case #3: convex hull trick
    {
        int n;
        std::cin >> n;
        std::vector<Integer> scopes(n), biases(n), xs(n);
        for (int i = 0; i < n; ++i) {
            std::cin >> scopes[i] >> biases[i] >> xs[i];
        }
        for (auto elem : ConvexHullTrick(scopes, biases, xs)) {
            std::cout << elem << ' ';
        }
        std::cout << "\n\n";
    }

    // Case #4: Li Chao tree
    {
        int n, m;
        std::cin >> n >> m;
        std::vector<Integer> scopes(n), biases(n), xs(n);
        for (int i = 0; i < n; ++i) {
            std::cin >> scopes[i] >> biases[i] >> xs[i];
        }
        LiChaoTree tree(*std::max_element(xs.begin(), xs.end()));  // suppose xs[i] > 0 for all i
        for (int i = 0; i < n; ++i) {
            tree.AddLine({scopes[i], biases[i]});
            std::cout << tree.GetMinimum(xs[i]) << ' ';
        }
        std::cout << "\n\n";
    }

    // Case #5: Knuth optimization
    {
        // Knuth division: minimize sum of squared sums of elements in arrays
        int n;
        std::cin >> n;
        std::vector<Integer> pref(n + 1);
        for (int i = 1; i <= n; ++i) {
            std::cin >> pref[i];
            pref[i] += pref[i - 1];
        }
        auto cost = [&pref](int i, int j) -> Integer { return Sqr(pref[j] - pref[i - 1]); };
        std::cout << KnuthOptimization(cost, n) << '\n';
    }

    return 0;
}
