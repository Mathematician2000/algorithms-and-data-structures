#include <iostream>
#include <vector>

// Stick Game (see CSES 1729)
auto StickGame(const std::vector<int>& ps, int n) {
    int k = ps.size();
    std::vector<bool> dp(n + 1);
    for (int i = 1; i <= n; ++i) {
        bool win = false;
        for (int j = 0; !win && j < k; ++j) {
            if (i >= ps[j]) {
                win |= !dp[i - ps[j]];
            }
        }
        dp[i] = win;
    }
    return dp;
}

// Find mex in O(n)
int MEX(std::vector<int>& vals) {
    int n = vals.size();
    for (int i = 0; i < n; ++i) {
        while (vals[i] < n && vals[vals[i]] != vals[i]) {
            std::swap(vals[i], vals[vals[i]]);
        }
    }
    for (int i = 0; i < n; ++i) {
        if (vals[i] != i) {
            return i;
        }
    }
    return n;
}

// Grundy's game (see CSES 2207)
auto GrundyGame(int n) {
    std::vector<int> gs(n + 1), vals;
    for (int i = 3; i <= n; ++i) {
        vals.clear();
        for (int j = 1; j < (i + 1) / 2; ++j) {
            vals.push_back(gs[j] ^ gs[i - j]);
        }
        gs[i] = MEX(vals);
    }
    return gs;
}

int main() {
    // Case #1: stick game (retrospective analysis)
    {
        int n, k;
        std::cin >> n >> k;
        std::vector<int> ps(k);
        for (int i = 0; i < k; ++i) {
            std::cin >> ps[i];
        }
        auto dp = StickGame(ps, n);
        for (int i = 1; i <= n; ++i) {
            std::cout << (dp[i] ? 'W' : 'L');
        }
        std::cout << "\n\n";
    }

    // Case #2: Grundy values for Grundy's game
    {
        int n;
        std::cin >> n;
        auto gs = GrundyGame(n);
        for (int i = 0; i <= n; ++i) {
            // it's conjectured that gs[i] > 0 for all i > 1222
            printf("gs[%d] = %d\n", i, gs[i]);
        }
    }

    return 0;
}
