#include <algorithm>
#include <iostream>
#include <tuple>
#include <utility>
#include <vector>

/*
* See also: https://neerc.ifmo.ru/wiki/index.php?title=Задача_о_рюкзаке
* - Bounded Knapsack Problem
* - Unbounded Knapsack Problem
* - Continuous Knapsack Problem
* - Value Independent Knapsack Problem (SSP = Subset Sum Problem)
* - Change-Making Problem
* - Bin Packing Problem
* - Multiple Knapsack Problem
* - Generalized Assignment Problem
*/

template<typename T>
using Table = std::vector<std::vector<T>>;

// Convert a binary mask into an array of indices
auto MaskToIndices(int mask, int size) {
    std::vector<int> ids;
    for (int i = 0; i < size; ++i) {
        if ((mask >> i) & 1) {
            ids.push_back(i + 1);
        }
    }
    return ids;
}

// Knapsack problem (brute-force): O(n 2^n)
auto KnapsackBruteForce(const std::vector<int>& weights,
                        const std::vector<int>& costs,
                        int max_weight) {
    int size = weights.size(), max_mask = 0, max_cost = 0;
    for (int mask = 1; mask < (1 << size); ++mask) {
        int cost = 0, weight = 0;
        for (int i = 0; i < size; ++i) {
            if ((mask >> i) & 1) {
                weight += weights[i];
                cost += costs[i];
            }
        }
        if (weight <= max_weight && cost > max_cost) {
            max_cost = cost, max_mask = mask;
        }
    }
    return std::make_pair(max_cost, max_mask);
}

// Knapsack problem: meet-in-the-middle in O(2^(n/2))
auto KnapsackMeetInTheMiddle(const std::vector<int>& weights,
                             const std::vector<int>& costs,
                             int max_weight) {
    int size = weights.size(), half = size / 2;
    std::vector<std::tuple<int, int, int>> vals;  // (weight, cost, mask)
    for (int mask = 0; mask < (1 << half); ++mask) {
        int weight = 0, cost = 0;
        for (int i = 0; i < half; ++i) {
            if ((mask >> i) & 1) {
                weight += weights[i];
                cost += costs[i];
            }
        }
        if (weight <= max_weight) {
            vals.emplace_back(weight, cost, mask);
        }
    }

    std::sort(vals.begin(), vals.end());
    int k = vals.size();
    for (int i = 1, j = 1; j < k; ++j) {
        while (j < k && std::get<1>(vals[j]) <= std::get<1>(vals[i - 1])) {
            ++j;
        }
        if (j == k) {
            vals.erase(vals.begin() + i, vals.end());
            break;
        }
        vals[i++] = vals[j];
    }

    int max_mask = 0, max_cost = 0;
    for (int mask = 0; mask < (1 << (size - half)); ++mask) {
        int left_weight = 0, left_cost = 0, left_mask = mask << half;
        for (int i = half; i < size; ++i) {
            if ((left_mask >> i) & 1) {
                left_weight += weights[i];
                left_cost += costs[i];
            }
        }

        std::tuple<int, int, int> elem = {max_weight - left_weight, 0, 0};
        int idx = std::upper_bound(vals.begin(), vals.end(), elem) - vals.begin() - 1;
        if (idx != -1) {
            auto [right_weight, right_cost, right_mask] = vals[idx];
            if (left_cost + right_cost > max_cost) {
                max_cost = left_cost + right_cost, max_mask = left_mask | right_mask;
            }
        }
    }

    return std::make_pair(max_cost, max_mask);
}

// Knapsack problem: dp in O(n * max_weight)
auto KnapsackDP(const std::vector<int>& weights,
                const std::vector<int>& costs,
                int max_weight) {
    int size = weights.size();
    Table<int> dp(size + 1, std::vector<int>(max_weight + 1));
    for (int i = 1; i <= size; ++i) {
        for (int j = 1; j < weights[i - 1]; ++j) {
            dp[i][j] = dp[i - 1][j];
        }
        for (int j = weights[i - 1]; j <= max_weight; ++j) {
            dp[i][j] = std::max(dp[i - 1][j], dp[i - 1][j - weights[i - 1]] + costs[i - 1]);
        }
    }

    int max_mask = 0;
    for (int i = size, j = max_weight; i && j; --i) {
        if (dp[i][j] != dp[i - 1][j]) {
            max_mask |= (1 << (i - 1));
            j -= weights[i - 1];
        }
    }

    return std::make_pair(dp[size][max_weight], max_mask);
}

int main() {
    // Case #1: knapsack with brute-force in O(2^n)
    {
        int size, max_weight;
        std::cin >> size >> max_weight;
        std::vector<int> weights(size), costs(size);
        for (int i = 0; i < size; ++i) {
            std::cin >> weights[i] >> costs[i];
        }
        auto [cost, mask] = KnapsackBruteForce(weights, costs, max_weight);
        auto ids = MaskToIndices(mask, size);
        std::cout << "Max cost is " << cost << "\n[" << ids[0];
        for (int i = 1; i < static_cast<int>(ids.size()); ++i) {
            std::cout << ", " << ids[i];
        }
        std::cout << "]\n\n";
    }

    // Case #2: knapsack with meet-in-the-middle in O(2^(n/2))
    {
        int size, max_weight;
        std::cin >> size >> max_weight;
        std::vector<int> weights(size), costs(size);
        for (int i = 0; i < size; ++i) {
            std::cin >> weights[i] >> costs[i];
        }
        auto [cost, mask] = KnapsackMeetInTheMiddle(weights, costs, max_weight);
        auto ids = MaskToIndices(mask, size);
        std::cout << "Max cost is " << cost << "\n[" << ids[0];
        for (int i = 1; i < static_cast<int>(ids.size()); ++i) {
            std::cout << ", " << ids[i];
        }
        std::cout << "]\n\n";
    }

    // Case #3: knapsack with dp in O(n * max_weight)
    {
        int size, max_weight;
        std::cin >> size >> max_weight;
        std::vector<int> weights(size), costs(size);
        for (int i = 0; i < size; ++i) {
            std::cin >> weights[i] >> costs[i];
        }
        auto [cost, mask] = KnapsackDP(weights, costs, max_weight);
        auto ids = MaskToIndices(mask, size);
        std::cout << "Max cost is " << cost << "\n[" << ids[0];
        for (int i = 1; i < static_cast<int>(ids.size()); ++i) {
            std::cout << ", " << ids[i];
        }
        std::cout << "]\n\n";
    }

    return 0;
}
