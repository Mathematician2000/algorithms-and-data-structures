#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <set>
#include <tuple>
#include <utility>
#include <vector>

constexpr double kEps = 1e-6;
constexpr double kPi = 3.14159265358979323846264;
constexpr int kNone = -1;

// Square function
inline double Sqr(double x) {
    return x * x;
}

// Sign function
inline int Sign(double x) {
    return x > 0 ? +1 : (x < 0 ? -1 : 0);
}

struct Point {
    double x, y;

    Point(double x = 0, double y = 0) : x(x), y(y) {
    }

    friend std::istream& operator>>(std::istream& in, Point& point) {
        return in >> point.x >> point.y;
    }

    friend std::ostream& operator<<(std::ostream& out, const Point& point) {
        return out << '(' << point.x << ", " << point.y << ')';
    }

    friend double Dot(const Point& lhs, const Point& rhs) {
        return lhs.x * rhs.x + lhs.y * rhs.y;
    }

    friend double Cross(const Point& lhs, const Point& rhs) {
        return lhs.x * rhs.y - lhs.y * rhs.x;
    }

    friend const Point operator-(const Point& point) {
        return {-point.x, -point.y};
    }

    friend const Point operator+(const Point& lhs, const Point& rhs) {
        return {lhs.x + rhs.x, lhs.y + rhs.y};
    }

    friend const Point operator-(const Point& lhs, const Point& rhs) {
        return lhs + (-rhs);
    }

    friend const Point operator*(const Point& point, double mult) {
        return {point.x * mult, point.y * mult};
    }

    friend const Point operator*(double mult, const Point& point) {
        return point * mult;
    }

    friend Point& operator+=(Point& lhs, const Point& rhs) {
        return lhs = lhs + rhs;
    }

    friend Point& operator-=(Point& lhs, const Point& rhs) {
        return lhs = lhs - rhs;
    }

    friend Point& operator*=(Point& point, double mult) {
        return point = point * mult;
    }

    friend bool operator<(const Point& lhs, const Point& rhs) {
        return lhs.x < rhs.x || (lhs.x == rhs.x && lhs.y < rhs.y);
    }

    operator bool() const {
        return std::max(std::fabs(x), std::fabs(y)) > kEps;
    }

    friend double Norm(const Point& point) {
        return std::sqrt(Dot(point, point));
    }

    friend double Dist(const Point& lhs, const Point& rhs) {
        return Norm(lhs - rhs);
    }

    friend double Angle(const Point& lhs, const Point& rhs) {
        return !lhs || !rhs ? 0 : std::acos(Dot(lhs, rhs) / (Norm(lhs) * Norm(rhs)));
    }

    friend const Point Rotate(const Point& point, double phi) {
        double cos = std::cos(phi), sin = std::sin(phi);
        return {point.x * cos - point.y * sin, point.x * sin + point.y * cos};
    }
};

using Segment = std::pair<Point, Point>;

// ax + by + c = 0
struct Line {
    double a = 0, b = 0, c = 0;

    Line() = default;
    Line(double a, double b, double c) : a(a), b(b), c(c) {}
    Line(const Point& p1, const Point& p2)
            : a(p1.y - p2.y), b(p2.x - p1.x), c(-(a * p1.x + b * p1.y)) {}
    Line(const Segment& s) : Line(s.first, s.second) {}

    friend bool operator==(const Line& lhs, const Line& rhs) {
        return std::fabs(lhs.a - rhs.a) < kEps &&
               std::fabs(lhs.b - rhs.b) < kEps &&
               std::fabs(lhs.c - rhs.c) < kEps;
    }

    friend double Evaluate(const Line& line, const Point& point) {
        return line.a * point.x + line.b * point.y + line.c;
    }

    friend const Point Norm(const Line& line) {
        return {line.a, line.b};
    }

    friend const Point Direction(const Line& line) {
        return Rotate(Norm(line), kPi / 2);
    }

    friend const Point Initial(const Line& line) {
        if (std::fabs(line.a) > kEps) {
            return {-line.c / line.a, 0};
        } else {
            return {0, -line.c / line.b};
        }
    }

    friend double Dist(const Line& line, const Point& point) {
        return std::fabs(Evaluate(line, point)) / Norm(Norm(line));
    }

    friend const Point Intersect(const Line& lhs, const Line& rhs) {
        Point r1 = Initial(lhs), r2 = Initial(rhs);
        Point a1 = Direction(lhs), a2 = Direction(rhs);
        double cross = Cross(a1, a2);
        if (std::fabs(cross) > kEps) {
            return r1 + Cross(r2 - r1, a2) / cross * a1;
        } else {
            throw std::invalid_argument("Line::intersect: lines are parallel or equivalent");
        }
    }

    friend double Angle(const Line& lhs, const Line& rhs) {
        return Angle(Norm(lhs), Norm(rhs));
    }

    friend const Point Project(const Line& line, const Point& point) {
        Point r = Initial(line), a = Direction(line);
        double t = Dot(point - r, a) / Dot(a, a);
        return r + t * a;
    }

    friend const Line Translate(const Line& line, const Point& point) {
        return {line.a, line.b, line.c - Dot(Norm(line), point)};
    }
};

struct Circle {
    Point center;
    double radius;

    Circle(const Point& center = {}, double radius = 0) : center(center), radius(radius) {
    }

    friend bool operator==(const Circle& lhs, const Circle& rhs) {
        return lhs.center == rhs.center && std::fabs(lhs.radius - rhs.radius) < kEps;
    }

    friend std::vector<Point> Intersect(const Circle& circ, const Line& line) {
        if (circ.center) {
            Circle tr_circ({}, circ.radius);
            Line tr_line = Translate(line, -circ.center);
            auto ans = Intersect(tr_circ, tr_line);
            for (auto& point : ans) {
                point += circ.center;
            }
            return ans;
        }

        std::vector<Point> ans;
        double norm = Norm(Norm(line));
        double d0 = std::fabs(line.c) / norm;
        if (d0 > circ.radius) {
            return ans;
        }
        double x0 = -(line.a * line.c) / Sqr(norm);
        double y0 = -(line.b * line.c) / Sqr(norm);
        if (std::fabs(d0 - circ.radius) < kEps) {
            ans.emplace_back(x0, y0);
            return ans;
        }
        double d = std::sqrt(Sqr(circ.radius) - Sqr(d0));
        double m = d / norm;
        ans.emplace_back(x0 + line.b * m, y0 - line.a * m);
        ans.emplace_back(x0 - line.b * m, y0 + line.a * m);
        return ans;
    }

    friend std::vector<Point> Intersect(const Circle& lhs, const Circle& rhs) {
        if (lhs.center) {
            Circle c3({}, lhs.radius), c4(rhs.center - lhs.center, rhs.radius);
            auto ans = Intersect(c3, c4);
            for (auto& point : ans) {
                point += lhs.center;
            }
            return ans;
        }
        return Intersect(lhs, Line(-2 * rhs.center.x, -2 * rhs.center.y,
                                   Dot(rhs.center, rhs.center) + Sqr(lhs.radius) - Sqr(rhs.radius)));
    }

    friend std::vector<Line> FindTangents(const Circle& lhs, const Circle& rhs) {
        if (lhs == rhs || Dist(lhs.center, rhs.center) + std::min(lhs.radius, rhs.radius) < std::max(lhs.radius, rhs.radius)) {
            return {};  // degenerate cases
        }

        if (lhs.center) {
            Circle c1({}, lhs.radius), c2(rhs.center - lhs.center, rhs.radius);
            auto ans = FindTangents(c1, c2);
            for (auto& line : ans) {
                line = Translate(line, lhs.center);
            }
            return ans;
        }

        std::vector<Line> ans;
        for (int s1 = -1; s1 <= 1; s1 += 2) {
            for (int s2 = -1; s2 <= 1; s2 += 2) {
                double d1 = lhs.radius * s1, d2 = rhs.radius * s2;
                double D = Dot(rhs.center, rhs.center) - Sqr(d2 - d1);
                if (D >= 0) {
                    double a = ((d2 - d1) * rhs.center.x + rhs.center.y * std::sqrt(D)) / Dot(rhs.center, rhs.center);
                    double b = ((d2 - d1) * rhs.center.y - rhs.center.x * std::sqrt(D)) / Dot(rhs.center, rhs.center);
                    Line new_line(a, b, d1);
                    bool found = false;
                    for (const auto& line : ans) {
                        if (line == new_line) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        ans.push_back(new_line);
                    }
                }
            }
        }
        return ans;
    }

    friend std::vector<Line> FindTangents(const Circle& circle, const Point& point) {
        return FindTangents(circle, Circle(point));
    }

    friend double Dist(const Circle& circle, const Point& point) {
        return std::fabs(Dist(circle.center, point) - circle.radius);
    }

    friend double Dist(const Circle& circle, const Line& line) {
        double dist = Dist(circle.center, Project(line, circle.center));
        return std::max(0., dist - circle.radius);
    }

    friend double Dist(const Circle& lhs, const Circle& rhs) {
        double dist = Dist(lhs.center, rhs.center);
        return std::max(0., dist - lhs.radius - rhs.radius);
    }
};

// Check whether two 1D segments intersect or not
bool CheckIntersection(double x1, double x2, double x3, double x4) {
    return std::max(x1, x3) <= std::min(x2, x4);
}

// Intersect 1D segments
auto Intersect(double x1, double x2, double x3, double x4) {
    if (!CheckIntersection(x1, x2, x3, x4)) {
        throw std::invalid_argument("1D::intersect: segments do not intersect");
    }
    return std::make_pair(std::max(x1, x3), std::min(x2, x4));
}

// Check whether two 2D segments intersect or not
bool CheckIntersection(const Point& p1, const Point& p2, const Point& p3, const Point& p4) {
    if (std::max(std::fabs(Cross(p4 - p1, p4 - p3)), std::fabs(Cross(p4 - p2, p4 - p3))) < kEps) {
        return CheckIntersection(p1.x, p2.x, p3.x, p4.x) &&
               CheckIntersection(p1.y, p2.y, p3.y, p4.y);
    }
    return Sign(Cross(p2 - p1, p3 - p1)) != Sign(Cross(p2 - p1, p4 - p1)) &&
           Sign(Cross(p4 - p3, p1 - p3)) != Sign(Cross(p4 - p3, p2 - p3));
}

bool CheckIntersection(const Segment& lhs, const Segment& rhs) {
    return CheckIntersection(lhs.first, lhs.second, rhs.first, rhs.second);
}

// Intersect 2D segments
const Point Intersect(const Point& p1, const Point& p2, const Point& p3, const Point& p4) {
    if (!CheckIntersection(p1, p2, p3, p4)) {
        throw std::invalid_argument("2D::intersect: segments do not intersect");
    }
    Line l1(p1, p2), l2(p3, p4);
    return Intersect(l1, l2);
}

const Point Intersect(const Segment& lhs, const Segment& rhs) {
    return Intersect(lhs.first, lhs.second, rhs.first, rhs.second);
}

// Find any two intersecting 2D segments using scan-line in O(n log n)
auto FindIntersectingSegments(const std::vector<Segment>& arr) {
    int size = arr.size();
    std::vector<std::tuple<double, bool, double, int>> events;
    for (int i = 0; i < size; ++i) {
        auto [p1, p2] = arr[i];
        auto [x1, y1] = p1;
        auto [x2, y2] = p2;
        events.emplace_back(x1, false, y1, i);
        events.emplace_back(x2, true, y2, i);
    }

    std::sort(events.begin(), events.end());
    std::multiset<std::pair<double, int>> set;
    for (auto [x, type, y, idx] : events) {
        if (!type) {  // left
            set.emplace(y, idx);
        } else {  // right
            set.erase({arr[idx].first.y, idx});
            auto it = set.lower_bound({y, 0});
            if (it != set.end()) {
                auto [_, i] = *it;
                if (CheckIntersection(arr[i], arr[idx])) {
                    return std::make_pair(i, idx);
                }
            }
            if (it != set.begin()) {
                auto [_, i] = *--it;
                if (CheckIntersection(arr[i], arr[idx])) {
                    return std::make_pair(i, idx);
                }
            }
        }
    }

    return std::make_pair(kNone, kNone);
}

// Find the distance between a point and a 2D segment
double Dist(const Point& left, const Point& right, const Point& point) {
    Line line(left, right);
    Point proj = Project(line, point);
    if (!CheckIntersection(left.x, right.x, proj.x, proj.x) ||
            !CheckIntersection(left.y, right.y, proj.y, proj.y)) {
        return std::min(Dist(left, point), Dist(right, point));
    }
    return Dist(point, proj);
}

double Dist(const Segment& segment, const Point& point) {
    return Dist(segment.first, segment.second, point);
}

int main() {
    std::cout << std::fixed << std::setprecision(3);

    // Case 1: basic operations on points and vectors in 2D
    {
        Point p1, p2;
        double k, phi;
        std::cin >> p1 >> p2 >> k >> phi;

        std::cout << "p1 + p2 = " << p1 + p2 << '\n';
        std::cout << "p1 - p2 = " << p1 - p2 << '\n';
        std::cout << "p1 * k = " << p1 * k << '\n';
        std::cout << "(p1, p2) = " << Dot(p1, p2) << '\n';
        std::cout << "[p1, p2] = " << Cross(p1, p2) << '\n';
        std::cout << "||p1|| = " << Norm(p1) << '\n';
        std::cout << "d(p1, p2) = " << Dist(p1, p2) << '\n';
        std::cout << "r(p1, phi) = " << Rotate(p1, phi * kPi / 180) << "\n\n";
    }

    // Case 2: basic operations on lines and segments in 2D
    {
        Point p1, p2, p3;
        std::cin >> p1 >> p2 >> p3;
        Line line(p1, p2);

        printf("Line(p1, p2): a = %.3lf, b = %.3lf, c = %.3lf\n", line.a, line.b, line.c);
        std::cout << "norm = " << Norm(line) << '\n';
        std::cout << "direction = " << Direction(line) << '\n';
        std::cout << "initial = " << Initial(line) << '\n';
        std::cout << "d(Line(p1, p2), p3) = " << Dist(line, p3) << '\n';
        std::cout << "d(Segment(p1, p2), p3) = " << Dist(Segment(p1, p2), p3) << '\n';
        std::cout << "projection p3 on Line(p1, p2) = " << Project(line, p3) << '\n';
        Line tr_line = Translate(line, p3);
        printf("translation by p3: a = %.3lf, b = %.3lf, c = %.3lf\n\n",
               tr_line.a, tr_line.b, tr_line.c);
    }

    // Case 3: intersections, angles and tangents
    {
        Point p1, p2, p3, p4;
        std::cin >> p1 >> p2 >> p3 >> p4;
        Line l1(p1, p2), l2(p3, p4);
        Segment s1(p1, p2), s2(p3, p4);

        try {
            std::cout << "l1 \\cap l2 = " << Intersect(l1, l2) << '\n';
        } catch(std::exception& e) {
            std::cout << e.what() << '\n';
        }

        std::cout << "phi(l1, l2) = " << Angle(l1, l2) / kPi * 180 << '\n';

        try {
            std::cout << "s1 \\cap s2 = " << Intersect(s1, s2) << "\n\n";
        } catch(std::exception& e) {
            std::cout << e.what() << "\n\n";
        }
    }

    // Case 4: find two intersecting 2D segments in O(n log n)
    {
        int n;
        std::cin >> n;
        std::vector<Segment> arr(n);
        for (int i = 0; i < n; ++i) {
            std::cin >> arr[i].first >> arr[i].second;
        }

        auto [i, j] = FindIntersectingSegments(arr);
        if (i == kNone) {
            std::cout << "No two segments intersect\n\n";
        } else {
            std::cout << "arr[" << i << "] \\cap arr[" << j << "] = ";
            std::cout << Intersect(arr[i], arr[j]) << "\n\n";
        }
    }

    // Case 5: circles and tangents
    {
        Point p1, p2, p3;
        double r1, r2;
        std::cin >> p1 >> p2 >> p3 >> r1 >> r2;
        Circle c1(p1, r1), c2(p2, r2);
        Line line(p2, p3);

        auto ps_1 = Intersect(c1, c2);
        std::cout << "c1 \\cap c2 = { ";
        for (const auto& p : ps_1) {
            std::cout << p << ' ';
        }
        std::cout << "}\n";

        auto ps_2 = Intersect(c1, line);
        std::cout << "c1 \\cap line = { ";
        for (const auto& p : ps_2) {
            std::cout << p << ' ';
        }
        std::cout << "}\n";

        auto tgs_1 = FindTangents(c1, c2);
        std::cout << "tgs(c1, c2) = { ";
        for (const auto& l : tgs_1) {
            printf("(%.3lf, %.3lf, %.3lf) ", l.a, l.b, l.c);
        }
        std::cout << "}\n";

        auto tgs_2 = FindTangents(c1, p3);
        std::cout << "tgs(c1, p3) = { ";
        for (const auto& l : tgs_2) {
            printf("(%.3lf, %.3lf, %.3lf) ", l.a, l.b, l.c);
        }
        std::cout << "}\n";

        std::cout << "d(c1, line) = " << Dist(c1, line) << '\n';
        std::cout << "d(c1, c2) = " << Dist(c1, c2) << '\n';
        std::cout << "d(c1, p3) = " << Dist(c1, p3) << '\n';
    }

    return 0;
}
