#include <algorithm>
#include <cmath>
#include <iostream>
#include <set>
#include <stack>
#include <tuple>
#include <vector>

constexpr int kNone = -1;

struct Point {
    int x, y;

    Point(int x = 0, int y = 0) : x(x), y(y) {
    }

    friend std::istream& operator>>(std::istream& in, Point& point) {
        return in >> point.x >> point.y;
    }

    friend std::ostream& operator<<(std::ostream& out, const Point& point) {
        return out << '(' << point.x << ", " << point.y << ')';
    }

    friend int Dot(const Point& lhs, const Point& rhs) {
        return lhs.x * rhs.x + lhs.y * rhs.y;
    }

    friend int Cross(const Point& lhs, const Point& rhs) {
        return lhs.x * rhs.y - lhs.y * rhs.x;
    }

    friend const Point operator-(const Point& point) {
        return {-point.x, -point.y};
    }

    friend const Point operator+(const Point& lhs, const Point& rhs) {
        return {lhs.x + rhs.x, lhs.y + rhs.y};
    }

    friend const Point operator-(const Point& lhs, const Point& rhs) {
        return lhs + (-rhs);
    }

    friend Point& operator+=(Point& lhs, const Point& rhs) {
        return lhs = lhs + rhs;
    }

    friend Point& operator-=(Point& lhs, const Point& rhs) {
        return lhs = lhs - rhs;
    }

    friend bool operator<(const Point& lhs, const Point& rhs) {
        return lhs.x < rhs.x || (lhs.x == rhs.x && lhs.y < rhs.y);
    }

    friend int SquaredNorm(const Point& point) {
        return Dot(point, point);
    }

    friend double Norm(const Point& point) {
        return std::sqrt(SquaredNorm(point));
    }
};

// Auxiliary comparison functor
// This can be easily created from lambda using decltype
struct CmpByOrdinate {
    bool operator()(const Point& lhs, const Point& rhs) const {
        return lhs.y < rhs.y || (lhs.y == rhs.y && lhs.x < rhs.x);
    }
};

// Find minimum Euclidean distance between two points using Preparata's D&C algorithm in O(n log n)
void MinimumDistanceDC(std::vector<Point>& arr,
                       std::vector<Point>& buf,
                       double& dmin,
                       int left = 0,
                       int right = kNone) {
    int size = arr.size();
    if (right == kNone) {
        right = size;
    }
    if (right - left < 3) {
        for (int i = left; i < right - 1; ++i) {
            for (int j = i + 1; j < right; ++j) {
                dmin = std::min(dmin, Norm(arr[i] - arr[j]));
            }
        }
        std::sort(arr.begin() + left, arr.begin() + right, CmpByOrdinate());
        return;
    }

    int mid = (left + right) / 2, mx = arr[mid].x;
    MinimumDistanceDC(arr, buf, dmin, left, mid);
    MinimumDistanceDC(arr, buf, dmin, mid, right);
    std::merge(arr.begin() + left, arr.begin() + mid,
               arr.begin() + mid, arr.begin() + right,
               buf.begin(), CmpByOrdinate());
    std::copy(buf.begin(), buf.begin() + right - left, arr.begin() + left);

    int out_size = 0;
    for (int i = left; i < right; ++i) {
        if (std::abs(arr[i].x - mx) < dmin) {
            for (int j = out_size - 1; j >= 0 && arr[i].y - buf[j].y < dmin; --j) {
                dmin = std::min(dmin, Norm(arr[i] - buf[j]));
            }
            buf[out_size++] = arr[i];
        }
    }
}

// Preparata's algorithm launcher
double MinimumDistanceDC(std::vector<Point>& arr) {
    double dmin = Norm(arr[1] - arr[0]);
    std::vector<Point> buf(arr.size());
    std::sort(arr.begin(), arr.end());
    MinimumDistanceDC(arr, buf, dmin);
    return dmin;
}

// Find minimum Euclidean distance between two points using scan-line algorithm in O(n log n)
int MinimumDistanceSweepLine(std::vector<Point>& arr) {
    std::sort(arr.begin(), arr.end());
    int size = arr.size();
    double dmin = Norm(arr[1] - arr[0]);
    std::multiset<Point, CmpByOrdinate> set = {arr[0], arr[1]};
    for (int j = 0, i = 2; i < size; ++i) {
        while (j < i && arr[j].x <= arr[i].x - dmin) {
            set.erase(set.find(arr[j++]));
        }
        if (!set.empty()) {
            Point bound(arr[i].x - dmin, arr[i].y - dmin);
            auto it = set.lower_bound(bound);
            while (it != set.end() && it->y < arr[i].y + dmin) {
                dmin = std::min(dmin, Norm(arr[i] - *it++));
            }
        }
        set.insert(arr[i]);
    }
    return dmin;
}

int main() {
    // Case 1: length of segments' union in O(n log n)
    {
        int n;
        std::cin >> n;
        std::vector<std::pair<int, bool>> arr;
        arr.reserve(2 * n);
        for (int a, b, i = 0; i < n; ++i) {
            std::cin >> a >> b;
            arr.emplace_back(a, false);
            arr.emplace_back(b, true);
        }

        std::sort(arr.begin(), arr.end());
        int cnt = 1, ans = 0;
        for (int i = 1; i < 2 * n; ++i) {
            if (cnt) {
                ans += arr[i].first - arr[i - 1].first;
            }
            cnt += (arr[i].second ? -1 : 1);
        }

        std::cout << ans << "\n\n";
    }

    // Case 2: find min distance using D&C algorithm in O(n log n)
    {
        int n;
        std::cin >> n;
        std::vector<Point> ps(n);
        for (int i = 0; i < n; ++i) {
            std::cin >> ps[i];
        }

        std::cout << MinimumDistanceDC(ps) << "\n\n";
    }

    // Case 3: find min distance using scan-line algorithm in O(n log n)
    {
        int n;
        std::cin >> n;
        std::vector<Point> ps(n);
        for (int i = 0; i < n; ++i) {
            std::cin >> ps[i];
        }

        std::cout << MinimumDistanceSweepLine(ps) << "\n\n";
    }

    // Case 4: find minimal set of points covering segments using scan-line in O(n log n)
    {
        int n;
        std::cin >> n;
        std::vector<std::tuple<int, bool, int>> ps;
        ps.reserve(2 * n);
        for (int a, b, i = 0; i < n; ++i) {
            std::cin >> a >> b;
            ps.emplace_back(a, false, i);
            ps.emplace_back(b, true, i);
        }

        std::vector<int> ans;
        std::vector<bool> covered(n);
        std::stack<int> st;
        for (auto [x, type, idx] : ps) {
            if (type) {  // right
                if (!covered[idx]) {
                    ans.push_back(x);
                    while (!st.empty()) {
                        int i = st.top();
                        st.pop();
                        covered[i] = true;
                    }
                }
            } else {  // left
                st.push(idx);
            }
        }

        std::cout << ans.size() << '\n';
        for (auto x : ans) {
            std::cout << x << ' ';
        }
    }

    return 0;
}
