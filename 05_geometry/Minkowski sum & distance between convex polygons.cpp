#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <vector>

constexpr double kEps = 1e-6;
constexpr double kPi = 3.14159265358979323846264;
constexpr double kInf = 1e9;

// Square function
inline double Sqr(double x) {
    return x * x;
}

// Sign function
inline int Sign(double x) {
    return x > 0 ? +1 : (x < 0 ? -1 : 0);
}

struct Point {
    double x, y;

    Point(double x = 0, double y = 0) : x(x), y(y) {
    }

    friend std::istream& operator>>(std::istream& in, Point& point) {
        return in >> point.x >> point.y;
    }

    friend std::ostream& operator<<(std::ostream& out, const Point& point) {
        return out << '(' << point.x << ", " << point.y << ')';
    }

    friend double Dot(const Point& lhs, const Point& rhs) {
        return lhs.x * rhs.x + lhs.y * rhs.y;
    }

    friend double Cross(const Point& lhs, const Point& rhs) {
        return lhs.x * rhs.y - lhs.y * rhs.x;
    }

    friend const Point operator-(const Point& point) {
        return {-point.x, -point.y};
    }

    friend const Point operator+(const Point& lhs, const Point& rhs) {
        return {lhs.x + rhs.x, lhs.y + rhs.y};
    }

    friend const Point operator-(const Point& lhs, const Point& rhs) {
        return lhs + (-rhs);
    }

    friend const Point operator*(const Point& point, double mult) {
        return {point.x * mult, point.y * mult};
    }

    friend const Point operator*(double mult, const Point& point) {
        return point * mult;
    }

    friend Point& operator+=(Point& lhs, const Point& rhs) {
        return lhs = lhs + rhs;
    }

    friend Point& operator-=(Point& lhs, const Point& rhs) {
        return lhs = lhs - rhs;
    }

    friend Point& operator*=(Point& point, double mult) {
        return point = point * mult;
    }

    friend bool operator<(const Point& lhs, const Point& rhs) {
        return lhs.x < rhs.x || (lhs.x == rhs.x && lhs.y < rhs.y);
    }

    friend double Norm(const Point& point) {
        return std::sqrt(Dot(point, point));
    }

    friend double Dist(const Point& lhs, const Point& rhs) {
        return Norm(lhs - rhs);
    }
};

struct Polygon {
    std::vector<Point> points;

    Polygon(const std::vector<Point>& arr) : points(arr) {
        Point min = *std::min_element(points.begin(), points.end());
        for (auto& point : points) {
            point -= min;
        }

        std::sort(points.begin(), points.end(), [](const Point& lhs, const Point& rhs) {
                      return lhs.y * rhs.x < lhs.x * rhs.y ||
                             (lhs.y * rhs.x == lhs.x * rhs.y && Norm(lhs) > Norm(rhs));
                  });

        for (auto& point : points) {
            point += min;
        }
    }

    size_t Size() const {
        return points.size();
    }

    const Point& operator[](int idx) const {
        return points[idx];
    }

    friend const Polygon operator-(const Polygon& poly) {
        std::vector<Point> reflected;
        reflected.reserve(poly.Size());
        for (const auto& point : poly.points) {
            reflected.emplace_back(-point);
        }
        return {reflected};
    }

    friend std::ostream& operator<<(std::ostream& out, const Polygon& poly) {
        out << "[" << poly.points[0];
        for (int idx = 1; idx < static_cast<int>(poly.Size()); ++idx) {
            out << ", " << poly[idx];
        }
        return out << "]";
    }
};

// Find Minkowski sum of two convex polygons: O(|P| + |Q|)
Polygon MinkowskiSum(const Polygon& lhs, const Polygon& rhs) {
    int left_size = lhs.Size(), right_size = rhs.Size();
    std::vector<Point> output;
    output.reserve(left_size + right_size);

    int left_ptr = 0, right_ptr = 0;
    while (left_ptr < left_size || right_ptr < right_size) {
        output.emplace_back(lhs[left_ptr % left_size] + rhs[right_ptr % right_size]);
        auto cross = Cross(
            lhs[(left_ptr + 1) % left_size] - lhs[left_ptr % left_size],
            rhs[(right_ptr + 1) % right_size] - rhs[right_ptr % right_size]);
        if (cross >= 0) {
            ++left_ptr;
        }
        if (cross <= 0) {
            ++right_ptr;
        }
    }

    return {output};
}

// Check whether a point p3 lies between p1 and p2 when projected on axes: O(1)
inline bool IsBetween(const Point& p1, const Point& p2, const Point& p3) {
    return std::min(p1.x, p2.x) <= p3.x && p3.x <= std::max(p1.x, p2.x) &&
           std::min(p1.y, p2.y) <= p3.y && p3.y <= std::max(p1.y, p2.y);
}

// Check whether a point lies inside a triangle: O(1)
inline bool IsPointInTraingle(const Point& p1, const Point& p2, const Point& p3, const Point& point) {
    int s1 = std::abs(Cross(p2 - p1, p3 - p1));
    int s2 = std::abs(Cross(p1 - point, p2 - point)) +
             std::abs(Cross(p2 - point, p3 - point)) +
             std::abs(Cross(p3 - point, p1 - point));
    return s1 == s2;
}

// Check whether a point is inside, outside or on the boundary of a convex polygon: O(log_2 n)
bool IsPointInConvexPolygon(const Polygon& poly, const Point& point) {
    int size = poly.Size();
    for (int i = 0, j = size - 1; i < size; j = i++) {
        if (!Cross(poly[i] - poly[j], point - poly[i]) &&
                IsBetween(poly[i], poly[j], point)) {
            return true;  // boundary
        }
    }

    if (Cross(poly[size - 1] - poly[0], point - poly[size - 1]) >= 0 ||
            Cross(poly[1] - poly[0], point - poly[1]) <= 0) {
        return false;  // outside
    }

    int left = 1, right = size - 2;
    while (left != right) {
        int mid = (left + right + 1) / 2;
        if (Cross(poly[mid] - poly[0], point - poly[0]) < 0) {
            right = mid - 1;
        } else {
            left = mid;
        }
    }
    return IsPointInTraingle(poly[0], poly[left], poly[left + 1], point);
}

// Compute the distance between a point and a 2D segment: O(1)
double PointToSegmentDistance(const Point& point, const Point& left, const Point& right) {
    Point segment = right - left;
    double t = std::max(0., std::min(1., Dot(point - left, segment) / Dot(segment, segment)));
    return Dist(point, left + t * segment);
}

// Compute the distance between two convex polygons: O(|P| + |Q|)
double ConvexPolygonDistance(const Polygon& lhs, const Polygon& rhs) {
    Polygon sum = MinkowskiSum(lhs, -rhs);
    Point zero;
    if (IsPointInConvexPolygon(sum, zero)) {
        return 0;
    }

    double dist = kInf;
    int size = sum.Size();
    for (int idx = 0; idx < size; ++idx) {
        dist = std::min(dist, Norm(sum[idx]));
        dist = std::min(dist, PointToSegmentDistance(zero, sum[idx], sum[(idx + 1) % size]));
    }

    return dist;
}

int main() {
    std::cout << std::fixed << std::setprecision(3);

    // Case 1: Minkowski sum of two polygons
    {
        int n, m;
        std::cin >> n >> m;
        std::vector<Point> left(n), right(m);
        for (auto& point : left) {
            std::cin >> point;
        }
        for (auto& point : right) {
            std::cin >> point;
        }

        Polygon lhs(left), rhs(right);
        Polygon sum = MinkowskiSum(lhs, rhs);

        std::cout << sum << "\n\n";
    }

    // Case 2: distance between two convex polygons
    {
        int n, m;
        std::cin >> n >> m;
        std::vector<Point> left(n), right(m);
        for (auto& point : left) {
            std::cin >> point;
        }
        for (auto& point : right) {
            std::cin >> point;
        }

        Polygon lhs(left), rhs(right);
        double dist = ConvexPolygonDistance(lhs, rhs);

        std::cout << "Distance = " << dist << '\n';
    }

    return 0;
}
