#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <vector>

constexpr double kInf = 1e9;
constexpr double kEps = 1e-6;
constexpr int kTernarySteps = 200;

struct Point {
    double x, y;

    Point(double x = 0, double y = 0) : x(x), y(y) {
    }

    friend std::istream& operator>>(std::istream& in, Point& point) {
        return in >> point.x >> point.y;
    }

    friend std::ostream& operator<<(std::ostream& out, const Point& point) {
        return out << std::fixed << std::setprecision(4) << '(' << point.x << ", " << point.y << ')';
    }

    friend double Dot(const Point& lhs, const Point& rhs) {
        return lhs.x * rhs.x + lhs.y * rhs.y;
    }

    friend double Cross(const Point& lhs, const Point& rhs) {
        return lhs.x * rhs.y - lhs.y * rhs.x;
    }

    friend const Point operator-(const Point& point) {
        return {-point.x, -point.y};
    }

    friend const Point operator+(const Point& lhs, const Point& rhs) {
        return {lhs.x + rhs.x, lhs.y + rhs.y};
    }

    friend const Point operator-(const Point& lhs, const Point& rhs) {
        return lhs + (-rhs);
    }

    friend Point& operator+=(Point& lhs, const Point& rhs) {
        return lhs = lhs + rhs;
    }

    friend Point& operator-=(Point& lhs, const Point& rhs) {
        return lhs = lhs - rhs;
    }

    friend bool operator<(const Point& lhs, const Point& rhs) {
        return lhs.x < rhs.x || (lhs.x == rhs.x && lhs.y < rhs.y);
    }

    friend double Norm(const Point& point) {
        return std::sqrt(Dot(point, point));
    }
};

// Find distance between a point p and a line p1 - p2
inline double Dist(const Point& left, const Point& right, const Point& point) {
    return std::fabs(Cross(left - point, right - point)) / Norm(left - right);
}

// Find min distance between a point and a polygon: O(n)
double GetRadius(const std::vector<Point>& points, const Point& point) {
    int size = points.size();
    double dmin = Dist(points[0], points[size - 1], point);
    for (int i = 1; i < size; ++i) {
        dmin = std::min(dmin, Dist(points[i], points[i - 1], point));
    }
    return dmin;
}

// Ternary search inner procedure
auto GetOrdinate(const std::vector<Point>& points, double x) {
    int size = points.size();
    double ly = kInf, ry = -kInf;

    for (int i = 0; i < size; ++i) {
        double x1 = points[i].x, x2 = points[(i + 1) % size].x;
        double y1 = points[i].y, y2 = points[(i + 1) % size].y;
        if (std::fabs(x1 - x2) < kEps) {
            continue;
        }
        if (x1 > x2) {
            std::swap(x1, x2);
            std::swap(y1, y2);
        }
        if (x1 < x && x < x2) {
            double y = y1 + (x - x1) * (y2 - y1) / (x2 - x1);
            ly = std::min(ly, y);
            ry = std::max(ry, y);
        }
    }

    for (int t = 0; t < kTernarySteps; ++t) {
        double dy = (ry - ly) / 3;
        double y1 = ly + dy, y2 = ry - dy;
        double r1 = GetRadius(points, {x, y1}), r2 = GetRadius(points, {x, y2});
        if (r1 < r2) {
            ly = y1;
        } else {
            ry = y2;
        }
    }
    return std::make_pair((ly + ry) / 2, GetRadius(points, {x, (ly + ry) / 2}));
}

int main() {
    // Case 1: find inscribed circle of a convex polygon in O(n log^2 C)
    {
        int n;
        std::cin >> n;
        std::vector<Point> points(n);
        for (int i = 0; i < n; ++i) {
            std::cin >> points[i];
        }
        double lx = kInf, rx = -kInf;
        for (int i = 0; i < n; ++i) {
            lx = std::min(lx, points[i].x);
            rx = std::max(rx, points[i].x);
        }
        for (int t = 0; t < kTernarySteps; ++t) {
            double dx = (rx - lx) / 3;
            double x1 = lx + dx, x2 = rx - dx;
            auto [y1, r1] = GetOrdinate(points, x1);
            auto [y2, r2] = GetOrdinate(points, x2);
            if (r1 < r2) {
                lx = x1;
            } else {
                rx = x2;
            }
        }
        double x = (lx + rx) / 2;
        auto [y, r] = GetOrdinate(points, x);
        printf("I = (%.3lf, %.3lf), r = %.3lf\n", x, y, r);
    }

    return 0;
}
