#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

struct Point {
    int x, y;

    Point(int x = 0, int y = 0) : x(x), y(y) {
    }

    friend std::istream& operator>>(std::istream& in, Point& point) {
        return in >> point.x >> point.y;
    }

    friend std::ostream& operator<<(std::ostream& out, const Point& point) {
        return out << '(' << point.x << ", " << point.y << ')';
    }

    friend int Dot(const Point& lhs, const Point& rhs) {
        return lhs.x * rhs.x + lhs.y * rhs.y;
    }

    friend int Cross(const Point& lhs, const Point& rhs) {
        return lhs.x * rhs.y - lhs.y * rhs.x;
    }

    friend const Point operator-(const Point& point) {
        return {-point.x, -point.y};
    }

    friend const Point operator+(const Point& lhs, const Point& rhs) {
        return {lhs.x + rhs.x, lhs.y + rhs.y};
    }

    friend const Point operator-(const Point& lhs, const Point& rhs) {
        return lhs + (-rhs);
    }

    friend Point& operator+=(Point& lhs, const Point& rhs) {
        return lhs = lhs + rhs;
    }

    friend Point& operator-=(Point& lhs, const Point& rhs) {
        return lhs = lhs - rhs;
    }

    friend bool operator<(const Point& lhs, const Point& rhs) {
        return lhs.x < rhs.x || (lhs.x == rhs.x && lhs.y < rhs.y);
    }

    friend int Norm(const Point& point) {
        return Dot(point, point);
    }
};

// Graham's algorithm: O(n log n)
auto ConvexHullGraham(std::vector<Point>& points) {
    int size = points.size();
    Point min = *std::min_element(points.begin(), points.end());
    for (auto& point : points) {
        point -= min;
    }

    std::sort(points.begin(), points.end(), [](const auto& lhs, const auto& rhs) {
                  return lhs.y * rhs.x < lhs.x * rhs.y ||
                         (lhs.y * rhs.x == lhs.x * rhs.y && Norm(lhs) > Norm(rhs));
              });

    std::vector<Point> hull = {points[0], points[1]};
    for (int out_size = 2, i = 2; i < size; ++i) {
        while (out_size >= 2 &&
                Cross(hull[out_size - 1] - hull[out_size - 2], points[i] - hull[out_size - 1]) <= 0) {
            hull.pop_back();
            --out_size;
        }
        hull.push_back(points[i]);
        ++out_size;
    }

    for (auto& point : hull) {
        point += min;
    }
    return hull;
}

// Andrew's algorithm: O(n log n)
auto ConvexHullAndrew(std::vector<Point>& points) {
    int size = points.size();
    std::sort(points.begin(), points.end());

    Point p1 = points[0], p2 = points[size - 1];
    std::vector<Point> upper = {p1}, lower = {p1};
    int up_sz = 1, low_sz = 1;

    for (int i = 1; i < size; ++i) {
        if (i == size - 1 || Cross(points[i] - p1, p2 - points[i]) <= 0) {
            while (up_sz >= 2 &&
                   Cross(upper[up_sz - 1] - upper[up_sz - 2],
                         points[i] - upper[up_sz - 1]) >= 0) {
                upper.pop_back();
                --up_sz;
            }
            upper.push_back(points[i]);
            ++up_sz;
        }

        if (i == size - 1 || Cross(points[i] - p1, p2 - points[i]) >= 0) {
            while (low_sz >= 2 &&
                   Cross(lower[low_sz - 1] - lower[low_sz - 2],
                         points[i] - lower[low_sz - 1]) <= 0) {
                lower.pop_back();
                --low_sz;
            }
            lower.push_back(points[i]);
            ++low_sz;
        }
    }

    std::vector<Point> hull = upper;
    for (int i = low_sz - 2; i > 0; --i) {
        hull.push_back(lower[i]);
    }
    return hull;
}

// Check whether a point p3 lies between p1 and p2 when projected on axes: O(1)
inline bool IsBetween(const Point& p1, const Point& p2, const Point& p3) {
    return std::min(p1.x, p2.x) <= p3.x && p3.x <= std::max(p1.x, p2.x) &&
           std::min(p1.y, p2.y) <= p3.y && p3.y <= std::max(p1.y, p2.y);
}

// Check whether a point is inside, outside or on the boundary of a simple polygon: O(n)
void IsPointInPolygon(const std::vector<Point>& poly, const Point& point) {
    int size = poly.size(), balance = 0;
    for (int i = 0, j = size - 1; i < size; j = i++) {
        int cross = Cross(point - poly[i], poly[i] - poly[j]);
        if (!cross && IsBetween(poly[i], poly[j], point)) {
            std::cout << "BOUNDARY\n";
            return;
        }
        if (cross > 0 && poly[j].y <= point.y && point.y < poly[i].y) {
            ++balance;
        } else if (cross < 0 && poly[j].y > point.y && point.y >= poly[i].y) {
            --balance;
        }
    }
    std::cout << (balance ? "INSIDE\n" : "OUTSIDE\n");
}

// Check whether a point lies inside a triangle: O(1)
inline bool IsPointInTraingle(const Point& p1, const Point& p2, const Point& p3, const Point& point) {
    int s1 = std::abs(Cross(p2 - p1, p3 - p1));
    int s2 = std::abs(Cross(p1 - point, p2 - point)) +
             std::abs(Cross(p2 - point, p3 - point)) +
             std::abs(Cross(p3 - point, p1 - point));
    return s1 == s2;
}

// Check whether a point is inside, outside or on the boundary of a convex polygon: O(log_2 n)
void IsPointInConvexPolygon(const std::vector<Point>& poly, const Point& point) {
    int size = poly.size();
    for (int i = 0, j = size - 1; i < size; j = i++) {
        if (!Cross(poly[i] - poly[j], point - poly[i]) &&
                IsBetween(poly[i], poly[j], point)) {
            std::cout << "BOUNDARY\n";
            return;
        }
    }

    if (Cross(poly[size - 1] - poly[0], point - poly[size - 1]) >= 0 ||
            Cross(poly[1] - poly[0], point - poly[1]) <= 0) {
        std::cout << "OUTSIDE\n";
        return;
    }

    int left = 1, right = size - 2;
    while (left != right) {
        int mid = (left + right + 1) / 2;
        if (Cross(poly[mid] - poly[0], point - poly[0]) < 0) {
            right = mid - 1;
        } else {
            left = mid;
        }
    }
    std::cout << (IsPointInTraingle(poly[0], poly[left], poly[left + 1], point)
                  ? "INSIDE\n" : "OUTSIDE\n");
}

int main() {
    // Case 1: scalar (dot) and vector (cross) product
    {
        Point p1, p2;
        std::cin >> p1 >> p2;

        std::cout << "(p1, p2) = " << Dot(p1, p2) << '\n';
        std::cout << "[p1, p2] = " << Cross(p1, p2) << "\n\n";
    }

    // Case 2: area of triangle
    {
        Point p1, p2, p3;
        std::cin >> p1 >> p2 >> p3;

        printf("S = %.3lf\n\n", std::abs(0.5 * Cross(p2 - p1, p3 - p1)));
    }

    // Case 3: area of polygon (not necessarily convex!) in O(n)
    {
        int n;
        std::cin >> n;
        std::vector<Point> poly(n);
        for (auto& point : poly) {
            std::cin >> point;
        }

        int area = 0;
        for (int i = 2; i < n; ++i) {
            area += Cross(poly[i - 1] - poly[0], poly[i] - poly[0]);
        }

        printf("S = %.3lf\n\n", std::abs(0.5 * area));
    }

    // Case 4: convex hull construction using Graham's algorithm in O(n log n)
    {
        int n;
        std::cin >> n;
        std::vector<Point> ps(n);
        for (auto& point : ps) {
            std::cin >> point;
        }

        auto hull = ConvexHullGraham(ps);

        std::cout << hull.size() << '\n';
        for (const auto& p : hull) {
            std::cout << p << '\n';
        }
        std::cout << '\n';
    }

    // Case 5: convex hull construction using Andrew's algorithm in O(n log n)
    {
        int n;
        std::cin >> n;
        std::vector<Point> ps(n);
        for (auto& point : ps) {
            std::cin >> point;
        }

        auto hull = ConvexHullAndrew(ps);

        std::cout << hull.size() << '\n';
        for (const auto& p : hull) {
            std::cout << p << '\n';
        }
        std::cout << '\n';
    }

    // Case 6: find number of lattice points inside a lattice in O(n)
    {
        int n;
        std::cin >> n;
        std::vector<Point> ps(n);
        for (auto& point : ps) {
            std::cin >> point;
        }

        int s = 0, p = 0;
        p += std::gcd(std::abs(ps[1].x - ps[0].x), std::abs(ps[1].y - ps[0].y));
        p += std::gcd(std::abs(ps[n - 1].x - ps[0].x), std::abs(ps[n - 1].y - ps[0].y));
        for (int i = 2; i < n; ++i) {
            s += Cross(ps[i - 1] - ps[0], ps[i] - ps[0]);
            auto dp = ps[i] - ps[i - 1];
            p += std::gcd(std::abs(dp.x), std::abs(dp.y));
        }

        // Pick's theorem: S = I + P/2 - 1 => I = S - P/2 + 1
        printf("S = %.3lf, P = %d, I = %d\n\n", 0.5 * std::abs(s), p, (std::abs(s) - p) / 2 + 1);
    }

    // Case 7: check if a point is inside a simple polygon (not necessarily convex!) in O(n)
    {
        int n, qs;
        std::cin >> n >> qs;
        std::vector<Point> poly(n);
        for (auto& point : poly) {
            std::cin >> point;
        }

        Point query;
        while (qs--) {
            std::cin >> query;
            IsPointInPolygon(poly, query);
        }

        std::cout << '\n';
    }

    // Case 8: check if a point is inside a convex polygon in O(log n)
    {
        int n, qs;
        std::cin >> n >> qs;
        std::vector<Point> poly(n);
        for (auto& point : poly) {
            std::cin >> point;
        }

        int imin = std::min_element(poly.begin(), poly.end()) - poly.begin();
        if (imin) {
            std::rotate(poly.begin(), poly.begin() + imin, poly.end());
        }
        Point query;
        while (qs--) {
            std::cin >> query;
            IsPointInConvexPolygon(poly, query);
        }
    }

    return 0;
}
