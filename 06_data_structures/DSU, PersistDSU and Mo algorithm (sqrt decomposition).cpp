#include <algorithm>
#include <cmath>
#include <iostream>
#include <tuple>
#include <vector>

class DSU {
    std::vector<int> parent_, rank_;
    int num_sets_;

public:
    explicit DSU(int size) : parent_(size + 1), rank_(size + 1, 1), num_sets_(size) {
        for (int vertex = 1; vertex <= size; ++vertex) {
            parent_[vertex] = vertex;
        }
    }

    int FindSet(int vertex) {
        if (parent_[vertex] != vertex) {
            parent_[vertex] = FindSet(parent_[vertex]);
        }
        return parent_[vertex];
    }

    bool UniteSets(int left, int right) {
        left = FindSet(left);
        right = FindSet(right);
        if (left != right) {
            if (rank_[left] > rank_[right]) {
                std::swap(left, right);
            }
            parent_[left] = right;
            rank_[right] += rank_[left];
            --num_sets_;
            return true;
        }
        return false;
    }

    int NumberOfSets() const {
        return num_sets_;
    }
};

class PersistDSU {
    using Record = std::pair<int, int>;

    std::vector<std::vector<Record>> history_;
    std::vector<int> parent_, rank_;
    int num_sets_;

public:
    explicit PersistDSU(int size) : history_(1), parent_(size + 1), rank_(size + 1, 1), num_sets_(size) {
        for (int vertex = 1; vertex <= size; ++vertex) {
            parent_[vertex] = vertex;
        }
    }

    void Clear() {
        history_.assign(1, {});
        int size = parent_.size() - 1;
        rank_.assign(size + 1, 1);
        num_sets_ = size;
        for (int vertex = 1; vertex <= size; ++vertex) {
            parent_[vertex] = vertex;
        }
    }

    int FindSet(int vertex) const {
        return parent_[vertex] == vertex ? vertex : FindSet(parent_[vertex]);
    }

    bool UniteSets(int left, int right) {
        left = FindSet(left);
        right = FindSet(right);
        if (left != right) {
            if (rank_[left] > rank_[right]) {
                std::swap(left, right);
            }
            history_.back().emplace_back(left, right);
            parent_[left] = right;
            rank_[right] += rank_[left];
            --num_sets_;
            return true;
        }
        return false;
    }

    bool Connected(int left, int right) const {
        return FindSet(left) == FindSet(right);
    }

    int NumberOfSets() const {
        return num_sets_;
    }

    void Persist() {
        history_.emplace_back();
    }

    void Rollback() {
        auto& batch = history_.back();
        while (!batch.empty()) {
            auto [left, right] = batch.back();
            batch.pop_back();
            parent_[left] = left;
            rank_[right] -= rank_[left];
            ++num_sets_;
        }
        if (history_.size() > 1) {
            history_.pop_back();
        }
    }
};

// Auxiliary struct for query processing
struct Query {
    int left, right, idx;

    Query(int left, int right, int idx) : left(left), right(right), idx(idx) {
    }

    friend bool operator<(const Query& lhs, const Query& rhs) {
        return lhs.right < rhs.right;
    }
};

int main() {
    // Case 1: connectivity problem (only + and ? queries)
    {
        int n, qs, u, v;
        std::string query;
        std::cin >> n >> qs;
        DSU dsu(n);
        while (qs--) {
            std::cin >> query;
            if (query == "+") {
                std::cin >> u >> v;
                std::cout << "add: " << (dsu.UniteSets(u, v) ? "OK\n" : "Already connected!\n");
            } else if (query == "?") {
                std::cin >> u >> v;
                std::cout << (dsu.FindSet(u) == dsu.FindSet(v) ? "YES\n" : "NO\n");
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
        std::cout << '\n';
    }

    // Case 2: dynamic connectivity problem (segments of edges)
    {
        // sqrt decomposition (Mo's algorithm)
        int n, m, k, u, v, left, right;
        std::cin >> n >> m;
        PersistDSU dsu(n);
        std::vector<std::pair<int, int>> edges;
        edges.reserve(m);
        for (int i = 0; i < m; ++i) {
            std::cin >> u >> v;
            edges.emplace_back(u, v);
        }
        std::cin >> k;
        int root = std::sqrt(m);
        std::vector<std::vector<Query>> blocks(root + 2);
        for (int i = 0; i < k; ++i) {
            std::cin >> left >> right;
            --left, --right;
            int lower = left / root;
            blocks[lower].emplace_back(left, right, i);
        }
        std::vector<int> ans(k);

        for (int b = 0; b < int(blocks.size()); ++b) {
            int upper = root * (b + 1);
            auto& block = blocks[b];
            std::sort(block.begin(), block.end());
            int i;
            for (i = 0; i < static_cast<int>(block.size()) && block[i].right < upper; ++i) {
                auto [left, right, idx] = block[i];
                dsu.Persist();
                for (int j = left; j <= right; ++j) {
                    auto [u, v] = edges[j];
                    dsu.UniteSets(u, v);
                }
                ans[idx] = dsu.NumberOfSets();
                dsu.Rollback();
            }

            int prev = upper - 1;
            for (; i < int(block.size()); ++i) {
                auto [left, right, idx] = block[i];
                for (int j = prev + 1; j <= right; ++j) {
                    auto [u, v] = edges[j];
                    dsu.UniteSets(u, v);
                }
                prev = right;
                dsu.Persist();
                for (int j = left; j < upper; ++j) {
                    auto [u, v] = edges[j];
                    dsu.UniteSets(u, v);
                }
                ans[idx] = dsu.NumberOfSets();
                dsu.Rollback();
            }
            dsu.Clear();
        }

        for (auto elem : ans) {
            std::cout << elem << ' ';
        }
    }

    return 0;
}
