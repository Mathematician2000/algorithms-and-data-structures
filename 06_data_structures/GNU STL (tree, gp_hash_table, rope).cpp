#include <ext/pb_ds/assoc_container.hpp>
#include <ext/rope>
#include <functional>
#include <iostream>
#include <vector>

template<typename T>
using Tree = __gnu_pbds::tree<T,
                              __gnu_pbds::null_type,
                              std::less<T>,  // for non-unique elements use std::less_equal or std::pair
                              __gnu_pbds::rb_tree_tag,
                              __gnu_pbds::tree_order_statistics_node_update>;

template<typename T1, typename T2>
using HashMap = __gnu_pbds::gp_hash_table<T1, T2>;

int main() {
    // Case 1: BST with order statistics in O(log n)
    {
        int qs, arg;
        std::string query;
        Tree<int> tree;
        std::cin >> qs;
        while (qs--) {
            std::cin >> query;
            if (query == "insert") {
                std::cin >> arg;
                tree.insert(arg);
                std::cout << "insert: OK\n";
            } else if (query == "erase") {
                std::cin >> arg;
                tree.erase(arg);  // or tree.erase(tree.find(arg))
                std::cout << "erase: OK\n";
            } else if (query == "get_order") {
                std::cin >> arg;
                std::cout << tree.order_of_key(arg) << '\n';
            } else if (query == "by_order") {
                std::cin >> arg;
                auto it = tree.find_by_order(arg);
                if (it == tree.end()) {
                    std::cout << "Not found\n";
                } else {
                    std::cout << *it << '\n';
                }
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
        std::cout << '\n';
    }

    // Case 2: hash table
    {
        int qs, key, value;
        std::string query;
        HashMap<int, int> ht;
        std::cin >> qs;
        while (qs--) {
            std::cin >> query;
            if (query == "insert") {
                std::cin >> key >> value;
                ht[key] = value;
                std::cout << "insert: OK\n";
            } else if (query == "erase") {
                std::cin >> key;
                ht.erase(key);
                std::cout << "erase: OK\n";
            } else if (query == "get") {
                std::cin >> key;
                auto it = ht.find(key);  // ht[key], ht.at(key)
                if (it == ht.end()) {
                    std::cout << "Not found\n";
                } else {
                    std::cout << it->second << '\n';
                }
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
        std::cout << '\n';
    }

    // Case 3: rope (~treap)
    {
        int qs, idx, left, right;
        std::string initial, query, s;
        std::cin >> initial >> qs;
        __gnu_cxx::crope rope = initial.c_str();  // crope = rope<char>
        while (qs--) {
            std::cin >> query;
            if (query == "insert") {
                std::cin >> idx >> s;
                rope.insert(idx, s.c_str());
                std::cout << "insert: OK\n";
            } else if (query == "append") {
                std::cin >> s;
                rope.append(s.c_str());
                std::cout << "append: OK\n";
            } else if (query == "erase") {
                std::cin >> left >> right;
                --left, --right;
                rope.erase(left, right - left + 1);
                std::cout << "erase: OK\n";
            } else if (query == "get") {
                std::cin >> left >> right;
                --left, --right;
                std::cout << rope.substr(left, right - left + 1) << '\n';
            } else if (query == "all") {
                std::cout << rope.replace_with_c_str() << '\n';
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
    }

    return 0;
}
