#include <functional>
#include <iostream>
#include <vector>

template<typename T, class Cmp = std::less<T>>
class Heap {
    std::vector<T> arr_;
    std::function<bool(T, T)> cmp_ = Cmp();

    int SiftUp(int idx) {
        while (idx && cmp_(arr_[(idx - 1) / 2], arr_[idx])) {
            std::swap(arr_[(idx - 1) / 2], arr_[idx]);
            idx = (idx - 1) / 2;
        }
        return idx;
    }

    int SiftDown(int idx = 0) {
        int size = Size();
        while (2 * idx + 1 < size) {
            int left = 2 * idx + 1, right = 2 * idx + 2;
            int pos = left;
            if (right < size && !cmp_(arr_[right], arr_[left])) {
                pos = right;
            }
            if (cmp_(arr_[pos], arr_[idx])) {
                break;
            }
            std::swap(arr_[idx], arr_[pos]);
            idx = pos;
        }
        return idx;
    }

    void Erase(int idx) {
        int size = Size();
        if (idx != size - 1) {
            std::swap(arr_[idx], arr_.back());
            arr_.pop_back();
            SiftDown(idx);
        } else {
            arr_.pop_back();
        }
    }

public:
    Heap(const std::vector<T>& arr = {}) : arr_(arr) {
        int size = Size();
        for (int i = size / 2; i >= 0; --i) {
            SiftDown(i);
        }
    }

    bool Empty() const {
        return arr_.empty();
    }

    size_t Size() const {
        return arr_.size();
    }

    const std::pair<int, T> ExtractMax() {
        if (Empty()) {
            throw std::out_of_range("Heap is empty");
        }
        const T ans = arr_.front();
        arr_[0] = arr_.back();
        arr_.pop_back();
        return {SiftDown(0), ans};
    }

    int Insert(const T& elem) {
        arr_.push_back(elem);
        return SiftUp(arr_.size() - 1);
    }

    const T Pop(int idx) {
        if (Empty()) {
            throw std::out_of_range("Heap is empty");
        }
        const T ans = arr_[idx];
        Erase(idx);
        return ans;
    }

    int ChangePriority(int idx, const T& delta) {
        return SetPriority(idx, arr_[idx] + delta);
    }

    int SetPriority(int idx, const T& priority) {
        const T old = arr_[idx];
        arr_[idx] = priority;
        return !cmp_(priority, old)
               ? SiftUp(idx)
               : SiftDown(idx);
    }

    void Traverse() const {
        if (Empty()) {
            std::cout << "Heap is empty\n";
            return;
        }
        for (auto elem : arr_) {
            std::cout << elem << ' ';
        }
        std::cout << '\n';
    }
};

// HeapSort: O(n log n)
template<typename T>
void HeapSort(std::vector<T>& arr) {
    Heap<T, std::greater<T>> heap(arr);  // min-heap
    for (int i = 0; i < static_cast<int>(arr.size()); ++i) {
        arr[i] = heap.ExtractMax().second;
    }
}

int main() {
    // Case 1: max-heap ~ std::priority_queue
    {
        int qs, idx, arg;
        std::string query;
        Heap<int> heap;
        std::cin >> qs;
        while (qs--) {
            std::cin >> query;
            if (query == "empty") {
                std::cout << (heap.Empty() ? "YES\n" : "NO\n");
            } else if (query == "size") {
                std::cout << heap.Size() << '\n';
            } else if (query == "insert") {
                std::cin >> arg;
                heap.Insert(arg);
                std::cout << "insert: OK\n";
            } else if (query == "pop") {
                std::cin >> idx;
                std::cout << heap.Pop(idx) << '\n';
            } else if (query == "extract") {
                auto [idx, arg] = heap.ExtractMax();
                printf("heap[%d] = %d\n", idx, arg);
            } else if (query == "set") {
                std::cin >> idx >> arg;
                std::cout << heap.SetPriority(idx, arg) << '\n';
                std::cout << "set: OK\n";
            } else if (query == "Traverse") {
                heap.Traverse();
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
        std::cout << '\n';
    }

    // Case 2: HeapSort in O(n log n)
    {
        int n;
        std::cin >> n;
        std::vector<int> arr(n);
        for (int i = 0; i < n; ++i) {
            std::cin >> arr[i];
        }
        HeapSort(arr);
        for (auto elem : arr) {
            std::cout << elem << ' ';
        }
        std::cout << '\n';
    }

    return 0;
}
