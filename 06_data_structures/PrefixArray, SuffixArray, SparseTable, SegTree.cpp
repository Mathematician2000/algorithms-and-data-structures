#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>

template<typename T>
class PrefixArray {
    std::vector<T> pref_;

public:
    PrefixArray(const std::vector<T>& arr) : pref_(arr.size() + 1) {
        int size = arr.size();
        for (int i = 0; i < size; ++i) {
            pref_[i + 1] = pref_[i] + arr[i];
        }
    }

    const T GetSum(int left, int right) const {
        if (left < 1 || right > static_cast<int>(pref_.size()) - 1) {
            throw std::out_of_range("PrefixArray: indices out of range");
        }
        return pref_[right] - pref_[left - 1];
    }
};

template<typename T>
class SuffixArray {
    std::vector<T> suff_;

public:
    SuffixArray(const std::vector<T>& arr) : suff_(arr.size() + 2) {
        int size = arr.size();
        for (int i = size - 1; i >= 0; --i) {
            suff_[i + 1] = suff_[i + 2] + arr[i];
        }
    }

    const T GetSum(int left, int right) const {
        if (left < 1 || right > static_cast<int>(suff_.size()) - 1) {
            throw std::out_of_range("SuffixArray: indices out of range");
        }
        return suff_[left] - suff_[right + 1];
    }
};

template<typename T>
class SparseTable {
    std::vector<std::vector<T>> arr_;
    std::vector<int> log2_;

public:
    SparseTable(const std::vector<T>& arr) {
        int size = arr.size();
        log2_.resize(size + 1);
        arr_.emplace_back(arr);
        for (int level = 1, sz = 2; sz <= size; ++level, sz *= 2) {
            for (int i = sz; i <= std::min(size, 2 * sz - 1); ++i) {
                log2_[i] = level;
            }
            arr_.emplace_back(size - sz + 1);
            for (int i = 0; i + sz - 1 < size; ++i) {
                arr_[level][i] = std::min(arr_[level - 1][i], arr_[level - 1][i + sz / 2]);
            }
        }
    }

    const T GetMin(int left, int right) const {
        if (left < 1 || right > static_cast<int>(arr_[0].size())) {
            throw std::out_of_range("SparseTable: indices out of range");
        }
        --left, --right;
        int len = right - left + 1, log = log2_[len];
        return std::min(arr_[log][left], arr_[log][right - (1 << log) + 1]);
    }
};

template<typename T>
class SegTree {
    static constexpr int kNone = -1;

    std::vector<T> tree_;
    std::function<T(T, T)> op_;
    T neutral_;
    int size_ = 1;

public:
    SegTree(const std::vector<T>& arr,
            const std::function<T(T, T)>& op,
            const T& neutral) : op_(op), neutral_(neutral) {
        int size = arr.size();
        while (size_ < size) {
            size_ *= 2;
        }

        tree_.assign(2 * size_ - 1, neutral_);
        for (int i = 0; i < size; ++i) {
            tree_[i + size_ - 1] = arr[i];
        }
        for (int i = size_ - 2; i >= 0; --i) {
            tree_[i] = op_(tree_[2 * i + 1], tree_[2 * i + 2]);
        }
    }

    const T Aggregate(int from, int to, int idx = 0, int left = 0, int right = kNone) const {
        if (right == kNone) {
            right = size_;
        }
        if (from == left && right == to) {
            return tree_[idx];
        }

        int mid = (left + right) / 2;
        T ans = neutral_;
        if (from < mid) {
            ans = op_(ans, Aggregate(from, std::min(to, mid), 2 * idx + 1, left, mid));
        }
        if (to > mid) {
            ans = op_(ans, Aggregate(std::max(from, mid), to, 2 * idx + 2, mid, right));
        }
        return ans;
    }

    void SetValue(int pos, const T& elem, int idx = 0, int left = 0, int right = kNone) {
        if (right == kNone) {
            right = size_;
        }
        if (left + 1 == right) {
            tree_[idx] = elem;
            return;
        }

        int mid = (left + right) / 2;
        if (pos < mid) {
            SetValue(pos, elem, 2 * idx + 1, left, mid);
        } else {
            SetValue(pos, elem, 2 * idx + 2, mid, right);
        }
        tree_[idx] = op_(tree_[2 * idx + 1], tree_[2 * idx + 2]);
    }
};

int main() {
    // Case 1: prefix sums in <O(n), O(1)> (static RSQ)
    {
        int n, qs, l, r;
        std::cin >> n >> qs;
        std::vector<int> arr(n);
        for (auto& elem : arr) {
            std::cin >> elem;
        }

        PrefixArray<int> pref(arr);
        while (qs--) {
            std::cin >> l >> r;
            std::cout << pref.GetSum(l, r) << '\n';
        }
        std::cout << '\n';
    }

    // Case 2: suffix sums in <O(n), O(1)> (static RSQ)
    {
        int n, qs, l, r;
        std::cin >> n >> qs;
        std::vector<int> arr(n);
        for (auto& elem : arr) {
            std::cin >> elem;
        }

        SuffixArray<int> suff(arr);
        while (qs--) {
            std::cin >> l >> r;
            std::cout << suff.GetSum(l, r) << '\n';
        }
        std::cout << '\n';
    }

    // Case 3: sparse table in <O(n log n), O(1)> (static RMQ)
    {
        int n, qs, l, r;
        std::cin >> n >> qs;
        std::vector<int> arr(n);
        for (auto& elem : arr) {
            std::cin >> elem;
        }

        SparseTable<int> table(arr);
        while (qs--) {
            std::cin >> l >> r;
            std::cout << table.GetMin(l, r) << '\n';
        }
        std::cout << '\n';
    }

    // Case 4: segment tree in <O(n), O(log n)> (dynamic RMQ/RSQ)
    {
        int n, qs, l, r, idx, elem;
        std::string query;
        std::cin >> n >> qs;
        std::vector<int> arr(n);
        for (auto& elem : arr) {
            std::cin >> elem;
        }

        SegTree<int> tree(arr, [](auto x, auto y) { return x + y; }, 0);
        while (qs--) {
            std::cin >> query;
            if (query == "set") {
                std::cin >> idx >> elem;
                tree.SetValue(idx - 1, elem);
                std::cout << "set: OK\n";
            } else if (query == "get") {
                std::cin >> l >> r;
                std::cout << tree.Aggregate(l - 1, r) << '\n';
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
    }

    return 0;
}
