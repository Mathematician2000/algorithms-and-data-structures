#include <algorithm>
#include <iostream>
#include <vector>

constexpr int kNone = -1;

// Segment tree for range sums, increasing and assignment
class RangeSegTree {
    std::vector<int64_t> tree_, adds_, assigns_;
    int size_;

    int64_t GetValue(int idx, int left, int right) {
        return (assigns_[idx] ? assigns_[idx] * (right - left) : tree_[idx]) + adds_[idx] * (right - left);
    }

    void Push(int idx, int left, int right) {
        if (assigns_[idx]) {
            adds_[2 * idx + 1] = adds_[2 * idx + 2] = 0;
            assigns_[2 * idx + 1] = assigns_[2 * idx + 2] = assigns_[idx];
            tree_[idx] = assigns_[idx] * (right - left);
            assigns_[idx] = 0;
        }
        if (adds_[idx]) {
            adds_[2 * idx + 1] += adds_[idx];
            adds_[2 * idx + 2] += adds_[idx];
            tree_[idx] += adds_[idx] * (right - left);
            adds_[idx] = 0;
        }
    }

public:
    RangeSegTree(const std::vector<int64_t>& arr) {
        int size = arr.size();
        size_ = 1;
        while (size_ < size) {
            size_ *= 2;
        }

        tree_.assign(2 * size_ - 1, 0);
        adds_.assign(2 * size_ - 1, 0);
        assigns_.assign(2 * size_ - 1, 0);
        for (int i = 0; i < size; ++i) {
            tree_[i + size_ - 1] = arr[i];
        }
        for (int i = size_ - 2; i >= 0; --i) {
            tree_[i] = tree_[2 * i + 1] + tree_[2 * i + 2];
        }
    }

    void Increase(int from, int to, int delta, int idx = 0, int left = 0, int right = kNone) {
        if (right == kNone) {
            right = size_;
        }
        if (from == left && right == to) {
            adds_[idx] += delta;
            return;
        }

        Push(idx, left, right);
        int mid = (left + right) / 2;
        if (from < mid) {
            Increase(from, std::min(mid, to), delta, 2 * idx + 1, left, mid);
        }
        if (to > mid) {
            Increase(std::max(from, mid), to, delta, 2 * idx + 2, mid, right);
        }
        tree_[idx] = GetValue(2 * idx + 1, left, mid) + GetValue(2 * idx + 2, mid, right);
    }

    void Assign(int from, int to, int val, int idx = 0, int left = 0, int right = kNone) {
        if (right == kNone) {
            right = size_;
        }
        if (from == left && right == to) {
            assigns_[idx] = val;
            adds_[idx] = 0;
            return;
        }

        Push(idx, left, right);
        int mid = (left + right) / 2;
        if (from < mid) {
            Assign(from, std::min(mid, to), val, 2 * idx + 1, left, mid);
        }
        if (to > mid) {
            Assign(std::max(from, mid), to, val, 2 * idx + 2, mid, right);
        }
        tree_[idx] = GetValue(2 * idx + 1, left, mid) + GetValue(2 * idx + 2, mid, right);
    }

    int64_t GetSum(int from, int to, int idx = 0, int left = 0, int right = kNone) {
        if (right == kNone) {
            right = size_;
        }
        if (from == left && right == to) {
            return GetValue(idx, left, right);
        }

        Push(idx, left, right);
        int mid = (left + right) / 2;
        int64_t ans = 0;
        if (from < mid) {
            ans += GetSum(from, std::min(mid, to), 2 * idx + 1, left, mid);
        }
        if (to > mid) {
            ans += GetSum(std::max(from, mid), to, 2 * idx + 2, mid, right);
        }
        return ans;
    }
};

// 2-dimensional segment tree
class SegTree2D {
    std::vector<std::vector<int>> tree_;
    int x_size_, y_size_;

    void UpdateInner(int x, int y, int val,
                     int vx = 0, int lx = 0, int rx = kNone,
                     int vy = 0, int ly = 0, int ry = kNone) {
        if (rx == kNone) {
            rx = x_size_;
        }
        if (ry == kNone) {
            ry = y_size_;
        }
        if (ly + 1 == ry) {
            if (lx + 1 == rx) {
                tree_[vx][vy] = val;
            } else {
                tree_[vx][vy] = tree_[2 * vx + 1][vy] + tree_[2 * vx + 2][vy];
            }
            return;
        }

        int mid = (ly + ry) / 2;
        if (y < mid) {
            UpdateInner(x, y, val, vx, lx, rx, 2 * vy + 1, ly, mid);
        } else {
            UpdateInner(x, y, val, vx, lx, rx, 2 * vy + 2, mid, ry);
        }
        tree_[vx][vy] = tree_[vx][2 * vy + 1] + tree_[vx][2 * vy + 2];
    }

    int GetInner(int y1, int y2, int vx = 0, int vy = 0, int ly = 0, int ry = kNone) const {
        if (ry == kNone) {
            ry = y_size_;
        }
        if (y1 == ly && y2 == ry) {
            return tree_[vx][vy];
        }

        int mid = (ly + ry) / 2, ans = 0;
        if (y1 < mid) {
            ans += GetInner(y1, std::min(mid, y2), vx, 2 * vy + 1, ly, mid);
        }
        if (y2 > mid) {
            ans += GetInner(std::max(y1, mid), y2, vx, 2 * vy + 2, mid, ry);
        }
        return ans;
    }

public:
    SegTree2D(const std::vector<std::vector<int>>& arr) {
        int rows = arr.size(), cols = arr[0].size();
        x_size_ = y_size_ = 1;
        while (x_size_ < rows) {
            x_size_ *= 2;
        }
        while (y_size_ < cols) {
            y_size_ *= 2;
        }

        tree_.assign(2 * x_size_ - 1, std::vector<int>(2 * y_size_ - 1, 0));
        for (int i = 0; i < rows; ++i) {
            for (int j = 0; j < cols; ++j) {
                tree_[i + x_size_ - 1][j + y_size_ - 1] = arr[i][j];
            }
            for (int j = y_size_ - 2; j >= 0; --j) {
                tree_[i + x_size_ - 1][j] = tree_[i + x_size_ - 1][2 * j + 1] +
                                        tree_[i + x_size_ - 1][2 * j + 2];
            }
        }
        for (int i = x_size_ - 2; i >= 0; --i) {
            for (int j = 0; j < 2 * y_size_ - 1; ++j) {
                tree_[i][j] = tree_[2 * i + 1][j] + tree_[2 * i + 2][j];
            }
        }
    }

    void Update(int x, int y, int val,
                int vx = 0, int lx = 0, int rx = kNone,
                int vy = 0, int ly = 0, int ry = kNone) {
        if (rx == kNone) {
            rx = x_size_;
        }
        if (ry == kNone) {
            ry = y_size_;
        }
        if (lx + 1 != rx) {
            int mid = (lx + rx) / 2;
            if (x < mid) {
                Update(x, y, val, 2 * vx + 1, lx, mid, vy, ly, ry);
            } else {
                Update(x, y, val, 2 * vx + 2, mid, rx, vy, ly, ry);
            }
        }
        UpdateInner(x, y, val, vx, lx, rx);
    }

    int GetSum(int x1, int x2, int y1, int y2, int vx = 0, int lx = 0, int rx = kNone) const {
        if (rx == -1) {
            rx = x_size_;
        }
        if (x1 == lx && x2 == rx) {
            return GetInner(y1, y2, vx);
        }

        int mid = (lx + rx) / 2, ans = 0;
        if (x1 < mid) {
            ans += GetSum(x1, std::min(mid, x2), y1, y2, 2 * vx + 1, lx, mid);
        }
        if (x2 > mid) {
            ans += GetSum(std::max(x1, mid), x2, y1, y2, 2 * vx + 2, mid, rx);
        }
        return ans;
    }
};

// Dynamic segment tree
// You might use smart pointers (std::shared_ptr) to avoid memory leaks
class DynamicSegTree {
    struct Node;
    using Ptr = Node*;

    struct Node {
        Ptr left = nullptr, right = nullptr;
        int64_t sum = 0;

        Node(int64_t val) : sum(val) {}
        Node(Ptr left, Ptr right) : left(left), right(right) {
            if (left) {
                sum += left->sum;
            }
            if (right) {
                sum += right->sum;
            }
        }
    };

    static Ptr Build(const std::vector<int>& arr, int left, int right) {
        if (left + 1 == right) {
            return new Node(arr[left]);
        }
        int mid = (left + right) / 2;
        return new Node(Build(arr, left, mid), Build(arr, mid, right));
    }

    static Ptr Update(Ptr tree, int pos, int val, int left, int right) {
        if (left + 1 == right) {
            return new Node(val);
        }
        int mid = (left + right) / 2;
        return pos < mid
               ? new Node(Update(tree->left, pos, val, left, mid), tree->right)
               : new Node(tree->left, Update(tree->right, pos, val, mid, right));
    }

    static int64_t GetSum(const Ptr tree, int from, int to, int left, int right) {
        if (left == from && to == right) {
            return tree->sum;
        }
        int mid = (left + right) / 2;
        int64_t ans = 0;
        if (from < mid) {
            ans += GetSum(tree->left, from, std::min(mid, to), left, mid);
        }
        if (to > mid) {
            ans += GetSum(tree->right, std::max(from, mid), to, mid, right);
        }
        return ans;
    }

    std::vector<Ptr> history_;
    Ptr root_ = nullptr;
    int size_;

public:
    explicit DynamicSegTree(const std::vector<int>& arr) : size_(arr.size()) {
        root_ = Build(arr, 0, size_);
    }

    void Update(int pos, int val) {
        root_ = Update(root_, pos, val, 0, size_);
    }

    int64_t GetSum(int left, int right) const {
        return GetSum(root_, left, right, 0, size_);
    }

    void Persist() {
        history_.push_back(root_);
    }

    bool Rollback() {
        if (history_.empty()) {
            return false;
        }
        root_ = history_.back();
        history_.pop_back();
        return true;
    }
};

// Persistent segment tree over node array
class PersistentSegTree {
    struct Node {
        int left = kNone, right = kNone;
        int64_t sum = 0;

        explicit Node(int64_t val) : sum(val) {
        }

        Node(int left, int right, int64_t sum) : left(left), right(right), sum(sum) {
        }
    };

    std::vector<Node> nodes_;
    int root_ = kNone, size_ = 0;

    int CreateNode(int value) {
        int id = nodes_.size();
        nodes_.emplace_back(value);
        return id;
    }

    int AddInnerNode(int left, int right) {
        int id = nodes_.size();
        int64_t sum = nodes_[left].sum + nodes_[right].sum;
        nodes_.emplace_back(left, right, sum);
        return id;
    }

    int Build(const std::vector<int>& arr, int left, int right) {
        if (left + 1 == right) {
            return CreateNode(arr[left]);
        }
        int mid = (left + right) / 2;
        return AddInnerNode(Build(arr, left, mid), Build(arr, mid, right));
    }

    int Update(int node, int pos, int val, int left, int right) {
        if (left + 1 == right) {
            return CreateNode(val);
        }
        int mid = (left + right) / 2;
        return pos < mid
               ? AddInnerNode(Update(nodes_[node].left, pos, val, left, mid), nodes_[node].right)
               : AddInnerNode(nodes_[node].left, Update(nodes_[node].right, pos, val, mid, right));
    }

    int64_t GetSum(int node, int from, int to, int left, int right) const {
        if (left == from && to == right) {
            return nodes_[node].sum;
        }
        int mid = (left + right) / 2;
        int64_t ans = 0;
        if (from < mid) {
            ans += GetSum(nodes_[node].left, from, std::min(mid, to), left, mid);
        }
        if (to > mid) {
            ans += GetSum(nodes_[node].right, std::max(from, mid), to, mid, right);
        }
        return ans;
    }

public:
    explicit PersistentSegTree(const std::vector<int>& arr) : size_(arr.size()) {
        root_ = Build(arr, 0, size_);
    }

    int GetRoot() const {
        return root_;
    }

    int Update(int root, int pos, int val) {
        return Update(root, pos, val, 0, size_);
    }

    int64_t GetSum(int root, int left, int right) const {
        return GetSum(root, left, right, 0, size_);
    }
};

// Linear update queries
class LinearSegTree {
    std::vector<int64_t> tree_, z0, z1;
    int size_;

    int64_t GetValue(int idx, int left, int right) {
        return tree_[idx] + z0[idx] * (right - left) + z1[idx] * ((right - left) * (right - left - 1LL) / 2);
    }

    void Push(int idx, int left, int right) {
        if (z0[idx] || z1[idx]) {
            int mid = (left + right) / 2;
            z0[2 * idx + 1] += z0[idx];
            z0[2 * idx + 2] += z0[idx] + z1[idx] * (mid - left);
            z1[2 * idx + 1] += z1[idx];
            z1[2 * idx + 2] += z1[idx];
            tree_[idx] = GetValue(idx, left, right);
            z0[idx] = z1[idx] = 0;
        }
    }

public:
    explicit LinearSegTree(const std::vector<int>& arr) {
        int size = arr.size();
        size_ = 1;
        while (size_ < size) {
            size_ *= 2;
        }

        tree_.assign(2 * size_ - 1, 0);
        z0.assign(2 * size_ - 1, 0);
        z1.assign(2 * size_ - 1, 0);
        for (int i = 0; i < size; ++i) {
            tree_[i + size_ - 1] = arr[i];
        }
        for (int i = size_ - 2; i >= 0; --i) {
            tree_[i] = tree_[2 * i + 1] + tree_[2 * i + 2];
        }
    }

    void Update(int from, int to, int64_t dz0 = 1, int idx = 0, int left = 0, int right = kNone) {
        if (right == kNone) {
            right = size_;
        }
        if (left == from && to == right) {
            ++z1[idx];
            z0[idx] += dz0;
            return;
        }

        Push(idx, left, right);
        int mid = (left + right) / 2;
        if (from < mid) {
            Update(from, std::min(mid, to), dz0, 2 * idx + 1, left, mid);
        }
        if (to > mid) {
            Update(std::max(from, mid), to, dz0, 2 * idx + 2, mid, right);
        }
        tree_[idx] = GetValue(2 * idx + 1, left, mid) + GetValue(2 * idx + 2, mid, right);
    }

    int64_t GetSum(int from, int to, int idx = 0, int left = 0, int right = kNone) {
        if (right == kNone) {
            right = size_;
        }
        if (left == from && to == right) {
            return GetValue(idx, left, right);
        }

        Push(idx, left, right);
        int mid = (left + right) / 2;
        int64_t ans = 0;
        if (from < mid) {
            ans += GetSum(from, std::min(mid, to), 2 * idx + 1, left, mid);
        }
        if (to > mid) {
            ans += GetSum(std::max(from, mid), to, 2 * idx + 2, mid, right);
        }
        return ans;
    }
};

int main() {
    // Case 1: segment tree for 3 range operations
    {
        int n, qs, left, right, val;
        std::string query;
        std::cin >> n >> qs;
        std::vector<int64_t> arr(n);
        for (auto& elem : arr) {
            std::cin >> elem;
        }

        RangeSegTree tree(arr);
        while (qs--) {
            std::cin >> query;
            if (query == "add") {
                std::cin >> left >> right >> val;
                tree.Increase(left - 1, right, val);
                std::cout << "add: OK\n";
            } else if (query == "assign") {
                std::cin >> left >> right >> val;
                tree.Assign(left - 1, right, val);
                std::cout << "assign: OK\n";
            } else if (query == "sum") {
                std::cin >> left >> right;
                std::cout << tree.GetSum(left - 1, right) << '\n';
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
        std::cout << '\n';
    }

    // Case 2: 2-dimensional segment tree for sums
    {
        int n, m, qs, x1, y1, x2, y2, val;
        std::string query;
        std::cin >> n >> m >> qs;
        std::vector<std::vector<int>> arr(n, std::vector<int>(m));
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                std::cin >> arr[i][j];
            }
        }

        SegTree2D tree(arr);
        while (qs--) {
            std::cin >> query;
            if (query == "update") {
                std::cin >> x1 >> y1 >> val;
                tree.Update(x1 - 1, y1 - 1, val);
                std::cout << "update: OK\n";
            } else if (query == "get") {
                std::cin >> x1 >> y1 >> x2 >> y2;
                std::cout << tree.GetSum(x1 - 1, x2, y1 - 1, y2) << '\n';
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
        std::cout << '\n';
    }

    // Case 3: dynamic segment tree for sums
    {
        int n, qs, left, right, pos, val;
        std::string query;
        std::cin >> n >> qs;
        std::vector<int> arr(n);
        for (auto& elem : arr) {
            std::cin >> elem;
        }

        DynamicSegTree tree(arr);
        while (qs--) {
            std::cin >> query;
            if (query == "update") {
                std::cin >> pos >> val;
                tree.Update(pos, val);
                std::cout << "update: OK\n";
            } else if (query == "get") {
                std::cin >> left >> right;
                std::cout << tree.GetSum(left, right) << '\n';
            } else if (query == "persist") {
                tree.Persist();
                std::cout << "persist: OK\n";
            } else if (query == "rollback") {
                std::cout << "rollback: ";
                std::cout << (tree.Rollback() ? "OK\n" : "No trees persist at the moment\n");
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
        std::cout << '\n';
    }

    // Case 4: persistent segment tree for sums
    {
        int n, qs, left, right, pos, val, idx;
        std::string query;
        std::cin >> n >> qs;
        std::vector<int> arr(n);
        for (auto& elem : arr) {
            std::cin >> elem;
        }

        PersistentSegTree tree(arr);
        std::vector<int> roots = {tree.GetRoot()};
        while (qs--) {
            std::cin >> query >> idx;
            if (query == "update") {
                std::cin >> pos >> val;
                roots.push_back(tree.Update(roots[idx], pos, val));
                std::cout << "update: OK\n";
            } else if (query == "get") {
                std::cin >> left >> right;
                std::cout << tree.GetSum(roots[idx], left, right) << '\n';
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
        std::cout << '\n';
    }

    // Case 5: segment tree for polynomial (linear) update queries
    {
        int n, qs, left, right;
        std::string query;
        std::cin >> n >> qs;
        std::vector<int> arr(n);
        for (auto& elem : arr) {
            std::cin >> elem;
        }

        LinearSegTree tree(arr);
        while (qs--) {
            std::cin >> query;
            if (query == "update") {
                std::cin >> left >> right;
                tree.Update(left - 1, right);
                std::cout << "update: OK\n";
            } else if (query == "get") {
                std::cin >> left >> right;
                std::cout << tree.GetSum(left, right) << '\n';
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
    }

    return 0;
}
