#include <algorithm>
#include <functional>
#include <iostream>
#include <stdexcept>

template<typename T>
class Stack {
    int size_ = 0, capacity_ = 8;
    T* arr_;

public:
    Stack() : arr_(new T[capacity_]) {
    }

    bool Empty() const {
        return !Size();
    }

    int Size() const {
        return size_;
    }

    const T Top() const {
        if (Empty()) {
            throw std::out_of_range("Stack is empty");
        }
        return arr_[size_ - 1];
    }

    void Pop() {
        if (Empty()) {
            throw std::out_of_range("Stack is empty");
        }
        --size_;
    }

    void Push(const T& elem) {
        if (size_ == capacity_) {
            capacity_ *= 2;
            T* new_arr = new T[capacity_];
            for (int i = 0; i < size_; ++i) {
                new_arr[i] = arr_[i];
            }
            delete[] arr_;
            arr_ = new_arr;
        }
        arr_[size_++] = elem;
    }

    template<typename ...Args>
    void Emplace(Args&&... args) {
        Push(T(std::forward<Args>(args)...));
    }

    void Clear() {
        while (!Empty()) {
            Pop();
        }
    }

    ~Stack() {
        delete[] arr_;
    }
};

template<typename T>
class Queue {
    Stack<T> in_, out_;

    void Rebalance() {
        if (out_.Empty()) {
            if (in_.Empty()) {
                throw std::out_of_range("Queue is empty");
            }
            do {
                out_.Push(in_.Top());
                in_.Pop();
            } while (!in_.Empty());
        }
    }

public:
    Queue() = default;

    bool Empty() const {
        return in_.Empty() && out_.Empty();
    }

    int Size() const {
        return in_.Size() + out_.Size();
    }

    const T Front() {
        Rebalance();
        return out_.Top();
    }

    void Pop() {
        Rebalance();
        out_.Pop();
    }

    void Push(const T& elem) {
        in_.Push(elem);
    }

    template<typename ...Args>
    void Emplace(Args&&... args) {
        Push(T(std::forward<Args>(args)...));
    }

    void Clear() {
        in_.Clear();
        out_.Clear();
    }
};

template<typename T>
class Deque {
    int left_ = 0, right_ = 0, capacity_ = 8;
    T* arr_;

public:
    Deque() : arr_(new T[capacity_]) {
    }

    bool Empty() const {
        return left_ == right_;
    }

    int Size() const {
        return right_ - left_;
    }

    const T Front() const {
        if (Empty()) {
            throw std::out_of_range("Deque is empty");
        }
        return arr_[left_];
    }

    const T back() const {
        if (Empty()) {
            throw std::out_of_range("Deque is empty");
        }
        return arr_[right_];
    }

    void PopFront() {
        if (Empty()) {
            throw std::out_of_range("Deque is empty");
        }
        ++left_;
    }

    void PopBack() {
        if (Empty()) {
            throw std::out_of_range("Deque is empty");
        }
        --right_;
    }

    void PushFront(const T& elem) {
        if (!left_) {
            T* new_arr = new T[capacity_ * 2];
            for (int i = 0; i < capacity_; ++i) {
                new_arr[i + capacity_] = arr_[i];
            }
            left_ += capacity_;
            right_ += capacity_;
            capacity_ *= 2;
            delete[] arr_;
            arr_ = new_arr;
        }
        arr_[--left_] = elem;
    }

    void PushBack(const T& elem) {
        if (right_ == capacity_) {
            T* new_arr = new T[capacity_ * 2];
            for (int i = 0; i < capacity_; ++i) {
                new_arr[i] = arr_[i];
            }
            capacity_ *= 2;
            delete[] arr_;
            arr_ = new_arr;
        }
        arr_[right_++] = elem;
    }

    template<typename ...Args>
    void EmplaceFront(Args&&... args) {
        PushFront(T(std::forward<Args>(args)...));
    }

    template<typename ...Args>
    void EmplaceBack(Args&&... args) {
        PushBack(T(std::forward<Args>(args)...));
    }

    void Clear() {
        while (!Empty()) {
            PopBack();
        }
    }

    ~Deque() {
        delete[] arr_;
    }
};

template<typename T, class Cmp = std::less<T>>
class ExtrStack {
    Stack<std::pair<T, T>> st_;
    std::function<bool(T, T)> cmp_ = Cmp();

    void AssertNonEmpty() const {
        if (st_.Empty()) {
            throw std::out_of_range("ExtrStack is empty");
        }
    }

public:
    ExtrStack() = default;

    bool Empty() const {
        return st_.Empty();
    }

    int Size() const {
        return st_.Size();
    }

    const T Top() const {
        AssertNonEmpty();
        auto [elem, extr] = st_.Top();
        return elem;
    }

    const T Extr() const {
        AssertNonEmpty();
        auto [elem, extr] = st_.Top();
        return extr;
    }

    void Pop() {
        AssertNonEmpty();
        st_.Pop();
    }

    void Push(const T& elem) {
        if (st_.Empty()) {
            st_.Emplace(elem, elem);
        } else {
            auto [top, extr] = st_.Top();
            st_.Emplace(elem, cmp_(elem, extr) ? extr : elem);
        }
    }

    template<typename ...Args>
    void Emplace(Args&&... args) {
        Push(T(std::forward<Args>(args)...));
    }

    void Clear() {
        st_.Clear();
    }
};


template<typename T, class Cmp = std::less<T>>
class ExtrQueue {
    ExtrStack<T, Cmp> in_, out_;
    std::function<bool(T, T)> cmp_ = Cmp();

    void AssertNonEmpty() const {
        if (in_.Empty()) {
            throw std::out_of_range("ExtrQueue is empty");
        }
    }

    void Rebalance() {
        if (out_.Empty()) {
            AssertNonEmpty();
            do {
                out_.Push(in_.Top());
                in_.Pop();
            } while (!in_.Empty());
        }
    }

public:
    ExtrQueue() = default;

    bool Empty() const {
        return in_.Empty() && out_.Empty();
    }

    int Size() const {
        return in_.Size() + out_.Size();
    }

    const T Front() {
        Rebalance();
        return out_.Top();
    }

    const T Extr() {
        Rebalance();
        return out_.Extr();
    }

    void Pop() {
        Rebalance();
        return out_.Pop();
    }

    void Push(const T& elem) {
        in_.Push(elem);
    }

    template<typename ...Args>
    void Emplace(Args&&... args) {
        Push(T(std::forward<Args>(args)...));
    }

    void Clear() {
        in_.Clear();
        out_.Clear();
    }
};

int main() {
    // Case #1: stack
    {
        int qs, arg;
        std::string query;
        Stack<int> st;
        std::cin >> qs;
        while (qs--) {
            std::cin >> query;
            if (query == "empty") {
                std::cout << (st.Empty() ? "YES\n" : "NO\n");
            } else if (query == "size") {
                std::cout << st.Size() << '\n';
            } else if (query == "push") {
                std::cin >> arg;
                st.Push(arg);
                std::cout << "push: OK\n";
            } else if (query == "pop") {
                st.Pop();
                std::cout << "pop: OK\n";
            } else if (query == "top") {
                std::cout << st.Top() << '\n';
            } else if (query == "clear") {
                st.Clear();
                std::cout << "clear: OK\n";
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
        std::cout << '\n';
    }

    // Case #2: queue
    {
        int qs, arg;
        std::string query;
        Queue<int> q;
        std::cin >> qs;
        while (qs--) {
            std::cin >> query;
            if (query == "empty") {
                std::cout << (q.Empty() ? "YES\n" : "NO\n");
            } else if (query == "size") {
                std::cout << q.Size() << '\n';
            } else if (query == "push") {
                std::cin >> arg;
                q.Push(arg);
                std::cout << "push: OK\n";
            } else if (query == "pop") {
                q.Pop();
                std::cout << "pop: OK\n";
            } else if (query == "front") {
                std::cout << q.Front() << '\n';
            } else if (query == "clear") {
                q.Clear();
                std::cout << "clear: OK\n";
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
        std::cout << '\n';
    }

    // Case #3: deque
    {
        int qs, arg;
        std::string query;
        Deque<int> dq;
        std::cin >> qs;
        while (qs--) {
            std::cin >> query;
            if (query == "empty") {
                std::cout << (dq.Empty() ? "YES\n" : "NO\n");
            } else if (query == "size") {
                std::cout << dq.Size() << '\n';
            } else if (query == "PushFront") {
                std::cin >> arg;
                dq.PushFront(arg);
                std::cout << "PushFront: OK\n";
            } else if (query == "PopFront") {
                dq.PopFront();
                std::cout << "PopFront: OK\n";
            } else if (query == "PushBack") {
                std::cin >> arg;
                dq.PushBack(arg);
                std::cout << "PushBack: OK\n";
            } else if (query == "PopBack") {
                dq.PopBack();
                std::cout << "PopBack: OK\n";
            } else if (query == "front") {
                std::cout << dq.Front() << '\n';
            } else if (query == "back") {
                std::cout << dq.back() << '\n';
            } else if (query == "clear") {
                dq.Clear();
                std::cout << "clear: OK\n";
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
        std::cout << '\n';
    }

    // Case #4: stack with max() in O(1)
    {
        int qs, arg;
        std::string query;
        ExtrStack<int> st;
        std::cin >> qs;
        while (qs--) {
            std::cin >> query;
            if (query == "empty") {
                std::cout << (st.Empty() ? "YES\n" : "NO\n");
            } else if (query == "size") {
                std::cout << st.Size() << '\n';
            } else if (query == "push") {
                std::cin >> arg;
                st.Push(arg);
                std::cout << "push: OK\n";
            } else if (query == "pop") {
                st.Pop();
                std::cout << "pop: OK\n";
            } else if (query == "top") {
                std::cout << st.Top() << '\n';
            } else if (query == "max") {
                std::cout << st.Extr() << '\n';
            } else if (query == "clear") {
                st.Clear();
                std::cout << "clear: OK\n";
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
        std::cout << '\n';
    }

    // Case #5: queue with min() in O(1)
    {
        int qs, arg;
        std::string query;
        ExtrQueue<int, std::greater<int>> q;  // custom comparator class should do too
        std::cin >> qs;
        while (qs--) {
            std::cin >> query;
            if (query == "empty") {
                std::cout << (q.Empty() ? "YES\n" : "NO\n");
            } else if (query == "size") {
                std::cout << q.Size() << '\n';
            } else if (query == "push") {
                std::cin >> arg;
                q.Push(arg);
                std::cout << "push: OK\n";
            } else if (query == "pop") {
                q.Pop();
                std::cout << "pop: OK\n";
            } else if (query == "front") {
                std::cout << q.Front() << '\n';
            } else if (query == "max") {  // 'min' in this case
                std::cout << q.Extr() << '\n';
            } else if (query == "clear") {
                q.Clear();
                std::cout << "clear: OK\n";
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
    }

    return 0;
}
