#include <algorithm>
#include <ctime>
#include <iostream>
#include <memory>
#include <optional>
#include <random>
#include <string>
#include <utility>
#include <vector>

constexpr int kInf = 1e9;
constexpr int kNone = -1;

struct Nil {
};

// BST
template <typename K, typename V = Nil>
class Treap {
    struct Node;
    using Ptr = std::shared_ptr<Node>;

    std::mt19937 gen_;
    std::uniform_int_distribution<int> distr_;
    Ptr root_ = nullptr;
    size_t size_;

    int GetRand() {
        return distr_(gen_);
    }

    struct Node {
        K key;
        V value;
        Ptr left = nullptr, right = nullptr;
        int prior;

        Node(const K& key, const V& value, int prior) : key(key), value(value), prior(prior) {
        }
    };

    static void Split(const Ptr tree, Ptr& left, Ptr& right, const K& key) {
        if (!tree) {
            left = right = nullptr;
        } else if (key < tree->key) {
            Split(tree->left, left, tree->left, key);
            right = tree;
        } else {
            Split(tree->right, tree->right, right, key);
            left = tree;
        }
    }

    static void Merge(Ptr& tree, const Ptr left, const Ptr right) {
        if (!left || !right) {
            tree = left ? left : right;
        } else if (left->prior > right->prior) {
            Merge(left->right, left->right, right);
            tree = left;
        } else {
            Merge(right->left, left, right->left);
            tree = right;
        }
    }

    static bool Insert(Ptr& tree, const Ptr node) {
        if (!tree) {
            tree = node;
        } else if (tree->prior == node->prior) {
            return false;
        } else if (tree->prior < node->prior) {
            Split(tree, node->left, node->right, node->key);
            tree = node;
        } else {
            return Insert(node->key < tree->key ? tree->left : tree->right, node);
        }
        return true;
    }

    static bool Erase(Ptr& tree, const K& key) {
        if (!tree) {
            return false;
        } else if (tree->key == key) {
            Merge(tree, tree->left, tree->right);
            return true;
        } else {
            return Erase(key < tree->key ? tree->left : tree->right, key);
        }
    }

public:
    Treap() = default;

    size_t Size() const {
        return size_;
    }

    bool Empty() const {
        return !Size();
    }

    std::pair<K, V> GetMin() const {
        if (Empty()) {
            throw std::runtime_error("GetMin(): BST is empty");
        }
        Ptr node = root_;
        while (node->left) {
            node = node->left;
        }
        return std::make_pair(node->key, node->value);
    }

    std::optional<V> Find(const K& key) const {
        Ptr node = root_;
        while (node) {
            if (key == node->key) {
                return node->value;
            } else if (key < node->key) {
                node = node->left;
            } else {
                node = node->right;
            }
        }
        return std::nullopt;
    }

    void Insert(const K& key, const V& value = {}) {
        if (Find(key)) {
            Erase(key);
        }
        Ptr node = std::make_shared<Node>(key, value, GetRand());
        while (!Insert(root_, node)) {
            node->prior = GetRand();
        }
        ++size_;
    }

    bool Erase(const K& key) {
        bool result = Erase(root_, key);
        size_ -= result;
        return result;
    }

    const K FindNext(const K& key) const {
        Ptr cur = root_;
        K mn = kInf;
        while (cur) {
            if (key > cur->key) {
                cur = cur->right;
            } else {
                mn = std::min(mn, cur->key);
                cur = cur->left;
            }
        }
        return mn;
    }
};

// Extended random integer from uniform distribution
inline int GetRand() {
    return (rand() << 15) ^ rand();
}

// RMQ
class ImplicitTreap {
    struct Node;
    using Ptr = Node*;

    struct Node {
        int prior, cnt, key, value;
        Ptr left, right;

        Node(int x, const Ptr left = nullptr, const Ptr right = nullptr) :
            prior(GetRand()), cnt(1), key(x), value(x), left(left), right(right) {
        }
    };

    static int GetCnt(const Ptr tree) {
        return tree ? tree->cnt : 0;
    }

    static void UpdateCnt(const Ptr tree) {
        if (tree) {
            tree->cnt = GetCnt(tree->left) + GetCnt(tree->right) + 1;
        }
    }

    static int GetMin(const Ptr tree) {
        return tree ? tree->value : kInf;
    }

    static void UpdateMin(const Ptr tree) {
        if (tree) {
            tree->value = std::min(std::min(GetMin(tree->left), GetMin(tree->right)), tree->key);
        }
    }

    static void Split(const Ptr tree, Ptr& left, Ptr& right, int x, int add = 0) {
        if (!tree) {
            left = right = nullptr;
            return;
        }
        int key = GetCnt(tree->left) + add;
        if (x <= key) {
            Split(tree->left, left, tree->left, x, add);
            right = tree;
        } else {
            Split(tree->right, tree->right, right, x, add + GetCnt(tree->left) + 1);
            left = tree;
        }
        UpdateCnt(tree), UpdateMin(tree);
    }

    static void Merge(Ptr& tree, const Ptr left, const Ptr right) {
        if (!left || !right) {
            tree = left ? left : right;
        } else if (left->prior > right->prior) {
            Merge(left->right, left->right, right);
            tree = left;
        } else {
            Merge(right->left, left, right->left);
            tree = right;
        }
        UpdateCnt(tree), UpdateMin(tree);
    }

    static void Traverse(const Ptr tree) {
        if (tree) {
            Traverse(tree->left);
            printf("%d (%d, %d, %d)\n", tree->key, tree->prior, tree->cnt, tree->value);
            Traverse(tree->right);
        }
    }

    Ptr root_ = nullptr;

public:
    ImplicitTreap() = default;

    void Insert(int pos, int x) {
        Ptr t1, t2;
        Split(root_, t1, t2, pos);
        Merge(t1, t1, new Node(x));
        Merge(root_, t1, t2);
    }

    void Erase(int pos) {
        Ptr t1, t2, t3;
        Split(root_, t1, t3, pos + 1);
        Split(t1, t1, t2, pos);
        Merge(root_, t1, t3);
    }

    int GetMin(int left, int right) {
        Ptr t1, t2, t3;
        Split(root_, t1, t2, left);
        Split(t2, t2, t3, right - left + 1);
        int res = GetMin(t2);
        Merge(root_, t1, t2);
        Merge(root_, root_, t3);
        return res;
    }

    void Traverse() const {
        Traverse(root_);
    }
};

/*
* Persistent implicit treap over node array
* Supports 3 types of queries
* - copy(a, b, L): copy an array segment [a..a+L) to [b..b+L)
* - sum(a, b): get sum of an array segment [a..b]
* - print(a, b): print out the elements of segment [a..b]
*/
class PersistentTreap {
    struct Node {
        int64_t sum;
        int key, size;
        int left, right;

        Node(int64_t sum, int key, int size, int left, int right)
            : sum(sum), key(key), size(size), left(left), right(right) {
        }
    };

    std::mt19937 gen_;
    std::uniform_real_distribution<double> distr_;
    std::vector<Node> nodes_;
    int root_ = kNone;

    bool RollDice(double prob) {
        return distr_(gen_) < prob;
    }

    int GetSize(int id) const {
        return id == kNone ? 0 : nodes_[id].size;
    }

    int64_t GetSum(int id) const {
        return id == kNone ? 0 : nodes_[id].sum;
    }

    int CreateNode(int key, int left = kNone, int right = kNone) {
        int id = nodes_.size();
        int64_t sum = GetSum(left) + GetSum(right) + key;
        int size = GetSize(left) + GetSize(right) + 1;
        nodes_.emplace_back(sum, key, size, left, right);
        return id;
    }

    int Merge(int left, int right) {
        if (left == kNone || right == kNone) {
            return left == kNone ? right : left;
        }

        int left_sz = GetSize(left), right_sz = GetSize(right);
        if (RollDice((left_sz + .0) / (left_sz + right_sz))) {
            return CreateNode(nodes_[left].key, nodes_[left].left,
                              Merge(nodes_[left].right, right));
        }
        return CreateNode(nodes_[right].key, Merge(left, nodes_[right].left), nodes_[right].right);
    }

    std::pair<int, int> Split(int tree, int pos) {
        if (tree == kNone) {
            return {kNone, kNone};
        }

        int idx = GetSize(nodes_[tree].left);
        if (pos <= idx) {
            auto [left, right] = Split(nodes_[tree].left, pos);
            return {left, CreateNode(nodes_[tree].key, right, nodes_[tree].right)};
        } else {
            auto [left, right] = Split(nodes_[tree].right, pos - idx - 1);
            return {CreateNode(nodes_[tree].key, nodes_[tree].left, left), right};
        }
    }

    void Traverse(int tree, std::vector<int>& output) const {
        if (tree == kNone) {
            return;
        }
        Traverse(nodes_[tree].left, output);
        output.push_back(nodes_[tree].key);
        Traverse(nodes_[tree].right, output);
    }

public:
    explicit PersistentTreap(const std::vector<int>& arr) {
        nodes_.reserve(arr.size());
        for (auto elem : arr) {
            root_ = Merge(root_, CreateNode(elem));
        }
    }

    void Copy(int from, int to, int length) {
        auto [from_head, from_third] = Split(root_, from + length - 1);
        auto [from_first, from_second] = Split(from_head, from - 1);
        auto [to_head, to_third] = Split(root_, to + length - 1);
        auto [to_first, to_second] = Split(to_head, to - 1);
        root_ = Merge(Merge(to_first, from_second), to_third);
    }

    int64_t Sum(int from, int to) {
        auto [head, third] = Split(root_, to);
        auto [first, second] = Split(head, from - 1);
        return GetSum(second);
    }

    auto Print(int from, int to) {
        auto [head, third] = Split(root_, to);
        auto [first, second] = Split(head, from - 1);
        std::vector<int> output;
        output.reserve(to - from + 1);
        Traverse(second, output);
        return output;
    }
};

// String with MoveToBack()
class Rope {
    struct Node;
    using Ptr = Node*;

    struct Node {
        int prior, cnt;
        Ptr left = nullptr, right = nullptr;
        char c;

        Node(char c) : prior(GetRand()), c(c) {
        }
    };

    static int GetCnt(const Ptr tree) {
        return tree ? tree->cnt : 0;
    }

    static void UpdateCnt(const Ptr tree) {
        if (tree) {
            tree->cnt = GetCnt(tree->left) + GetCnt(tree->right) + 1;
        }
    }

    static void Split(const Ptr tree, Ptr& left, Ptr& right, int x, int add = 0) {
        if (!tree) {
            left = right = nullptr;
            return;
        }
        int key = GetCnt(tree->left) + add;
        if (x <= key) {
            Split(tree->left, left, tree->left, x, add);
            right = tree;
        } else {
            Split(tree->right, tree->right, right, x, add + GetCnt(tree->left) + 1);
            left = tree;
        }
        UpdateCnt(tree);
    }

    static void Merge(Ptr& tree, const Ptr left, const Ptr right) {
        if (!left || !right) {
            tree = left ? left : right;
        } else if (left->prior > right->prior) {
            Merge(left->right, left->right, right);
            tree = left;
        } else {
            Merge(right->left, left, right->left);
            tree = right;
        }
        UpdateCnt(tree);
    }

    static void Traverse(const Ptr tree, std::string& res) {
        if (tree) {
            Traverse(tree->left, res);
            res += tree->c;
            Traverse(tree->right, res);
        }
    }

    Ptr root_ = nullptr;

public:
    Rope() = default;

    explicit Rope(const std::string& s) {
        for (int i = 0; i < static_cast<int>(s.length()); ++i) {
            Insert(i, s[i]);
        }
    }

    void Insert(int pos, char c) {
        Ptr t1, t2;
        Split(root_, t1, t2, pos);
        Merge(t1, t1, new Node(c));
        Merge(root_, t1, t2);
    }

    void MoveToBack(int left, int right) {
        Ptr t1, t2, t3;
        Split(root_, t1, t2, left);
        Split(t2, t2, t3, right - left + 1);
        Merge(root_, t1, t3);
        Merge(root_, root_, t2);
    }

    auto Traverse() const {
        std::string res;
        Traverse(root_, res);
        return res;
    }
};

// RSQ with subarray reversals
class ReversalTreap {
    struct Node;
    using Ptr = Node*;

    struct Node {
        int prior, key, cnt = 1;
        Ptr left = nullptr, right = nullptr;
        int64_t sum;
        bool rev = false;

        Node(int x) : prior(GetRand()), key(x), sum(x) {}
    };

    static int GetCnt(Ptr tree) {
        return tree ? tree->cnt : 0;
    }

    static void UpdateCnt(Ptr& tree) {
        if (tree) {
            tree->cnt = GetCnt(tree->left) + GetCnt(tree->right) + 1;
        }
    }

    static int64_t GetSum(Ptr tree) {
        return tree ? tree->sum : 0;
    }

    static void UpdateSum(Ptr& tree) {
        if (tree) {
            tree->sum = GetSum(tree->left) + GetSum(tree->right) + tree->key;
        }
    }

    static void Push(Ptr tree) {
        if (tree && tree->rev) {
            tree->rev = false;
            std::swap(tree->left, tree->right);
            if (tree->left) {
                tree->left->rev ^= 1;
            }
            if (tree->right) {
                tree->right->rev ^= 1;
            }
        }
    }

    static void Split(Ptr tree, Ptr& left, Ptr& right, int x, int add = 0) {
        Push(tree);
        if (!tree) {
            left = right = nullptr;
            return;
        }
        int key = GetCnt(tree->left) + add;
        if (x <= key) {
            Split(tree->left, left, tree->left, x, add);
            right = tree;
        } else {
            Split(tree->right, tree->right, right, x, add + GetCnt(tree->left) + 1);
            left = tree;
        }
        UpdateCnt(tree), UpdateSum(tree);
    }

    static void Merge(Ptr& tree, Ptr left, Ptr right) {
        Push(left);
        Push(right);
        if (!left || !right) {
            tree = left ? left : right;
        } else if (left->prior > right->prior) {
            Merge(left->right, left->right, right);
            tree = left;
        } else {
            Merge(right->left, left, right->left);
            tree = right;
        }
        UpdateCnt(tree), UpdateSum(tree);
    }

    Ptr root_ = nullptr;

public:
    explicit ReversalTreap(const std::vector<int>& arr) {
        for (int i = 0; i < static_cast<int>(arr.size()); ++i) {
            Insert(i, arr[i]);
        }
    }

    void Insert(int pos, int x) {
        Ptr t1, t2;
        Split(root_, t1, t2, pos);
        Merge(t1, t1, new Node(x));
        Merge(root_, t1, t2);
    }

    void Reverse(int left, int right) {
        Ptr t1, t2, t3;
        Split(root_, t1, t2, left);
        Split(t2, t2, t3, right - left + 1);
        t2->rev ^= 1;
        Merge(root_, t1, t2);
        Merge(root_, root_, t3);
    }

    int64_t GetSum(int left, int right) {
        Ptr t1, t2, t3;
        Split(root_, t1, t2, left);
        Split(t2, t2, t3, right - left + 1);
        int64_t res = GetSum(t2);
        Merge(root_, t1, t2);
        Merge(root_, root_, t3);
        return res;
    }
};

int main() {
    // Case 1: simple treap (insert, erase, next)
    {
        srand(time(nullptr));
        int qs, arg;
        std::cin >> qs;

        std::string query;
        Treap<int> treap;
        while (qs--) {
            std::cin >> query;
            if (query == "+") {
                std::cin >> arg;
                treap.Insert(arg);
                std::cout << "insert: OK\n";
            } else if (query == "-") {
                std::cin >> arg;
                std::cout << "erase: ";
                std::cout << (treap.Erase(arg) ? "OK\n" : "Not found\n");
            } else if (query == "?") {
                std::cin >> arg;
                int res = treap.FindNext(arg);
                std::cout << (res == kInf ? kNone : res) << '\n';
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
    }

    // Case 2: implicit treap (dynamic RMQ)
    {
        srand(time(nullptr));
        int qs, idx, arg, left ,right;
        std::cin >> qs;

        std::string query;
        ImplicitTreap treap;
        while (qs--) {
            std::cin >> query;
            if (query == "+") {
                std::cin >> idx >> arg;
                --idx;
                treap.Insert(idx, arg);
                std::cout << "insert: OK\n";
            } else if (query == "-") {
                std::cin >> idx;
                --idx;
                treap.Erase(idx);
                std::cout << "erase: OK\n";
            } else if (query == "?") {
                std::cin >> left >> right;
                --left, --right;
                std::cout << treap.GetMin(left, right) << '\n';
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
        std::cout << '\n';
    }

    // Case 3:
    {
        int n, qs, left, right, len;
        std::cin >> n >> qs;
        std::vector<int> arr(n);
        for (auto& elem : arr) {
            std::cin >> elem;
        }

        std::string query;
        PersistentTreap treap(arr);
        while (qs--) {
            std::cin >> query >> left >> right;
            --left, --right;
            if (query == "print") {
                for (auto elem : treap.Print(left, right)) {
                    std::cout << elem << ' ';
                }
                std::cout << '\n';
            } if (query == "sum") {
                std::cout << treap.Sum(left, right) << '\n';
            } else if (query == "copy") {
                std::cin >> len;
                treap.Copy(left, right, len);
                std::cout << "copy: OK\n";
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
    }

    // Case 4: rope (see CSES 2072)
    {
        int qs, left, right;
        std::string s;
        std::cin >> qs >> s;

        Rope rope(s);
        while (qs--) {
            std::cin >> left >> right;
            --left, --right;
            rope.MoveToBack(left, right);
        }
        std::cout << rope.Traverse() << "\n\n";
    }

    // Case 5: reversals and sums (see CSES 2074)
    {
        int n, qs, left, right;
        std::cin >> n >> qs;
        std::vector<int> arr(n);
        for (auto& elem : arr) {
            std::cin >> elem;
        }

        std::string query;
        ReversalTreap treap(arr);
        while (qs--) {
            std::cin >> query >> left >> right;
            --left, --right;
            if (query == "sum") {
                std::cout << treap.GetSum(left, right) << '\n';
            } else if (query == "reverse") {
                treap.Reverse(left, right);
                std::cout << "reverse: OK\n";
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
    }

    return 0;
}
