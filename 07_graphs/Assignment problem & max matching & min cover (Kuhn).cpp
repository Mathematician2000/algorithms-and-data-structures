#include <algorithm>
#include <iostream>
#include <queue>
#include <utility>
#include <vector>

constexpr int kInf = 1e9;
constexpr int kNone = -1;

struct EdgeTarget {
    int target, id;

    EdgeTarget(int target, int id) : target(target), id(id) {
    }
};

struct Edge {
    int flow = 0, capacity, cost;
    int reverse_id;

    Edge(int capacity, int cost, int reverse_id)
        : capacity(capacity), cost(cost), reverse_id(reverse_id) {
    }
};

class FlowNetwork {
    std::vector<std::vector<EdgeTarget>> graph_;
    std::vector<Edge> edges_;
    int source_, sink_;

public:
    explicit FlowNetwork(int size) : graph_(size) {
    }

    int Size() const {
        return graph_.size();
    }

    int GetSource() const {
        return source_;
    }

    int GetSink() const {
        return sink_;
    }

    void SetSource(int source) {
        source_ = source;
    }

    void SetSink(int sink) {
        sink_ = sink;
    }

    void AddDirectedEdge(int source, int target, int capacity, int cost) {
        int id = edges_.size();
        graph_[source].emplace_back(target, id);
        edges_.emplace_back(capacity, cost, id + 1);
        graph_[target].emplace_back(source, id + 1);
        edges_.emplace_back(0, -cost, id);
    }

    int GetResidualCapacity(int id) const {
        auto [flow, capacity, cost, reverse_id] = edges_[id];
        return capacity - flow;
    }

    int GetCost(int id) const {
        return edges_[id].cost;
    }

    int UpdateFlow(int id, int pushed) {
        auto& [flow, capacity, cost, reverse_id] = edges_[id];
        flow += pushed;
        edges_[reverse_id].flow -= pushed;
        return pushed * cost;
    }

    const auto& operator[](int vertex) const {
        return graph_[vertex];
    }
};

// SPFA algorithm: O(VE)
auto SPFA(const FlowNetwork& network) {
    int size = network.Size(), source = network.GetSource();
    std::vector<int> dist(size, kInf);
    std::vector<std::pair<int, int>> parent(size, {kNone, kNone});
    std::vector<bool> used(size);
    std::queue<int> queue;

    dist[source] = 0;
    used[source] = true;
    queue.push(source);

    while (!queue.empty()) {
        int vertex = queue.front();
        queue.pop();
        used[vertex] = false;
        for (auto [next, id] : network[vertex]) {
            int residual = network.GetResidualCapacity(id);
            int cost = network.GetCost(id);
            if (residual > 0 && dist[next] > dist[vertex] + cost) {
                dist[next] = dist[vertex] + cost;
                parent[next] = {vertex, id};
                if (!used[next]) {
                    used[next] = true;
                    queue.push(next);
                }
            }
        }
    }

    return parent;
}

// Find min-cost flow of size k (or min-cost-max-flow for k = kInf): O(V^2 E^2)
auto MinCostMaxFlow(FlowNetwork& network, int bound = kInf) {
    int source = network.GetSource(), sink = network.GetSink();
    int flow = 0, cost = 0;
    while (flow < bound) {
        auto parent = SPFA(network);
        if (parent[sink].first == kNone) {
            break;
        }

        // Traversing the augmenting path
        int pushed = bound - flow, vertex = sink;
        while (vertex != source) {
            auto [prev, id] = parent[vertex];
            pushed = std::min(pushed, network.GetResidualCapacity(id));
            vertex = prev;
        }

        // Updating flow along the augmenting path
        vertex = sink;
        while (vertex != sink) {
            auto [prev, id] = parent[vertex];
            cost += network.UpdateFlow(id, pushed);
            vertex = prev;
        }
        flow += pushed;
    }
    return std::make_pair(flow, cost);
}

class BipartiteGraph {
    std::vector<std::vector<int>> edges_;
    std::vector<int> left_, right_;

public:
    explicit BipartiteGraph(int size) : edges_(size) {
    }

    int Size() const {
        return edges_.size();
    }

    void AddDirectedEdge(int from, int to) {
        edges_[from].push_back(to);
    }

    void AddLeftVertex(int vertex) {
        left_.push_back(vertex);
    }

    void AddRightVertex(int vertex) {
        right_.push_back(vertex);
    }

    const auto& GetLeftPart() const {
        return left_;
    }

    const auto& GetRightPart() const {
        return right_;
    }

    const auto& operator[](int vertex) const {
        return edges_[vertex];
    }
};

// Kuhn's algorithm for maximum bipartite matching in O(n^2 m)
bool KuhnDFS(const BipartiteGraph& graph, std::vector<bool>& used,
             std::vector<int>& matching, int vertex) {
    used[vertex] = true;
    for (auto next : graph[vertex]) {
        int opposite = matching[next];
        // heuristic: try to split into 2 separate cycles (being lazy to run DFS)
        if (opposite == kNone || (!used[opposite] && KuhnDFS(graph, used, matching, opposite))) {
            matching[next] = vertex;
            return true;
        }
    }
    return false;
}

// Ordinary DFS (for min cover)
void DFS(const BipartiteGraph& graph, std::vector<bool>& used, int vertex) {
    used[vertex] = true;
    for (auto next : graph[vertex]) {
        if (!used[next]) {
            DFS(graph, used, next);
        }
    }
}

// Minimum vertex cover in bipartite graph
auto MinCover(const BipartiteGraph& graph, std::vector<int>& matching) {
    int size = graph.Size();
    BipartiteGraph cover_graph(size);
    for (auto left : graph.GetLeftPart()) {
        for (auto right : graph[left]) {
            if (matching[left] == right) {
                cover_graph.AddDirectedEdge(right, left);
            } else {
                cover_graph.AddDirectedEdge(left, right);
            }
        }
    }

    std::vector<bool> used(size);
    for (auto left : graph.GetLeftPart()) {
        if (!used[left] && matching[left] == kNone) {
            DFS(cover_graph, used, left);
        }
    }

    std::vector<int> cover;
    for (auto left : graph.GetLeftPart()) {
        if (!used[left]) {
            cover.push_back(left);
        }
    }
    for (auto right : graph.GetRightPart()) {
        if (used[right]) {
            cover.push_back(right);
        }
    }

    return cover;
}

// Solve assignment problem using Hungarian algorithm in O(n^2 m)
auto AssignmentHungarian(const std::vector<std::vector<int>>& table) {
    int rows = table.size() - 1, cols = table[0].size() - 1;
    std::vector<int> u(rows + 1), v(cols + 1), perm(cols + 1), prev(cols + 1);
    for (int row = 1; row <= rows; ++rows) {
        perm[0] = row;
        int j0 = 0;
        std::vector<int> vmin(cols + 1, kInf);
        std::vector<bool> used(cols + 1);
        do {
            used[j0] = true;
            int i0 = perm[j0], delta = kInf, jmin = 0;
            for (int col = 1; col <= cols; ++col) {
                if (!used[col]) {
                    int d = table[i0][col] - u[i0] - v[col];
                    if (d < vmin[col]) {
                        vmin[col] = d, prev[col] = j0;
                    }
                    if (vmin[col] < delta) {
                        delta = vmin[col], jmin = col;
                    }
                }
            }
            if (delta) {
                for (int col = 0; col <= cols; ++col) {
                    if (used[col]) {
                        u[perm[col]] += delta;
                        v[col] -= delta;
                    } else {
                        vmin[col] -= delta;
                    }
                }
            }
            j0 = jmin;
        } while (perm[j0]);
        do {
            int j1 = prev[j0];
            perm[j0] = perm[j1];
            j0 = j1;
        } while (j0);
    }
    return perm;
}

int main() {
    // Case 1: solve assignment problem using min-cost-max-flow in O(n^2 m^2)
    {
        int n, m, c;
        std::cin >> n >> m;
        int source = 0, sink = n + m + 1;
        FlowNetwork network(sink + 1);
        network.SetSource(source);
        network.SetSink(sink);

        for (int u = 1; u <= n; ++u) {
            network.AddDirectedEdge(source, u, 1, 0);
            for (int v = 1; v <= m; ++v) {
                std::cin >> c;
                network.AddDirectedEdge(u, v + n, 1, c);
            }
        }
        for (int v = 1; v <= m; ++v) {
            network.AddDirectedEdge(v + n, sink, 1, 0);
        }

        auto [_, cost] = MinCostMaxFlow(network);
        for (int u = 1; u <= n; ++u) {
            for (auto [v, id] : network[u]) {
                if (network.GetResidualCapacity(id) == 0) {
                    printf("%d - %d\n", u, v);
                    break;
                }
            }
        }
        printf("Cost = %d\n\n", cost);
    }

    // Case 2: find maximum matching using Kuhn's algorithm in O(VE) = O(n^2 m)
    {
        int n, m, k, u, v;
        std::cin >> n >> m >> k;
        BipartiteGraph graph(n + m);  // n x m
        for (int u = 0; u < n; ++u) {
            graph.AddLeftVertex(u);
        }
        for (int v = 0; v < m; ++v) {
            graph.AddRightVertex(v + n);
        }
        while (k--) {
            std::cin >> u >> v;
            graph.AddDirectedEdge(u, v + n);
        }

        std::vector<int> matching(n + m, kNone);
        std::vector<bool> used;
        for (auto u : graph.GetLeftPart()) {
            used.assign(n, false);
            KuhnDFS(graph, used, matching, u);
        }

        for (auto u : graph.GetLeftPart()) {
            if (matching[u] != kNone) {
                printf("%d - %d\n", matching[u], u);
            }
        }
        std::cout << '\n';
    }

    // Case 3: solve assignment problem using Hungarian algorithm in O(n^2 m)
    {
        int n, m;
        std::cin >> n >> m;
        std::vector<std::vector<int>> table(n + 1, std::vector<int>(m + 1));
        for (auto& row : table) {
            for (auto& elem : row) {
                std::cin >> elem;
            }
        }

        auto permutation = AssignmentHungarian(table);

        int cost = 0;
        for (int col = 1; col <= m; ++col) {
            if (permutation[col]) {
                printf("%d - %d\n", permutation[col], col);
                cost += table[permutation[col]][col];
            }
        }
        printf("Cost = %d\n\n", cost);
    }

    // Case 4: modified Kuhn's algorithm in O(VE) = O(n^2 m) with greedy heuristic
    {
        int n, m, k, u, v;
        std::cin >> n >> m >> k;
        BipartiteGraph graph(n + m);  // n x m
        for (int u = 0; u < n; ++u) {
            graph.AddLeftVertex(u);
        }
        for (int v = 0; v < m; ++v) {
            graph.AddRightVertex(v + n);
        }
        while (k--) {
            std::cin >> u >> v;
            graph.AddDirectedEdge(u, v + n);
        }

        std::vector<int> matching(n + m, kNone);
        std::vector<bool> used, pre_used(n);
        // greedy 'warm up' heuristic gives at least 50% of maximum matching
        for (int u = 0; u < n; ++u) {
            for (auto v : graph[u]) {
                if (matching[v] == kNone) {
                    matching[v] = u;
                    pre_used[u] = true;
                    break;
                }
            }
        }
        for (auto u : graph.GetLeftPart()) {
            if (!pre_used[u]) {
                used.assign(n, false);
                KuhnDFS(graph, used, matching, u);
            }
        }

        for (auto u : graph.GetLeftPart()) {
            if (matching[u] != kNone) {
                printf("%d - %d\n", matching[u], u);
            }
        }
        std::cout << '\n';
    }

    // Case 5: modified Kuhn's algorithm in O(EM) = O(M * nm)
    {
        int n, m, k, u, v;
        std::cin >> n >> m >> k;
        BipartiteGraph graph(n + m);  // n x m
        for (int u = 0; u < n; ++u) {
            graph.AddLeftVertex(u);
        }
        for (int v = 0; v < m; ++v) {
            graph.AddRightVertex(v + n);
        }
        while (k--) {
            std::cin >> u >> v;
            graph.AddDirectedEdge(u, v + n);
        }

        std::vector<int> matching(n + m, kNone);
        std::vector<bool> used;
        for (auto u : graph.GetLeftPart()) {
            // clear used only if dfs succeeded
            // heuristic can be used simultaneously with the greedy one :)
            if (KuhnDFS(graph, used, matching, u)) {
                used.assign(n, false);
            }
        }

        for (auto u : graph.GetLeftPart()) {
            if (matching[u] != kNone) {
                printf("%d - %d\n", matching[u], u);
            }
        }
        std::cout << '\n';
    }

    // Case 6: modified Kuhn's algorithm in O(V + E) = O(nm)
    {
        int n, m, k, u, v;
        std::cin >> n >> m >> k;
        BipartiteGraph graph(n + m);  // n x m
        for (int u = 0; u < n; ++u) {
            graph.AddLeftVertex(u);
        }
        for (int v = 0; v < m; ++v) {
            graph.AddRightVertex(v + n);
        }
        while (k--) {
            std::cin >> u >> v;
            graph.AddDirectedEdge(u, v + n);
        }

        std::vector<int> matching(n + m, kNone);
        std::vector<bool> used;
        // no need to use greedy heuristic!
        bool repeat;
        do {
            repeat = false;
            used.assign(n, false);  // 'int used' technique may come in handy
            for (auto u : graph.GetLeftPart()) {
                if (matching[u] == kNone && !used[u] && KuhnDFS(graph, used, matching, u)) {
                    repeat = true;
                }
            }
        } while (repeat);

        for (auto u : graph.GetLeftPart()) {
            if (matching[u] != kNone) {
                printf("%d - %d\n", matching[u], u);
            }
        }
        std::cout << '\n';
    }

    return 0;
}
