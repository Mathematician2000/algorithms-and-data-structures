#include <iostream>
#include <queue>
#include <vector>

constexpr int kNone = -1;

class Graph {
    std::vector<std::vector<int>> edges_;

public:
    explicit Graph(int size) : edges_(size) {
    }

    int Size() const {
        return edges_.size();
    }

    void AddEdge(int from, int to) {
        edges_[from].push_back(to);
        if (from != to) {
            edges_[to].push_back(from);
        }
    }

    const auto& operator[](int vertex) const {
        return edges_[vertex];
    }
};

// Ordinary BFS in O(V + E)
void BFS(const Graph& graph,
         std::vector<int>& dist,
         std::vector<int>& parent,
         int source) {
    int size = graph.Size();
    std::vector<bool> used(size);
    std::queue<int> queue;

    used[source] = true;
    dist[source] = 0;
    queue.emplace(source);

    while (!queue.empty()) {
        int vertex = queue.front();
        queue.pop();
        for (auto next : graph[vertex]) {
            if (!used[next]) {
                used[next] = true;
                dist[next] = dist[vertex] + 1;
                parent[next] = vertex;
                queue.emplace(next);
            }
        }
    }
}

// Multi-source BFS in O(V + E)
void MultiSourceBFS(const Graph& graph,
                    std::vector<int>& dist,
                    std::vector<int>& parent,
                    const std::vector<int>& srcs) {
    int size = graph.Size();
    std::vector<bool> used(size);
    std::queue<int> queue;

    for (auto src : srcs) {
        used[src] = true;
        dist[src] = 0;
        queue.emplace(src);
    }

    while (!queue.empty()) {
        int vertex = queue.front();
        queue.pop();
        for (auto next : graph[vertex]) {
            if (!used[next]) {
                used[next] = true;
                dist[next] = dist[vertex] + 1;
                parent[next] = vertex;
                queue.emplace(next);
            }
        }
    }
}

int main() {
    // Case 1: ordinary BFS in an undirected graph
    {
        int n, m, u, v, src;
        std::cin >> n >> m >> src;
        Graph graph(n);
        while (m--) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        std::vector<int> dist(n, kNone), parent(n, kNone);
        BFS(graph, dist, parent, src);
        for (int u = 0; u < n; ++u) {
            printf("%d: p = %d, d = %d\n", u, parent[u], dist[u]);
        }
        std::cout << '\n';
    }

    // Case 2: multi-source BFS in an undirected graph
    {
        int n, m, k, u, v;
        std::cin >> n >> m >> k;
        Graph graph(n);
        while (m--) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }
        std::vector<int> srcs(k);
        for (int i = 0; i < k; ++i) {
            std::cin >> srcs[i];
        }

        std::vector<int> dist(n, kNone), parent(n, kNone);
        MultiSourceBFS(graph, dist, parent, srcs);
        for (int u = 0; u < n; ++u) {
            printf("%d: p = %d, d = %d\n", u, parent[u], dist[u]);
        }
        std::cout << '\n';
    }

    return 0;
}
