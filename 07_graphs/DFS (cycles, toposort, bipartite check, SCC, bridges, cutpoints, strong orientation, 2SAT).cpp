#include <algorithm>
#include <iostream>
#include <vector>

constexpr int kNone = -1;

class Graph {
    std::vector<std::vector<int>> edges_;

public:
    explicit Graph(int size) : edges_(size) {
    }

    int Size() const {
        return edges_.size();
    }

    void AddDirectedEdge(int from, int to) {
        edges_[from].push_back(to);
    }

    void AddEdge(int from, int to) {
        AddDirectedEdge(from, to);
        if (from != to) {
            AddDirectedEdge(to, from);
        }
    }

    const auto& operator[](int vertex) const {
        return edges_[vertex];
    }

    Graph Transpose() const {
        int size = Size();
        Graph transposed(size);
        for (int from = 0; from < size; ++from) {
            for (auto to : edges_[from]) {
                transposed.AddDirectedEdge(to, from);
            }
        }
        return transposed;
    }
};

class IdGraph {
    struct EdgeTarget {
        int target, id;

        EdgeTarget(int target, int id) : target(target), id(id) {
        }
    };

    std::vector<std::vector<EdgeTarget>> edges_;
    int num_edges_ = 0;

public:
    explicit IdGraph(int size) : edges_(size) {
    }

    int Size() const {
        return edges_.size();
    }

    int NumEdges() const {
        return num_edges_;
    }

    void AddDirectedEdge(int from, int to, int id = kNone) {
        if (id == kNone) {
            id = num_edges_++;
        }
        edges_[from].emplace_back(to, id);
    }

    void AddEdge(int from, int to) {
        AddDirectedEdge(from, to);
        if (from != to) {
            AddDirectedEdge(to, from, num_edges_ - 1);
        }
    }

    const auto& operator[](int vertex) const {
        return edges_[vertex];
    }
};

enum class Color {
    White,
    Gray,
    Black,
};

// Ordinary DFS: O(V + E)
void DFS(const Graph& graph, std::vector<Color>& color, std::vector<int>& parent,
         std::vector<int>& tin, std::vector<int>& tout, int& time, int vertex) {
    color[vertex] = Color::Gray;
    tin[vertex] = ++time;
    for (auto next : graph[vertex]) {
        if (color[next] == Color::White) {
            parent[next] = vertex;
            DFS(graph, color, parent, tin, tout, time, next);
        }
    }
    tout[vertex] = ++time;
    color[vertex] = Color::Black;
}

// DFS: find cycle
bool CycleDFS(const Graph& graph, std::vector<Color>& color, std::vector<int>& parent,
              std::vector<int>& cycle, int vertex) {
    color[vertex] = Color::Gray;
    for (auto next : graph[vertex]) {
        if (color[next] == Color::Gray) {
            cycle.push_back(next);
            do {
                cycle.push_back(vertex);
                vertex = parent[vertex];
            } while (vertex != next);
            std::reverse(cycle.begin(), cycle.end());
            return true;
        } else if (color[next] == Color::White) {
            parent[next] = vertex;
            if (CycleDFS(graph, color, parent, cycle, next)) {
                return true;
            }
        }
    }
    color[vertex] = Color::Black;
    return false;
}

// DFS: find connected component
void CompDFS(const Graph& graph, std::vector<bool>& used, std::vector<int>& comp, int vertex) {
    used[vertex] = true;
    for (auto next : graph[vertex]) {
        if (!used[next]) {
            CompDFS(graph, used, comp, next);
        }
    }
    comp.push_back(vertex);
}

// DFS: find topological order
bool TopoSortDFS(const Graph& graph, std::vector<Color>& color, std::vector<int>& order, int vertex) {
    color[vertex] = Color::Gray;
    for (auto next : graph[vertex]) {
        if (color[next] == Color::Gray ||
                (color[next] == Color::White && !TopoSortDFS(graph, color, order, next))) {
            return false;
        }
    }
    order.push_back(vertex);
    color[vertex] = Color::Black;
    return true;
}

// DFS: find bicoloring
bool BipartiteDFS(const Graph& graph, std::vector<int>& color, int vertex, bool parity = true) {
    color[vertex] = parity + 1;
    for (auto next : graph[vertex]) {
        if (color[next] == parity + 1 ||
                (!color[next] && !BipartiteDFS(graph, color, next, !parity))) {
            return false;
        }
    }
    return true;
}

// DFS: find bridges
void BridgesDFS(const Graph& graph, std::vector<std::pair<int, int>>& bridges,
                std::vector<int>& tin, std::vector<int>& tup,
                int& time, int vertex, int parent = kNone) {
    tin[vertex] = tup[vertex] = ++time;
    for (auto next : graph[vertex]) {
        if (next == parent) {
            continue;
        } else if (tin[next]) {
            tup[vertex] = std::min(tup[vertex], tin[next]);
        } else {
            BridgesDFS(graph, bridges, tin, tup, time, next, vertex);
            tup[vertex] = std::min(tup[vertex], tup[next]);
            if (tup[next] > tin[vertex]) {
                bridges.emplace_back(vertex, next);
            }
        }
    }
}

// DFS: find cutpoints
void CutpointsDFS(const Graph& graph, std::vector<int>& cutpoints,
                  std::vector<int>& tin, std::vector<int>& tup,
                  int& time, int vertex, int parent = kNone) {
    tin[vertex] = tup[vertex] = ++time;
    bool is_cutpoint = false;
    int children = 0;
    for (auto next : graph[vertex]) {
        if (next == parent) {
            continue;
        } else if (tin[next]) {
            tup[vertex] = std::min(tup[vertex], tin[next]);
        } else {
            CutpointsDFS(graph, cutpoints, tin, tup, time, next, vertex);
            tup[vertex] = std::min(tup[vertex], tup[next]);
            ++children;
            if (parent != kNone && tup[next] >= tin[vertex]) {
                is_cutpoint = true;
            }
        }
    }
    if (parent == kNone && children > 1) {
        is_cutpoint = true;
    }
    if (is_cutpoint) {
        cutpoints.push_back(vertex);
    }
}

// DFS: build a graph strong orientation
void StrongOrientDFS(const IdGraph& graph, Graph& oriented, std::vector<bool>& used,
                     std::vector<bool>& edge_used, int vertex) {
    used[vertex] = true;
    for (auto [next, id] : graph[vertex]) {
        if (!edge_used[id]) {
            edge_used[id] = true;
            oriented.AddDirectedEdge(vertex, next);
            if (!used[next]) {
                StrongOrientDFS(graph, oriented, used, edge_used, next);
            }
        }
    }
}

// Find any cycle: O(V + E)
auto FindCycle(const Graph& graph) {
    int size = graph.Size();
    std::vector<Color> color(size);
    std::vector<int> parent(size, kNone), cycle;
    for (int vertex = 0; vertex < size; ++vertex) {
        if (color[vertex] == Color::White && CycleDFS(graph, color, parent, cycle, vertex)) {
            break;
        }
    }
    return cycle;
}

// Find any topological order: O(V + E)
auto TopoSort(const Graph& graph) {
    int size = graph.Size();
    std::vector<Color> color(size);
    std::vector<int> order;
    for (int vertex = 0; vertex < size; ++vertex) {
        if (color[vertex] == Color::White && !TopoSortDFS(graph, color, order, vertex)) {
            order.clear();
            break;
        }
    }
    std::reverse(order.begin(), order.end());
    return order;
}

// Find any correct bicoloring: O(V + E)
auto IsBipartite(const Graph& graph) {
    int size = graph.Size();
    std::vector<int> color(size), left, right;
    for (int vertex = 0; vertex < size; ++vertex) {
        if (!BipartiteDFS(graph, color, vertex)) {
            left.clear();
            right.clear();
            break;
        }
        if (color[vertex] == 1) {
            left.push_back(vertex);
        } else {
            right.push_back(vertex);
        }
    }
    return std::make_pair(left, right);
}

// Find SCCs: O(V + E)
auto Kosaraju(const Graph& graph) {
    int size = graph.Size();
    std::vector<bool> used(size);
    std::vector<int> order;
    for (int vertex = 0; vertex < size; ++vertex) {
        if (!used[vertex]) {
            CompDFS(graph, used, order, vertex);
        }
    }

    std::reverse(order.begin(), order.end());
    Graph transposed = graph.Transpose();
    used.assign(size, false);
    std::vector<std::vector<int>> comps;
    for (auto vertex : order) {
        if (!used[vertex]) {
            comps.emplace_back();
            CompDFS(transposed, used, comps.back(), vertex);
        }
    }
    return comps;
}

// Find all bridges: O(V + E)
auto FindBridges(const Graph& graph) {
    int size = graph.Size(), time = 0;
    std::vector<int> tin(size), tup(size);
    std::vector<std::pair<int, int>> bridges;
    for (int vertex = 0; vertex < size; ++vertex) {
        if (!tin[vertex]) {
            BridgesDFS(graph, bridges, tin, tup, time, vertex);
        }
    }
    return bridges;
}

// Find all cutpoints: O(V + E)
auto FindCutpoints(const Graph& graph) {
    int size = graph.Size(), time = 0;
    std::vector<int> tin(size), tup(size), cutpoints;
    for (int vertex = 0; vertex < size; ++vertex) {
        if (!tin[vertex]) {
            CutpointsDFS(graph, cutpoints, tin, tup, time, vertex);
        }
    }
    return cutpoints;
}

// Orient edges to minimize the number of SCCs: O(V + E)
// PS: #SCCs = #bridges + #comps
auto StrongOrientation(const IdGraph& graph) {
    int size = graph.Size(), edges = graph.NumEdges();
    std::vector<bool> used(size), edge_used(edges);
    Graph oriented(size);
    for (int vertex = 0; vertex < size; ++vertex) {
        if (!used[vertex]) {
            StrongOrientDFS(graph, oriented, used, edge_used, vertex);
        }
    }
    return oriented;
}

int main() {

    // Case 1: ordinary DFS on an undirected graph
    {
        int n, m, u, v;
        std::cin >> n >> m;
        Graph graph(n);
        while (m--) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        std::vector<Color> color(n);
        std::vector<int> parent(n, kNone), tin(n), tout(n);
        int time = 0;
        for (int u = 0; u < n; ++u) {
            if (color[u] == Color::White) {
                DFS(graph, color, parent, tin, tout, time, u);
            }
            printf("%d: parent = %d | [%d; %d]\n", u, parent[u], tin[u], tout[u]);
        }
        std::cout << '\n';
    }

    // Case 2: find cycle in a directed graph
    {
        int n, m, u, v;
        std::cin >> n >> m;
        Graph graph(n);
        while (m--) {
            std::cin >> u >> v;
            graph.AddDirectedEdge(u, v);
        }

        auto cycle = FindCycle(graph);
        for (auto u : cycle) {
            std::cout << u << ' ';
        }
        std::cout << "\n\n";
    }

    // Case 3: find connected components in an undirected graph
    {
        int n, m, u, v;
        std::cin >> n >> m;
        Graph graph(n);
        while (m--) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        std::vector<std::vector<int>> comps;
        std::vector<bool> used(n);
        for (int u = 0; u < n; ++u) {
            if (!used[u]) {
                comps.emplace_back();
                CompDFS(graph, used, comps.back(), u);
            }
        }

        for (const auto& comp : comps) {
            for (auto u : comp) {
                std::cout << u << ' ';
            }
            std::cout << '\n';
        }
        std::cout << '\n';
    }

    // Case 4: topological sort
    {
        int n, m, u, v;
        std::cin >> n >> m;
        Graph graph(n);
        while (m--) {
            std::cin >> u >> v;
            graph.AddDirectedEdge(u, v);
        }

        auto order = TopoSort(graph);

        if (order.empty()) {
            std::cout << "IMPOSSIBLE\n\n";
        } else {
            for (auto u : order) {
                std::cout << u << ' ';
            }
            std::cout << "\n\n";
        }
    }

    // Case 5: check if graph is bipartite
    {
        int n, m, u, v;
        std::cin >> n >> m;
        Graph graph(n);
        while (m--) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        auto [left, right] = IsBipartite(graph);

        if (left.empty() && right.empty()) {
            std::cout << "IMPOSSIBLE\n\n";
        } else {
            for (auto u : left) {
                std::cout << u << ' ';
            }
            std::cout << '\n';
            for (auto u : right) {
                std::cout << u << ' ';
            }
            std::cout << "\n\n";
        }
    }

    // Case 6: find strongly connected components (SCC) in a directed graph
    {
        int n, m, u, v;
        std::cin >> n >> m;
        Graph graph(n);
        while (m--) {
            std::cin >> u >> v;
            graph.AddDirectedEdge(u, v);
        }

        auto scc = Kosaraju(graph);

        for (const auto& comp : scc) {
            for (auto u : comp) {
                std::cout << u << ' ';
            }
            std::cout << '\n';
        }
        std::cout << '\n';
    }

    // Case 7: find bridges in an undirected graph
    {
        int n, m, u, v;
        std::cin >> n >> m;
        Graph graph(n);
        while (m--) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        auto bridges = FindBridges(graph);

        for (auto [u, v] : bridges) {
            printf("%d - %d\n", u, v);
        }
        std::cout << '\n';
    }

    // Case 8: find cutpoints in an undirected graph
    {
        int n, m, u, v;
        std::cin >> n >> m;
        Graph graph(n);
        while (m--) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        auto cutpoints = FindCutpoints(graph);

        for (auto u : cutpoints) {
            std::cout << u << ' ';
        }
        std::cout << "\n\n";
    }

    // Case 9: solve 2SAT problem for m conjuncts of boolean variables x1, ..., xn
    {
        int n, m, u, v;
        char su, sv;  // '+' or '-'
        std::cin >> n >> m;
        Graph graph(2 * n);
        while (--m) {
            std::cin >> su >> u >> sv >> v;
            --u, --v;
            if (u == v && su != sv) {
                continue;
            }

            u += n * (su == '-');
            v += n * (sv == '-');
            int nu = (u >= n ? u - n : u + n);
            int nv = (v >= n ? v - n : v + n);
            graph.AddDirectedEdge(nu, v);
            graph.AddDirectedEdge(nv, u);
        }

        auto comps = Kosaraju(graph);
        std::vector<int> num_comp(2 * n);
        for (int i = 0; i < static_cast<int>(comps.size()); ++i) {
            const auto& comp = comps[i];
            for (auto u : comp) {
                num_comp[u] = i;
            }
        }
        std::vector<bool> value(n);
        bool can = true;
        for (int u = 0; u < n; ++u) {
            if (num_comp[u] == num_comp[u + n]) {
                can = false;
                break;
            } else {
                value[u] = (num_comp[u] > num_comp[u + n]);
            }
        }

        if (!can) {
            std::cout << "IMPOSSIBLE\n";
        } else {
            for (int u = 0; u < n; ++u) {
                std::cout << int(value[u]);
            }
        }
        std::cout << "\n\n";
    }

    // Case 10: strong orientation
    {
        int n, m, u, v;
        std::cin >> n >> m;
        IdGraph graph(n);
        while (m--) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        auto oriented = StrongOrientation(graph);
        for (int u = 0; u < n; ++u) {
            for (auto v : oriented[u]) {
                std::cout << u << " -> " << v << '\n';
            }
        }
    }

    return 0;
}
