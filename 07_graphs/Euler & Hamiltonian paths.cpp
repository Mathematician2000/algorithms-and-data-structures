#include <algorithm>
#include <iostream>
#include <iterator>
#include <stack>
#include <unordered_set>
#include <utility>
#include <vector>

using AdjMatrix = std::vector<std::vector<int>>;

constexpr int kInf = 1e9;
constexpr int kNone = -1;

class DirectedErasableGraph {
    std::vector<std::unordered_multiset<int>> edges_;
    std::vector<int> in_deg_, out_deg_;

public:
    explicit DirectedErasableGraph(int size) : edges_(size), in_deg_(size), out_deg_(size) {
    }

    int Size() const {
        return edges_.size();
    }

    int InDegree(int vertex) const {
        return in_deg_[vertex];
    }

    int OutDegree(int vertex) const {
        return out_deg_[vertex];
    }

    int Degree(int vertex) const {
        return InDegree(vertex) + OutDegree(vertex);
    }

    int GetBalance(int vertex) const {
        return InDegree(vertex) - OutDegree(vertex);
    }

    void AddDirectedEdge(int from, int to) {
        edges_[from].insert(to);
        ++in_deg_[to];
        ++out_deg_[from];
    }

    void RemoveDirectedEdge(int from, int to) {
        auto& set = edges_[from];
        set.erase(set.find(to));
        --in_deg_[to];
        --out_deg_[from];
    }

    const auto& operator[](int vertex) const {
        return edges_[vertex];
    }
};

void DFS(const DirectedErasableGraph& graph, std::vector<bool>& used, int vertex) {
    used[vertex] = true;
    for (auto next : graph[vertex]) {
        if (!used[next]) {
            DFS(graph, used, next);
        }
    }
}

bool IsEulerGraph(const DirectedErasableGraph& graph) {
    int size = graph.Size();
    int in_odd = 0, out_odd = 0;
    int out_start = kNone;
    for (int u = 0; u < size; ++u) {
        int balance = graph.GetBalance(u);
        if (balance == 1) {
            ++in_odd;
        } else if (balance == -1) {
            ++out_odd;
            out_start = u;
        } else if (balance) {
            return false;
        }
    }
    if ((in_odd || out_odd) && (in_odd != 1 || out_odd != 1)) {
        return false;
    }

    std::vector<bool> used(size);
    DFS(graph, used, out_start == kNone ? 0 : out_start);
    for (int u = 0; u < size; ++u) {
        if (graph.Degree(u) > 0 && !used[u]) {
            return false;
        }
    }
    return true;
}

// Find Euler path (version for directed graphs): O(E)
auto FindEulerPath(DirectedErasableGraph& graph) {
    std::vector<int> path;
    if (!IsEulerGraph(graph)) {
        return path;
    }

    int size = graph.Size(), src = 0;
    for (int u = 0; u < size; ++u) {
        if (graph.GetBalance(u) == -1) {
            src = u;
            break;
        }
    }

    std::stack<int> st;
    st.push(src);
    while (!st.empty()) {
        int u = st.top();
        if (graph.OutDegree(u) > 0) {
            int v = *graph[u].begin();
            st.push(v);
            graph.RemoveDirectedEdge(u, v);
        } else {
            st.pop();
            path.push_back(u);
        }
    }

    std::reverse(path.begin(), path.end());
    return path;
}

class ErasableGraph {
    std::vector<std::unordered_multiset<int>> edges_;

public:
    explicit ErasableGraph(int size) : edges_(size) {
    }

    int Size() const {
        return edges_.size();
    }

    void AddEdge(int from, int to) {
        edges_[from].insert(to);
        edges_[to].insert(from);
    }

    void RemoveEdge(int from, int to) {
        auto& from_set = edges_[from];
        from_set.erase(from_set.find(to));
        auto& to_set = edges_[to];
        to_set.erase(to_set.find(from));
    }

    const auto& operator[](int vertex) const {
        return edges_[vertex];
    }
};

// Find Euler circuit: O(E) [O(E log E) if using std::set to erase edges]
auto FindEulerCircuit(ErasableGraph graph) {
    int size = graph.Size(), src = 0;
    std::vector<int> circuit;
    for (int u = 0; u < size; ++u) {
        if (graph[u].size() & 1) {
            return circuit;
        } else if (!src && !graph[u].empty()) {
            src = u;
        }
    }

    if (src) {
        std::stack<int> st;
        st.push(src);
        while (!st.empty()) {
            int u = st.top();
            if (graph[u].empty()) {
                circuit.push_back(u);
                st.pop();
            } else {
                int v = *graph[u].begin();
                graph.RemoveEdge(u, v);
                st.push(v);
            }
        }
    }

    return circuit;
}

// Find Euler path: O(E) [O(E log V) if using std::set to erase edges]
auto FindEulerPath(ErasableGraph graph) {
    int size = graph.Size(), src = 0, dst = 0;
    std::vector<int> path;
    for (int u = 0; u < size; ++u) {
        if (graph[u].size() & 1) {
            if (!src) {
                src = u;
            } else if (!dst) {
                dst = u;
            } else {
                return path;
            }
        }
    }

    if (!src) {
        src = dst = 1;
    } else if (!dst) {
        return path;
    } else {
        graph.AddEdge(src, dst);
    }

    auto circuit = FindEulerCircuit(graph);
    int idx;
    for (idx = 0; idx + 1 < static_cast<int>(circuit.size()); ++idx) {
        int u = circuit[idx], v = circuit[idx + 1];
        if ((u == src && v == dst) || (u == dst && v == src)) {
            break;
        }
    }
    std::copy(circuit.begin() + idx + 1, circuit.end(), std::back_inserter(path));
    std::copy(circuit.begin() + 1, circuit.begin() + idx + 1, std::back_inserter(path));
    return path;
}

// Find min-cost Hamiltonian path using brute-force: O(V * V!)
auto SalesmanBruteForce(const AdjMatrix& graph) {
    int size = graph.size(), cost = kInf;
    std::vector<int> path(size), opt(size);
    for (int u = 0; u < size; ++u) {
        path[u] = u;
    }

    do {
        int new_cost = 0;
        for (int i = 1; i < size; ++i) {
            new_cost += graph[path[i - 1]][path[i]];
        }
        if (new_cost < cost) {
            cost = new_cost, opt = path;
        }
    } while (std::next_permutation(path.begin(), path.end()));

    return std::make_pair(opt, cost);
}

// Find min-cost Hamiltonian path using DP: O(V^2 2^V)
auto SalesmanDP(const AdjMatrix& graph) {
    int size = graph.size();
    std::vector<std::vector<int>> dp(1 << size, std::vector<int>(size, kInf));
    for (int u = 0; u < size; ++u) {
        dp[1 << u][u] = 0;
    }

    for (int mask = 1; mask < (1 << size); ++mask) {
        for (int u = 0; u < size; ++u) {
            if (mask & (1 << u)) {
                int sub = mask ^ (1 << u);
                for (int v = 0; v < size; ++v) {
                    if (sub & (1 << v)) {
                        dp[mask][u] = std::min(dp[mask][u], dp[sub][v] + graph[v][u]);
                    }
                }
            }
        }
    }

    int cost = 0, mask = (1 << size) - 1;
    int u = std::min_element(dp[mask].begin(), dp[mask].end()) - dp[mask].begin();
    std::vector<int> opt = {u};
    while (static_cast<int>(opt.size()) != size) {
        int sub = mask ^ (1 << u);
        for (int v = 0; v < size; ++v) {
            if (dp[sub][v] + graph[v][u] == dp[mask][u]) {
                cost += graph[v][u];
                opt.push_back(v);
                mask = sub;
                u = v;
                break;
            }
        }
    }

    return std::make_pair(opt, cost);  // cost == dp.back()[u]
}

int main() {
    // Case 1: find Euler circuit in O(m)
    {
        int n, m, u, v;
        std::cin >> n >> m;
        ErasableGraph graph(n);
        for (int i = 0; i < m; ++i) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        auto circuit = FindEulerCircuit(graph);

        if (circuit.empty() && m) {
            std::cout << "IMPOSSIBLE\n\n";
        } else {
            for (auto u : circuit) {
                std::cout << u << ' ';
            }
            std::cout << "\n\n";
        }
    }

    // Case 2: find Euler path in O(m)
    {
        int n, m, u, v;
        std::cin >> n >> m;
        ErasableGraph graph(n);
        for (int i = 0; i < m; ++i) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        auto path = FindEulerPath(graph);

        if (path.empty() && m) {
            std::cout << "IMPOSSIBLE\n\n";
        } else {
            for (auto u : path) {
                std::cout << u << ' ';
            }
            std::cout << "\n\n";
        }
    }

    // Case 3: find min-cost Hamiltonian path in O(n * n!)
    {
        int n, m, u, v, w;
        std::cin >> n >> m;
        AdjMatrix graph(n, std::vector<int>(n, kInf));
        while (m--) {
            std::cin >> u >> v >> w;
            graph[u][v] = graph[v][u] = w;
        }

        auto [path, cost] = SalesmanBruteForce(graph);

        for (auto u : path) {
            std::cout << u << ' ';
        }
        printf("\nCost = %d\n\n", cost);
    }

    // Case 4: find min-cost Hamiltonian path in O(n^2 2^n)
    {
        int n, m, u, v, w;
        std::cin >> n >> m;
        AdjMatrix graph(n, std::vector<int>(n, kInf));
        while (m--) {
            std::cin >> u >> v >> w;
            graph[u][v] = graph[v][u] = w;
        }

        auto [path, cost] = SalesmanDP(graph);

        for (auto u : path) {
            std::cout << u << ' ';
        }
        printf("\nCost = %d\n\n", cost);
    }

    return 0;
}
