#include <algorithm>
#include <iostream>
#include <queue>
#include <utility>
#include <vector>

constexpr int kInf = 1e9;
constexpr int kNone = -1;

struct EdgeTarget {
    int target, id;

    EdgeTarget(int target, int id) : target(target), id(id) {
    }
};

struct Edge {
    int flow = 0, capacity;
    int reverse_id;

    Edge(int capacity, int reverse_id)
        : capacity(capacity), reverse_id(reverse_id) {
    }
};

class FlowNetwork {
    std::vector<std::vector<EdgeTarget>> graph_;
    std::vector<Edge> edges_;
    int source_, sink_;

public:
    FlowNetwork(int size, int source, int sink)
        : graph_(size), source_(source), sink_(sink) {
    }

    void AddEdge(int source, int target, int capacity = kInf) {
        int id = edges_.size();
        graph_[source].emplace_back(target, id);
        edges_.emplace_back(capacity, id + 1);
        graph_[target].emplace_back(source, id + 1);
        edges_.emplace_back(0, id);
    }

    size_t Size() const {
        return graph_.size();
    }

    int GetSource() const {
        return source_;
    }

    int GetSink() const {
        return sink_;
    }

    int GetFlow(int id) const {
        return edges_[id].flow;
    }

    int GetResidualCapacity(int id) const {
        auto [flow, capacity, reverse_id] = edges_[id];
        return capacity - flow;
    }

    void UpdateFlow(int id, int pushed) {
        auto& [flow, capacity, reverse_id] = edges_[id];
        flow += pushed;
        edges_[reverse_id].flow -= pushed;
    }

    const auto& operator[](int vertex) const {
        return graph_[vertex];
    }
};

// DFS: Ford-Fulkerson algorithm
int DFS(FlowNetwork& network, std::vector<bool>& used, int vertex,
        int bound = 0, int old_flow = kInf) {
    used[vertex] = true;
    if (vertex == network.GetSink()) {
        return old_flow;
    }

    for (auto [next, id] : network[vertex]) {
        if (!used[next]) {
            int residual = network.GetResidualCapacity(id);
            if (residual > bound) {
                int pushed = DFS(network, used, next, bound, std::min(old_flow, residual));
                if (pushed) {
                    network.UpdateFlow(id, pushed);
                    return pushed;
                }
            }
        }
    }

    return 0;
}

// DFS: Dinic algorithm
int DFS(FlowNetwork& network, const std::vector<int>& dist, std::vector<int>& pos,
        int vertex, int old_flow = kInf) {
    if (vertex == network.GetSink()) {
        return old_flow;
    }

    for (int& idx = pos[vertex]; idx < static_cast<int>(network[vertex].size()); ++idx) {
        auto [next, id] = network[vertex][idx];
        if (dist[next] != dist[vertex] + 1) {
            continue;
        }
        int residual = network.GetResidualCapacity(id);  // check for >0 is redundant
        int pushed = DFS(network, dist, pos, next, std::min(old_flow, residual));
        if (pushed) {
            network.UpdateFlow(id, pushed);
            return pushed;
        }
    }

    return 0;
}

// BFS: Edmonds-Karp algorithm
void BFS(const FlowNetwork& network, std::vector<int>& dist,
         std::vector<int>& parent, std::vector<int>& parent_id) {
    int source = network.GetSource();
    std::queue<int> queue;
    queue.push(source);
    dist[source] = 0;

    while (!queue.empty()) {
        int vertex = queue.front();
        queue.pop();
        for (auto [next, id] : network[vertex]) {
            if (dist[next] != kInf) {
                continue;
            }
            int residual = network.GetResidualCapacity(id);
            if (residual > 0) {
                queue.push(next);
                dist[next] = dist[vertex] + 1;
                parent[next] = vertex;
                parent_id[next] = id;
            }
        }
    }
}

// BFS: Dinic algorithm
void BFS(const FlowNetwork& network, std::vector<int>& dist) {
    int source = network.GetSource();
    std::queue<int> queue;
    queue.push(source);
    dist[source] = 0;

    while (!queue.empty()) {
        int vertex = queue.front();
        queue.pop();
        for (auto [next, id] : network[vertex]) {
            if (dist[next] != kInf) {
                continue;
            }
            int residual = network.GetResidualCapacity(id);
            if (residual > 0) {
                queue.push(next);
                dist[next] = dist[vertex] + 1;
            }
        }
    }
}

// Ford-Fulkerson algorithm for max-flow problem: O(EF)
// Warning: may diverge in some cases
int FordFulkerson(FlowNetwork& network) {
    int size = network.Size(), source = network.GetSource();
    int flow = 0, pushed;
    std::vector<bool> used;
    do {
        used.assign(size, false);
        pushed = DFS(network, used, source);
        flow += pushed;
    } while (pushed);
    return flow;
}

// Scaling algorithm for max-flow problem: O(E^2 log C), C = bound
int ScalingFlow(FlowNetwork& network, int bound = kInf) {
    int size = network.Size(), source = network.GetSource();
    int flow = 0, pushed;
    std::vector<bool> used;
    while (bound) {
        do {
            used.assign(size, false);
            pushed = DFS(network, used, source, bound);
            flow += pushed;
        } while (pushed);
        bound /= 2;
    }
    return flow;
}

// Edmonds-Karp algorithm for max-flow problem: O(V E^2)
int EdmondsKarp(FlowNetwork& network) {
    int size = network.Size(), source = network.GetSource(), sink = network.GetSink();
    int flow = 0;
    std::vector<int> dist, parent, parent_id(size);
    while (true) {
        dist.assign(size, kInf);
        parent.assign(size, kNone);
        BFS(network, dist, parent, parent_id);
        if (parent[sink] == kNone) {
            break;
        }

        int current = sink, pushed = kInf;
        while (current != source) {
            pushed = std::min(pushed, network.GetResidualCapacity(parent_id[current]));
            current = parent[current];
        }
        flow += pushed;
        current = sink;
        while (current != source) {
            network.UpdateFlow(parent_id[current], pushed);
            current = parent[current];
        }
    }
    return flow;
}

// Dinic algorithm for max-flow problem: O(V^2 E)
int Dinic(FlowNetwork& network) {
    int size = network.Size(), source = network.GetSource(), sink = network.GetSink();
    int flow = 0, pushed;
    std::vector<int> dist, pos;
    while (true) {
        dist.assign(size, kInf);
        BFS(network, dist);  // try enabling scaling => O(VE log U), U - max edge capacity
        if (dist[sink] == kInf) {
            break;
        }

        pos.assign(size, 0);
        while ((pushed = DFS(network, dist, pos, source))) {
            flow += pushed;
        }
    }
    return flow;
}

// Push-relabel with current-arc FIFO data structure: O(V^2 E) = O(V^4)
class PushRelabel {
    std::vector<int> height_, excess_, pos_;
    std::queue<int> excess_nodes_;
    FlowNetwork& network_;

    void Push(int from, int to, int id) {
        int residual = network_.GetResidualCapacity(id);
        int delta = std::min(excess_[from], residual);
        if (delta) {
            network_.UpdateFlow(id, delta);
            excess_[from] -= delta;
            if (!excess_[to]) {
                excess_nodes_.push(to);
            }
            excess_[to] += delta;
        }
    }

    void Relabel(int vertex) {
        int delta = kInf;
        for (auto [next, id] : network_[vertex]) {
            int residual = network_.GetResidualCapacity(id);
            if (residual > 0) {
                delta = std::min(delta, height_[next]);
            }
        }
        if (delta < kInf) {
            height_[vertex] = delta + 1;
        }
    }

    void Discharge(int vertex) {
        int sz = network_[vertex].size();
        int& idx = pos_[vertex];
        while (excess_[vertex] > 0) {
            if (idx == sz) {
                Relabel(vertex);
                idx = 0;
            }

            auto [next, id] = network_[vertex][idx];
            int residual = network_.GetResidualCapacity(id);
            if (residual > 0 && height_[vertex] > height_[next]) {
                Push(vertex, next, id);
            } else {
                ++idx;
            }
        }
    }

public:
    explicit PushRelabel(FlowNetwork& network) : network_(network) {
        int size = network_.Size();
        height_.assign(size, 0);
        excess_.assign(size, 0);
        pos_.assign(size, 0);

        int source = network_.GetSource();
        height_[source] = size;
        excess_[source] = kInf;
        for (auto [next, id] : network_[source]) {
            Push(source, next, id);
        }
    }

    int MaxFlow() {
        int source = network_.GetSource(), sink = network_.GetSink();
        while (!excess_nodes_.empty()) {
            int vertex = excess_nodes_.front();
            excess_nodes_.pop();
            if (vertex != source && vertex != sink) {
                Discharge(vertex);
            }
        }

        int flow = 0;
        for (auto [next, id] : network_[source]) {
            flow += network_.GetFlow(id);
        }
        return flow;
    }
};

// Push-relabel algorithm with highest label selection rule: O(VE + V^2 sqrt(E)) = O(V^3)
class OptimizedPushRelabel {
    std::vector<int> height_, excess_;
    FlowNetwork& network_;

    void Push(int from, int to, int id) {
        int residual = network_.GetResidualCapacity(id);
        int delta = std::min(excess_[from], residual);
        network_.UpdateFlow(id, delta);
        excess_[from] -= delta;
        excess_[to] += delta;
    }

    void Relabel(int vertex) {
        int delta = kInf;
        for (auto [next, id] : network_[vertex]) {
            int residual = network_.GetResidualCapacity(id);
            if (residual > 0) {
                delta = std::min(delta, height_[next]);
            }
        }
        if (delta < kInf) {
            height_[vertex] = delta + 1;
        }
    }

    auto GetMaxHeightNodes() const {
        std::vector<int> maxs;
        int size = network_.Size(), source = network_.GetSource(), sink = network_.GetSink();
        for (int vertex = 0; vertex < size; ++vertex) {
            if (vertex != source && vertex != sink && excess_[vertex] > 0) {
                if (!maxs.empty() && height_[vertex] > height_[maxs.front()]) {
                    maxs.clear();
                }
                if (maxs.empty() || height_[vertex] == height_[maxs.front()]) {
                    maxs.push_back(vertex);
                }
            }
        }
        return maxs;
    }

public:
    explicit OptimizedPushRelabel(FlowNetwork& network) : network_(network) {
        int size = network_.Size();
        height_.assign(size, 0);
        excess_.assign(size, 0);

        int source = network_.GetSource();
        height_[source] = size;
        excess_[source] = kInf;
        for (auto [next, id] : network_[source]) {
            Push(source, next, id);
        }
    }

    int MaxFlow() {
        std::vector<int> layer = GetMaxHeightNodes();
        while (!layer.empty()) {
            for (auto vertex : layer) {
                bool pushed = false;
                if (excess_[vertex]) {
                    for (auto [next, id] : network_[vertex]) {
                        int residual = network_.GetResidualCapacity(id);
                        if (residual > 0 && height_[vertex] == height_[next] + 1) {
                            Push(vertex, next, id);
                            pushed = true;
                            if (!excess_[vertex]) {
                                break;
                            }
                        }
                    }
                }

                if (!pushed) {
                    Relabel(vertex);
                    break;
                }
            }

            layer = GetMaxHeightNodes();
        }

        int source = network_.GetSource();
        int flow = 0;
        for (auto [next, id] : network_[source]) {
            flow += network_.GetFlow(id);
        }
        return flow;
    }
};

struct CostEdge {
    int flow = 0, capacity, cost;
    int reverse_id;

    CostEdge(int capacity, int cost, int reverse_id)
      : capacity(capacity), cost(cost), reverse_id(reverse_id) {}
};

class CostFlowNetwork {
    std::vector<std::vector<EdgeTarget>> graph_;
    std::vector<CostEdge> edges_;
    int source_, sink_;

public:
    CostFlowNetwork(int size, int source, int sink)
        : graph_(size), source_(source), sink_(sink) {}

    void AddEdge(int source, int target, int capacity, int cost) {
        int id = edges_.size();
        graph_[source].emplace_back(target, id);
        edges_.emplace_back(capacity, cost, id + 1);
        graph_[target].emplace_back(source, id + 1);
        edges_.emplace_back(0, -cost, id);
    }

    size_t Size() const {
        return graph_.size();
    }

    int GetSource() const {
        return source_;
    }

    int GetSink() const {
        return sink_;
    }

    int GetResidualCapacity(int id) const {
        auto [flow, capacity, cost, reverse_id] = edges_[id];
        return capacity - flow;
    }

    int GetCost(int id) const {
        return edges_[id].cost;
    }

    void UpdateFlow(int id, int pushed) {
        auto& [flow, capacity, cost, reverse_id] = edges_[id];
        flow += pushed;
        edges_[reverse_id].flow -= pushed;
    }

    const auto& operator[](int vertex) const {
        return graph_[vertex];
    }
};

// SPFA algorithm: O(VE)
void SPFA(CostFlowNetwork& network, std::vector<int>& parent, std::vector<int>& parent_id) {
    int size = network.Size(), source = network.GetSource();
    std::vector<int> dist(size, kInf);
    std::vector<bool> used(size);
    std::queue<int> queue;

    dist[source] = 0;
    queue.push(source);
    used[source] = true;
    while (!queue.empty()) {
        int vertex = queue.front();
        queue.pop();
        used[vertex] = false;
        for (auto [next, id] : network[vertex]) {
            int redisual = network.GetResidualCapacity(id);
            int cost = network.GetCost(id);
            if (redisual > 0 && dist[vertex] + cost < dist[next]) {
                dist[next] = dist[vertex] + cost;
                parent[next] = vertex;
                parent_id[next] = id;
                if (!used[next]) {
                    queue.push(next);
                    used[next] = true;
                }
            }
        }
    }
}

// Find min-cost flow of required size (or min-cost-max-flow for required_flow = kInf): O(V^2 E^2)
auto MinCostMaxFlow(CostFlowNetwork& network, int required_flow = kInf) {
    int size = network.Size(), source = network.GetSource(), sink = network.GetSink();
    std::vector<int> parent, parent_id(size);
    int flow = 0, cost = 0;
    while (flow < required_flow) {
        parent.assign(size, kNone);
        SPFA(network, parent, parent_id);
        if (parent[sink] == kNone) {
            break;
        }

        int current = sink, pushed = required_flow - flow;
        while (current != source) {
            pushed = std::min(pushed, network.GetResidualCapacity(parent_id[current]));
            current = parent[current];
        }
        flow += pushed;
        current = sink;
        while (current != source) {
            int id = parent_id[current];
            network.UpdateFlow(id, pushed);
            cost += network.GetCost(id) * pushed;
            current = parent[current];
        }
    }
    return std::make_pair(flow, cost);
}

int main() {
    // Case 1: find max-flow using Ford-Fulkerson algorithm in O(EF)
    {
        int vertices, edges, source, sink, from, to, capacity;
        std::cin >> vertices >> edges >> source >> sink;
        FlowNetwork network(vertices, source, sink);
        while (edges--) {
            std::cin >> from >> to >> capacity;
            network.AddEdge(from, to, capacity);
        }

        printf("Flow = %d\n\n", FordFulkerson(network));
    }

    // Case 2: find max-flow using scaling algorithm in O(E^2 log C)
    {
        int vertices, edges, source, sink, from, to, capacity;
        std::cin >> vertices >> edges >> source >> sink;
        FlowNetwork network(vertices, source, sink);
        while (edges--) {
            std::cin >> from >> to >> capacity;
            network.AddEdge(from, to, capacity);
        }

        printf("Flow = %d\n\n", ScalingFlow(network));
    }

    // Case 3: find max-flow using Edmonds-Karp algorithm in O(V E^2)
    {
        int vertices, edges, source, sink, from, to, capacity;
        std::cin >> vertices >> edges >> source >> sink;
        FlowNetwork network(vertices, source, sink);
        while (edges--) {
            std::cin >> from >> to >> capacity;
            network.AddEdge(from, to, capacity);
        }

        printf("Flow = %d\n\n", EdmondsKarp(network));
    }

    // Case 4: find max-flow using Dinic algorithm in O(V^2 E)
    {
        int vertices, edges, source, sink, from, to, capacity;
        std::cin >> vertices >> edges >> source >> sink;
        FlowNetwork network(vertices, source, sink);
        while (edges--) {
            std::cin >> from >> to >> capacity;
            network.AddEdge(from, to, capacity);
        }

        printf("Flow = %d\n\n", Dinic(network));
    }

    // Case 5: find max-flow using generic push-relabel algorithm in O(V^2 E)
    {
        int vertices, edges, source, sink, from, to, capacity;
        std::cin >> vertices >> edges >> source >> sink;
        FlowNetwork network(vertices, source, sink);
        while (edges--) {
            std::cin >> from >> to >> capacity;
            network.AddEdge(from, to, capacity);
        }

        PushRelabel push_relabel(network);
        printf("Flow = %d\n\n", push_relabel.MaxFlow());
    }

    // Case 6: find max-flow using optimized push-relabel algorithm in O(VE + V^2 sqrt(E))
    {
        int vertices, edges, source, sink, from, to, capacity;
        std::cin >> vertices >> edges >> source >> sink;
        FlowNetwork network(vertices, source, sink);
        while (edges--) {
            std::cin >> from >> to >> capacity;
            network.AddEdge(from, to, capacity);
        }

        OptimizedPushRelabel push_relabel(network);
        printf("Flow = %d\n\n", push_relabel.MaxFlow());
    }

    // Case 7: find min-cut using Ford-Fulkerson algorithm
    {
        int vertices, edges, source, sink, from, to, capacity;
        std::cin >> vertices >> edges >> source >> sink;
        FlowNetwork network(vertices, source, sink);
        while (edges--) {
            std::cin >> from >> to >> capacity;
            network.AddEdge(from, to, capacity);
        }
        printf("Min-cut size: %d\n\n", FordFulkerson(network));

        std::vector<bool> used(vertices);
        DFS(network, used, source);
        for (from = 0; from < vertices; ++from) {
            for (auto [to, id] : network[from]) {
                if (used[from] && !used[to] && !network.GetResidualCapacity(id)) {
                    printf("%d - %d\n", from, to);
                }
            }
        }
        std::cout << '\n';
    }

    // Case 8: find min-cost-flow or min-cost-max-flow in O(V^2 E^2)
    {
        // set required_flow = kInf for min-cost-max-flow problem
        int vertices, edges, source, sink, required_flow, from, to, capacity, edge_cost;
        std::cin >> vertices >> edges >> source >> sink >> required_flow;
        CostFlowNetwork network(vertices, source, sink);
        while (edges--) {
            std::cin >> from >> to >> capacity >> edge_cost;
            network.AddEdge(from, to, capacity, edge_cost);
        }

        auto [flow, cost] = MinCostMaxFlow(network, required_flow);
        printf("Flow = %d, cost = %d\n\n", flow, cost);
    }

    return 0;
}
