#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>

constexpr int kNone = -1;

class Graph {
    std::vector<std::vector<int>> edges_;

public:
    explicit Graph(int size) : edges_(size) {
    }

    int Size() const {
        return edges_.size();
    }

    void AddDirectedEdge(int from, int to) {
        edges_[from].push_back(to);
    }

    const auto& operator[](int vertex) const {
        return edges_[vertex];
    }

    Graph Transpose() const {
        int size = Size();
        Graph transposed(size);
        for (int from = 0; from < size; ++from) {
            for (auto to : edges_[from]) {
                transposed.AddDirectedEdge(to, from);
            }
        }
        return transposed;
    }
};

// Ordinary DFS + preordering enumeration: O(V + E)
void DFS(const Graph& graph, std::vector<int>& parent, std::vector<int>& preorder, int vertex) {
    preorder.push_back(vertex);
    for (auto next : graph[vertex]) {
        if (parent[next] == kNone) {
            parent[next] = vertex;
            DFS(graph, parent, preorder, next);
        }
    }
}

// DSU with root path minima operation using path compression: O(log n) per query
class MinDSU {
    std::vector<int> parent_, path_min_;
    std::function<int(int)> weight_;

public:
    MinDSU(int size, const std::function<int(int)>& weight)
        : parent_(size, kNone), path_min_(size, kNone), weight_(weight) {
    }

    int GetMin(int vertex) {
        if (parent_[vertex] == kNone) {
            return vertex;
        }
        int& parent = parent_[vertex];
        int& pmin = path_min_[vertex];
        if (parent_[parent] != kNone) {
            int candidate = GetMin(parent);
            if (pmin == kNone || weight_(candidate) < weight_(pmin)) {
                pmin = candidate;
            }
            parent = parent_[parent];
        }
        return pmin;
    }

    void UpdateSet(int vertex, int parent, int pmin) {
        parent_[vertex] = parent;
        path_min_[vertex] = pmin;
    }
};

// Lengauer-Tarjan algorithm for finding immediate dominators: O(E log V)
auto LengauerTarjan(const Graph& graph, int source) {
    int size = graph.Size();
    std::vector<int> parent(size, kNone), preorder;
    parent[source] = source;
    preorder.reserve(size);
    DFS(graph, parent, preorder, source);

    std::vector<int> reverse_preorder = preorder, preorder_index(size);
    std::reverse(reverse_preorder.begin(), reverse_preorder.end());
    for (int idx = 0; idx < static_cast<int>(preorder.size()); ++idx) {
        preorder_index[preorder[idx]] = idx;
    }

    std::vector<int> sdom(size), rdom(size, kNone);
    for (int idx = 0; idx < size; ++idx) {
        sdom[idx] = idx;
    }

    Graph transposed = graph.Transpose();
    Graph inv_sdom(size);
    MinDSU dsu(size, [&](int vertex) { return preorder_index[sdom[vertex]]; });

    for (auto vertex : reverse_preorder) {
        for (auto next : inv_sdom[vertex]) {
            rdom[next] = dsu.GetMin(next);
        }
        if (vertex == source) {
            break;
        }

        for (auto ancestor : transposed[vertex]) {
            int candidate = dsu.GetMin(ancestor);
            if (preorder_index[sdom[candidate]] < preorder_index[sdom[vertex]]) {
                sdom[vertex] = sdom[candidate];
            }
        }
        dsu.UpdateSet(vertex, parent[vertex], vertex);

        if (parent[vertex] == sdom[vertex]) {
            rdom[vertex] = vertex;
        } else {
            inv_sdom.AddDirectedEdge(sdom[vertex], vertex);
        }
    }

    std::vector<int> idom(size, kNone);
    for (auto vertex : preorder) {
        if (vertex == source) {
            continue;
        }
        if (rdom[vertex] == vertex) {
            idom[vertex] = sdom[vertex];
        } else {
            idom[vertex] = idom[rdom[vertex]];
        }
    }
    return idom;
}

int main() {
    // Case 1: build dominator tree
    {
        int n, m, source, from, to;
        std::cin >> n >> m >> source;
        Graph graph(n);
        while (m--) {
            std::cin >> from >> to;
            graph.AddDirectedEdge(from, to);
        }

        auto idom = LengauerTarjan(graph, source);
        for (int u = 0; u < n; ++u) {
            std::cout << "idom[" << u << "] = " << idom[u] << '\n';
        }
        std::cout << '\n';
    }

    // Case 2: find all "necessary / critical" vertices lying on every path between two vertices
    {
        int n, m, from, to;
        std::cin >> n >> m;
        Graph graph(n);
        while (m--) {
            std::cin >> from >> to;
            graph.AddDirectedEdge(from, to);
        }
        std::cin >> from >> to;

        auto idom = LengauerTarjan(graph, from);

        std::vector<int> critical = {to};
        while (critical.back() != from) {
            critical.push_back(idom[critical.back()]);
        }
        std::reverse(critical.begin(), critical.end());

        std::cout << critical.size() << '\n';
        for (auto vertex : critical) {
            std::cout << vertex << ' ';
        }
        std::cout << '\n';
    }

    return 0;
}
