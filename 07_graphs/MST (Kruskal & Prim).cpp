#include <algorithm>
#include <iostream>
#include <queue>
#include <tuple>
#include <vector>

template<typename T>
using MinHeap = std::priority_queue<T, std::vector<T>, std::greater<T>>;

constexpr int kInf = 1e9;
constexpr int kNone = -1;

struct Edge {
    int target;
    int weight;

    Edge(int target, int weight) : target(target), weight(weight) {
    }
};

class Graph {
    std::vector<std::vector<Edge>> edges_;

public:
    explicit Graph(int size) : edges_(size) {
    }

    int Size() const {
        return edges_.size();
    }

    void AddEdge(int from, int to, int weight) {
        edges_[from].emplace_back(to, weight);
        if (from != to) {
            edges_[to].emplace_back(from, weight);
        }
    }

    const auto& operator[](int vertex) const {
        return edges_[vertex];
    }
};

// Disjoint Set Union structure
class DSU {
    std::vector<int> parent_, rank_;

public:
    explicit DSU(int size) : parent_(size), rank_(size, 1) {
        for (int vertex = 0; vertex < size; ++vertex) {
            parent_[vertex] = vertex;
        }
    }

    int FindSet(int vertex) {
        if (parent_[vertex] != vertex) {
            parent_[vertex] = FindSet(parent_[vertex]);
        }
        return parent_[vertex];
    }

    bool UniteSets(int left, int right) {
        left = FindSet(left);
        right = FindSet(right);
        if (left != right) {
            if (rank_[left] > rank_[right]) {
                std::swap(left, right);
            }
            parent_[left] = right;
            rank_[right] += rank_[left];
            return true;
        }
        return false;
    }
};

// Find MST using Kruskal's algorithm in O(E log E)
auto Kruskal(const Graph& graph) {
    int size = graph.Size();
    std::vector<std::tuple<int, int, int>> edges;
    std::vector<std::pair<int, int>> mst;
    for (int from = 0; from < size; ++from) {
        for (auto [to, weight] : graph[from]) {
            edges.emplace_back(weight, from, to);
        }
    }

    std::sort(edges.begin(), edges.end());
    DSU dsu(size);
    for (auto [weight, from, to] : edges) {
        if (dsu.UniteSets(from, to)) {
            mst.emplace_back(from, to);
        }
    }

    if (static_cast<int>(mst.size()) < size - 1) {
        mst.clear();
    }
    return mst;
}

// Find MST using Prim's algorithm in O(E log E)
auto Prim(const Graph& graph, int source) {
    int size = graph.Size();
    std::vector<std::pair<int, int>> mst;
    std::vector<int> dist(size, kInf), parent(size, kNone);
    std::vector<bool> used(size);
    MinHeap<std::pair<int, int>> queue;

    dist[source] = 0;
    queue.emplace(0, source);
    while (!queue.empty()) {
        auto [distance, vertex] = queue.top();
        queue.pop();
        if (!used[vertex]) {
            if (parent[vertex] != kNone) {
                mst.emplace_back(parent[vertex], vertex);
            }
            used[vertex] = true;
            for (auto [next, weight] : graph[vertex]) {
                if (!used[next] && weight < dist[next]) {
                    dist[next] = weight;
                    parent[next] = vertex;
                    queue.emplace(weight, next);
                }
            }
        }
    }

    if (static_cast<int>(mst.size()) < size - 1) {
        mst.clear();
    }
    return mst;
}

int main() {
    // Case 1: find MST using Kruskal's algorithm
    {
        int n, m, u, v, w;
        std::cin >> n >> m;
        Graph graph(n);
        while (m--) {
            std::cin >> u >> v >> w;
            graph.AddEdge(u, v, w);
        }

        auto mst = Kruskal(graph);

        if (mst.empty() && n != 1) {
            std::cout << "Graph has no MST!\n\n";
        } else {
            for (auto [u, v] : mst) {
                printf("%d - %d\n", u, v);
            }
            std::cout << '\n';
        }
    }

    // Case 2: find MST using Prim's algorithm
    {
        int n, m, u, v, w;
        std::cin >> n >> m;
        Graph graph(n);
        while (m--) {
            std::cin >> u >> v >> w;
            graph.AddEdge(u, v, w);
        }

        auto mst = Prim(graph, 0);

        if (mst.empty() && n != 1) {
            std::cout << "Graph has no MST!\n";
        } else {
            for (auto [u, v] : mst) {
                printf("%d - %d\n", u, v);
            }
        }
    }

    return 0;
}
