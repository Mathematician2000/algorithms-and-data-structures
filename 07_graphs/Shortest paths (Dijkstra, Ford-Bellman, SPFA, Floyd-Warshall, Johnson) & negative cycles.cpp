#include <algorithm>
#include <iostream>
#include <queue>
#include <utility>
#include <vector>

template<typename T>
using MinHeap = std::priority_queue<T, std::vector<T>, std::greater<T>>;

using AdjMatrix = std::vector<std::vector<int>>;
constexpr int kInf = 1e9;

struct Edge {
    int target;
    int weight;

    Edge(int target, int weight) : target(target), weight(weight) {
    }
};

class Graph {
    std::vector<std::vector<Edge>> edges_;

    void Resize(int new_size) {
        edges_.resize(new_size);
    }

    auto& operator[](int vertex) {
        return edges_[vertex];
    }

public:
    explicit Graph(int size) : edges_(size) {
    }

    int Size() const {
        return edges_.size();
    }

    void AddDirectedEdge(int from, int to, int weight) {
        edges_[from].emplace_back(to, weight);
    }

    void AddEdge(int from, int to, int weight) {
        AddDirectedEdge(from, to, weight);
        if (from != to) {
            AddDirectedEdge(to, from, weight);
        }
    }

    const auto& operator[](int vertex) const {
        return edges_[vertex];
    }

    friend auto Johnson(Graph);
};

// Dijkstra's algorithm: O(E log E)
auto Dijkstra(const Graph& graph, int source) {
    int size = graph.Size();
    std::vector<int> dist(size, kInf);
    std::vector<bool> used(size);
    MinHeap<std::pair<int, int>> queue;

    dist[source] = 0;
    queue.emplace(0, source);
    while (!queue.empty()) {  // make sure graph has no edges with negative weight!
        auto [distance, vertex] = queue.top();
        queue.pop();
        if (!used[vertex]) {
            used[vertex] = true;
            for (auto [next, weight] : graph[vertex]) {
                if (distance + weight < dist[next]) {
                    dist[next] = distance + weight;
                    queue.emplace(dist[next], next);
                }
            }
        }
    }

    return dist;
}

// Ford-Bellman algorithm: O(VE)
auto FordBellman(const Graph& graph, int source) {
    int size = graph.Size();
    std::vector<int> dist(size, kInf);

    dist[source] = 0;
    bool changed = true;  // instead of n - 1 iterations
    while (changed) {  // make sure graph has no negative cycles!
        changed = false;
        for (int vertex = 0; vertex < size; ++vertex) {
            if (dist[vertex] != kInf) {
                for (auto [next, weight] : graph[vertex]) {
                    if (dist[vertex] + weight < dist[next]) {
                        changed = true;
                        dist[next] = dist[vertex] + weight;
                    }
                }
            }
        }
    }

    return dist;
}

// SPFA (optimized Ford-Bellman algorithm): O(VE)
auto SPFA(const Graph& graph, int source) {
    int size = graph.Size();
    std::vector<int> dist(size, kInf);
    std::vector<bool> used(size);
    std::queue<int> queue;

    dist[source] = 0;
    used[source] = true;
    queue.push(source);
    while (!queue.empty()) {  // make sure graph has no negative cycles!
        int vertex = queue.front();
        queue.pop();
        used[vertex] = false;
        for (auto [next, weight] : graph[vertex]) {
            if (dist[vertex] + weight < dist[next]) {
                dist[next] = dist[vertex] + weight;
                if (!used[next]) {
                    used[next] = true;
                    queue.push(next);
                }
            }
        }
    }

    return dist;
}

// Find any negative cycle reachable from s using Ford-Bellman algorithm: O(VE)
auto FindNegativeCycle(const Graph& graph, int source) {
    int size = graph.Size();
    std::vector<int> dist(size, kInf), parent(size);

    dist[source] = 0;
    bool changed = true;
    for (int iter = 0; changed && iter < size - 1; ++iter) {
        changed = false;
        for (int vertex = 0; vertex < size; ++vertex) {
            if (dist[vertex] != kInf) {
                for (auto [next, weight] : graph[vertex]) {
                    if (dist[vertex] + weight < dist[next]) {
                        changed = true;
                        dist[next] = dist[vertex] + weight;
                        parent[next] = vertex;
                    }
                }
            }
        }
    }

    std::vector<int> cycle;
    if (changed) {
        for (int vertex = 0; cycle.empty() && vertex < size; ++vertex) {
            for (auto [next, weight] : graph[vertex]) {
                if (dist[vertex] + weight < dist[next]) {
                    cycle.push_back(vertex);
                    do {
                        vertex = parent[vertex];
                        cycle.push_back(vertex);
                    } while (vertex != next);
                    break;
                }
            }
        }
    }

    return cycle;
}

// Evaluate distance matrix using Floyd-Warshall algorithm: O(V^3)
auto FloydWarshall(const AdjMatrix& graph) {
    int size = graph.size();
    AdjMatrix dist(graph);
    for (int k = 0; k < size; ++k) {
        for (int u = 0; u < size; ++u) {
            for (int v = 0; v < size; ++v) {
                if (dist[u][k] < kInf && dist[k][v] < kInf) {
                    dist[u][v] = std::min(dist[u][v], dist[u][k] + dist[k][v]);
                }
            }
        }
    }
    return dist;
}

// Find all pairs (u, v) such that dist[u][v] can be infinitely small: O(V^3)
auto FindInfinitelySmallPaths(const AdjMatrix& graph) {
    int size = graph.size();
    auto dist = FloydWarshall(graph);
    std::vector<std::pair<int, int>> pairs;
    for (int u = 0; u < size; ++u) {
        for (int v = 0; v < size; ++v) {
            for (int k = 0; k < size; ++k) {
                if (dist[u][k] < kInf && dist[k][v] < kInf && dist[k][k] < 0) {
                    pairs.emplace_back(u, v);
                    break;
                }
            }
        }
    }
    return pairs;
}

// Evaluate distance matrix using Johnson's algorithm: O(VE log V)
auto Johnson(Graph graph) {
    int size = graph.Size();
    graph.Resize(size + 1);
    for (int vertex = 0; vertex < size; ++vertex) {
        graph.AddDirectedEdge(size, vertex, 0);
    }
    auto phi = SPFA(graph, size);  // make sure graph has no negative cycles!

    for (int from = 0; from < size; ++from) {
        for (auto& [to, weight] : graph[from]) {
            if (to != size) {
                weight += phi[from] - phi[to];
            }
        }
    }

    AdjMatrix dist(size, std::vector<int>(size, kInf));
    for (int from = 0; from < size; ++from) {
        dist[from] = Dijkstra(graph, from);
        for (int to = 0; to < size; ++to) {
            dist[from][to] -= phi[from] - phi[to];
        }
    }

    return dist;
}

int main() {

    // Case 1: Dijkstra's algorithm
    {
        int n, m, s, u, v, w;
        std::cin >> n >> m >> s;
        Graph graph(n);
        bool can = true;
        while (m--) {
            std::cin >> u >> v >> w;
            if (w < 0) {
                can = false;
            }
            graph.AddDirectedEdge(u, v, w);
        }

        if (!can) {
            std::cout << "The algorithm is not applicable, sorry :(\n\n";
        } else {
            auto dist = Dijkstra(graph, s);
            for (int u = 0; u < n; ++u) {
                if (dist[u] == kInf) {
                    std::cout << "INF ";
                } else {
                    std::cout << dist[u] << ' ';
                }
            }
            std::cout << "\n\n";
        }
    }

    // Case 2: Ford-Bellman algorithm
    {
        int n, m, s, u, v, w;
        std::cin >> n >> m >> s;
        Graph graph(n);
        while (m--) {
            std::cin >> u >> v >> w;
            graph.AddDirectedEdge(u, v, w);
        }

        auto dist = FordBellman(graph, s);

        for (int u = 0; u < n; ++u) {
            if (dist[u] == kInf) {
                std::cout << "INF ";
            } else {
                std::cout << dist[u] << ' ';
            }
        }
        std::cout << "\n\n";
    }

    // Case 3: SPFA algorithm
    {
        int n, m, s, u, v, w;
        std::cin >> n >> m >> s;
        Graph graph(n);
        while (m--) {
            std::cin >> u >> v >> w;
            graph.AddDirectedEdge(u, v, w);
        }

        auto dist = SPFA(graph, s);

        for (int u = 0; u < n; ++u) {
            if (dist[u] == kInf) {
                std::cout << "INF ";
            } else {
                std::cout << dist[u] << ' ';
            }
        }
        std::cout << "\n\n";
    }

    // Case 4: find any negative cycle reachable from source
    {
        int n, m, s, u, v, w;
        std::cin >> n >> m >> s;
        Graph graph(n);
        while (m--) {
            std::cin >> u >> v >> w;
            graph.AddDirectedEdge(u, v, w);
        }

        auto cycle = FindNegativeCycle(graph, s);

        if (cycle.empty()) {
            std::cout << "No negative cycles have been detected\n\n";
        } else {
            for (auto u : cycle) {
                std::cout << u << ' ';
            }
            std::cout << "\n\n";
        }
    }

    // Case 5: Floyd-Warshall algorithm
    {
        int n, m, u, v, w;
        std::cin >> n >> m;
        AdjMatrix graph(n, std::vector<int>(n, kInf));
        while (m--) {
            std::cin >> u >> v >> w;
            graph[u][v] = w;
        }

        auto dist = FloydWarshall(graph);

        for (int u = 0; u < n; ++u) {
            for (int v = 0; v < n; ++v) {
                if (dist[u][v] == kInf) {
                    std::cout << "INF ";
                } else {
                    std::cout << dist[u][v] << ' ';
                }
            }
            std::cout << '\n';
        }
    }

    // Case 6: find all pairs (u, v) such that dist[u][v] can be infinitely small
    {
        int n, m, u, v, w;
        std::cin >> n >> m;
        AdjMatrix graph(n, std::vector<int>(n));
        while (m--) {
            std::cin >> u >> v >> w;
            graph[u][v] = w;
        }

        auto pairs = FindInfinitelySmallPaths(graph);

        if (pairs.empty()) {
            std::cout << "No such pairs have been found\n";
        } else {
            for (auto [u, v] : pairs) {
                std::cout << u << ' ' << v << '\n';
            }
        }
    }

    // Case 7: Johnson's algorithm
    {
        int n, m, u, v, w;
        std::cin >> n >> m;
        Graph graph(n);
        while (m--) {
            std::cin >> u >> v >> w;
            graph.AddDirectedEdge(u, v, w);
        }

        auto dist = Johnson(graph);

        for (int u = 0; u < n; ++u) {
            for (int v = 0; v < n; ++v) {
                if (dist[u][v] == kInf) {
                    std::cout << "INF ";
                } else {
                    std::cout << dist[u][v] << ' ';
                }
            }
            std::cout << '\n';
        }
    }

    return 0;
}
