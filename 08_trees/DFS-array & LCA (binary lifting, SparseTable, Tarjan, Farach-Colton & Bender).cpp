#include <algorithm>
#include <iostream>
#include <vector>

class Graph {
    std::vector<std::vector<int>> edges_;

public:
    explicit Graph(int size) : edges_(size) {
    }

    int Size() const {
        return edges_.size();
    }

    void AddEdge(int from, int to) {
        edges_[from].push_back(to);
    }

    const auto& operator[](int vertex) const {
        return edges_[vertex];
    }
};

// Calculate ceil(log2)
inline int Log2(int n) {
    int ans = 0;
    while (n > 1) {
        ++ans, n >>= 1;
    }
    return ans;
}

// DFS-array construction: O(V)
void DFS(const Graph& graph, std::vector<int>& dfa, std::vector<int>& depth,
         std::vector<int>& parent, int vertex) {
    dfa.push_back(vertex);
    for (auto next : graph[vertex]) {
        if (next != parent[vertex]) {
            depth[next] = depth[vertex] + 1;
            parent[next] = vertex;
            DFS(graph, dfa, depth, parent, next);
            dfa.push_back(vertex);
        }
    }
}

// Auxiliary function for binary lifting
int GetAncestor(const std::vector<std::vector<int>>& lca, int vertex, int dist) {
    int log = lca.size() - 1;
    for (int level = 0; level <= log; ++level) {
        if ((dist >> level) & 1) {
            vertex = lca[level][vertex];
        }
    }
    return vertex;
}

// Sparse table for range minimum queries
template<typename T>
class SparseTable {
    std::vector<std::vector<T>> arr_;
    std::vector<int> log2_;

public:
    SparseTable(const std::vector<T>& arr) {
        int size = arr.size();
        log2_.resize(size + 1);
        arr_.emplace_back(arr);
        for (int level = 1, sz = 2; sz <= size; ++level, sz *= 2) {
            for (int i = sz; i <= std::min(size, 2 * sz - 1); ++i) {
                log2_[i] = level;
            }
            arr_.emplace_back(size - sz + 1);
            for (int i = 0; i + sz - 1 < size; ++i) {
                arr_[level][i] = std::min(arr_[level - 1][i], arr_[level - 1][i + sz / 2]);
            }
        }
    }

    const T GetMin(int left, int right) const {
        int width = right - left + 1, level = log2_[width];
        return std::min(arr_[level][left], arr_[level][right - (1 << level) + 1]);
    }
};

// Disjoint Set Union structure
class DSU {
    std::vector<int> parent_, rank_, ancestor_;

public:
    explicit DSU(int size) : parent_(size), rank_(size, 1), ancestor_(size) {
        for (int vertex = 0; vertex < size; ++vertex) {
            parent_[vertex] = ancestor_[vertex] = vertex;
        }
    }

    int GetAncestor(int vertex) {
        return ancestor_[FindSet(vertex)];
    }

    int FindSet(int vertex) {
        if (parent_[vertex] != vertex) {
            parent_[vertex] = FindSet(parent_[vertex]);
        }
        return parent_[vertex];
    }

    void UniteSets(int left, int right, int anc) {
        left = FindSet(left);
        right = FindSet(right);
        if (left != right) {
            if (rank_[left] > rank_[right]) {
                std::swap(left, right);
            }
            parent_[left] = right;
            rank_[right] += rank_[left];
            ancestor_[right] = anc;
        }
    }
};

// Tarjan algorithm for offline LCA problem
void Tarjan(const Graph& graph, std::vector<bool>& used, DSU& dsu,
            const std::vector<std::vector<std::pair<int, int>>>& queries,
            std::vector<int>& answers, int vertex) {
    used[vertex] = true;
    for (auto next : graph[vertex]) {
        if (!used[next]) {
            Tarjan(graph, used, dsu, queries, answers, next);
            dsu.UniteSets(vertex, next, vertex);
        }
    }

    for (auto [node, idx] : queries[vertex]) {
        if (used[node]) {
            answers[idx] = dsu.GetAncestor(node);
        }
    }
}

// LCA structure for Farach-Colton & Bender algorithm
class LCA {
    SparseTable<std::pair<int, int>>* table_;
    std::vector<std::vector<std::vector<int>>> block_min_;
    std::vector<int> mask_, dfa_, pos_, depth_;
    int block_size_;

    int Choose(int i, int j) const {
        return depth_[dfa_[i]] < depth_[dfa_[j]] ? i : j;
    }

    int InBlock(int b, int i, int j) const {
        return b * block_size_ + block_min_[mask_[b]][i][j];
    }

public:
    LCA(const Graph& graph, int root) {
        int size = graph.Size();
        depth_.resize(size);
        pos_.resize(size);
        std::vector<int> parent(size);
        DFS(graph, dfa_, depth_, parent, root);

        int k = 2 * size - 1;
        for (int i = 0; i < k; ++i) {
            pos_[dfa_[i]] = i;
        }

        block_size_ = std::max(1, Log2(k / 2));
        int block_cnt = (k + block_size_ - 1) / block_size_;
        block_min_.resize(1 << (block_size_ - 1));
        mask_.assign(block_cnt, 0);

        for (int b = 0; b < block_cnt; ++b) {
            int i = b * block_size_;
            for (int j = 1; j < std::min(k - i, block_size_); ++j) {
                mask_[b] <<= 1;
                mask_[b] |= (Choose(i + j - 1, i + j) == i + j);
            }
        }

        std::vector<std::pair<int, int>> arr;
        arr.reserve(block_cnt);
        for (int b = 0; b < block_cnt; ++b) {
            int m = mask_[b];
            if (block_min_[m].empty()) {
                block_min_[m].assign(block_size_, std::vector<int>(block_size_));
                int base = block_size_ * b;
                for (int i = 0; i < block_size_; ++i) {
                    block_min_[m][i][i] = i;
                    for (int j = i + 1; j < block_size_; ++j) {
                        block_min_[m][i][j] = block_min_[m][i][j - 1];
                        int i1 = base + block_min_[m][i][j], i2 = base + j;
                        if (i2 < k && Choose(i1, i2) == i2) {
                            block_min_[m][i][j] = j;
                        }
                    }
                }
            }
            int idx = block_min_[m][0][block_size_ - 1] + b * block_size_;
            arr.emplace_back(depth_[dfa_[idx]], idx);
        }

        table_ = new SparseTable(arr);
    }

    int GetLCA(int u, int v) const {
        int pu = pos_[u], pv = pos_[v];
        if (pu > pv) {
            std::swap(pu, pv);
        }

        int bu = pu / block_size_, bv = pv / block_size_;
        if (bu == bv) {
            return dfa_[InBlock(bu, pu % block_size_, pv % block_size_)];
        }

        int idx = Choose(InBlock(bu, pu % block_size_, block_size_ - 1),
                         InBlock(bv, 0, pv % block_size_));
        if (bu + 1 < bv) {
            idx = Choose(idx, table_->GetMin(bu + 1, bv - 1).second);
        }
        return dfa_[idx];
    }

    ~LCA() {
        delete table_;
    }
};

int main() {
    // Case 1: LCA = binary lifting in <O(n log n), O(log n)>
    {
        int n, root, qs, u, v;
        std::cin >> n >> root >> qs;
        Graph graph(n);
        for (int i = 0; i < n - 1; ++i) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        int lg = Log2(n - 1);
        std::vector<int> dfa, depth(n);
        std::vector<std::vector<int>> jmp(lg + 1, std::vector<int>(n));
        DFS(graph, dfa, depth, jmp[0], root);
        for (int L = 1; L <= lg; ++L) {
            for (int u = 0; u < n; ++u) {
                int v = jmp[L - 1][u];
                jmp[L][u] = jmp[L - 1][v];
            }
        }

        while (qs--) {
            std::cin >> u >> v;
            if (depth[u] < depth[v]) {
                std::swap(u, v);
            }
            if (depth[u] > depth[v]) {
                u = GetAncestor(jmp, u, depth[u] - depth[v]);
            }
            if (u != v) {
                for (int d = lg; d >= 0; --d) {
                    int pu = jmp[d][u], pv = jmp[d][v];
                    if (pu != pv) {
                        u = pu, v = pv;
                    }
                }
                u = jmp[0][u];
            }
            std::cout << u << '\n';
        }
        std::cout << "\n\n";
    }

    // Case 2: DFS-array of tree
    {
        int n, root;
        std::cin >> n >> root;
        Graph graph(n);
        for (int u, v, i = 0; i < n - 1; ++i) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        std::vector<int> dfa, depth(n), parent(n);
        DFS(graph, dfa, depth, parent, root);

        for (auto u : dfa) {
            std::cout << u << ' ';
        }
        std::cout << "\n\n";
    }

    // Case 3: LCA = dfa + sparse table in <O(n log n), O(1)>
    {
        int n, root, qs, u, v;
        std::cin >> n >> root >> qs;
        Graph graph(n);
        for (int i = 0; i < n - 1; ++i) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        std::vector<int> dfa, depth(n), parent(n), pos(n);
        DFS(graph, dfa, depth, parent, root);
        std::vector<std::pair<int, int>> arr;
        arr.reserve(2 * n - 1);
        for (int i = 0; i < 2 * n - 1; ++i) {
            pos[dfa[i]] = i;
            arr.emplace_back(depth[dfa[i]], dfa[i]);
        }
        SparseTable<std::pair<int, int>> lca(arr);

        while (qs--) {
            std::cin >> u >> v;
            int pu = pos[u], pv = pos[v];
            if (pu > pv) {
                std::swap(pu, pv);
            }
            auto [d, a] = lca.GetMin(pu, pv);
            std::cout << a << '\n';
        }
        std::cout << '\n';
    }

    // Case 4: LCA = Tarjan offline algorithm in <O(n), O(1)>
    {
        int n, root, qs, u, v;
        std::cin >> n >> root >> qs;
        Graph graph(n);
        for (int i = 0; i < n - 1; ++i) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        std::vector<int> answers(qs);
        std::vector<std::vector<std::pair<int, int>>> queries(n);
        for (int i = 0; i < qs; ++i) {
            std::cin >> u >> v;
            queries[u].emplace_back(v, i);
            queries[v].emplace_back(u, i);
        }

        std::vector<bool> used(n);
        DSU dsu(n);
        Tarjan(graph, used, dsu, queries, answers, root);

        for (auto u : answers) {
            std::cout << u << '\n';
        }
        std::cout << '\n';
    }

    // Case 5: LCA = Farach-Colton & Bender online algorithm in <O(n), O(1)>
    {
        int n, root, qs, u, v;
        std::cin >> n >> root >> qs;
        Graph graph(n);
        for (int i = 0; i < n - 1; ++i) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        LCA lca(graph, root);

        while (qs--) {
            std::cin >> u >> v;
            std::cout << lca.GetLCA(u, v) << '\n';
        }
    }

    return 0;
}
