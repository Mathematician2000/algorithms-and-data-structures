#include <algorithm>
#include <iostream>
#include <tuple>
#include <vector>

constexpr int kNone = -1;

class Graph {
    std::vector<std::vector<int>> edges_;

public:
    explicit Graph(int size) : edges_(size) {
    }

    int Size() const {
        return edges_.size();
    }

    void AddEdge(int from, int to) {
        edges_[from].push_back(to);
        if (from != to) {
            edges_[from].push_back(to);
        }
    }

    const auto& operator[](int vertex) const {
        return edges_[vertex];
    }
};

// Find distances from the source using DFS: O(V)
void DFS(const Graph& graph, std::vector<int>& dist,
         int vertex, int parent = kNone) {
    for (auto next : graph[vertex]) {
        if (next != parent) {
            dist[next] = dist[vertex] + 1;
            DFS(graph, dist, next, vertex);
        }
    }
}

// Find diameter of the graph using 2 DFS runs: O(V)
auto FindDiameter(const Graph& graph) {
    int size = graph.Size();
    std::vector<int> dist(size);
    DFS(graph, dist, 1);
    int u = std::max_element(dist.begin(), dist.end()) - dist.begin();
    DFS(graph, dist, u);
    int v = std::max_element(dist.begin(), dist.end()) - dist.begin();

    return std::make_tuple(u, v, dist[v]);
}

// Find eccentricities of all vertices: O(V)
auto FindEccentricities(const Graph& graph) {
    int size = graph.Size();
    std::vector<int> du(size), dv(size);
    DFS(graph, du, 1);
    int u = std::max_element(du.begin(), du.end()) - du.begin();
    DFS(graph, du, u);
    int v = std::max_element(du.begin(), du.end()) - du.begin();
    DFS(graph, dv, v);

    std::vector<std::pair<int, int>> ecc(size);
    for (int x = 0; x < size; ++x) {
        auto pu = std::make_pair(du[x], u);
        auto pv = std::make_pair(dv[x], v);
        ecc[x] = std::max(pu, pv);
    }
    return ecc;
}

// Find sizes of subtrees and sum of the distances to the nodes in subtrees: O(V)
int64_t SubtreeSizeDFS(const Graph& graph, std::vector<int>& sz,
                       int vertex, int parent = kNone) {
    sz[vertex] = 1;
    int64_t ans = 0;
    for (auto next : graph[vertex]) {
        if (next != parent) {
            ans += SubtreeSizeDFS(graph, sz, next, vertex);
            ans += sz[next];
            sz[vertex] += sz[next];
        }
    }
    return ans;
}

// Find sum of the distances to all other nodes using rerooting technique: O(V)
void FindSumOfDistances(const Graph& graph, std::vector<int64_t>& dist,
                        const std::vector<int>& sz, int vertex, int parent = kNone) {
    if (parent != kNone) {
        int size = graph.Size();
        dist[vertex] = dist[parent] + size - 2 * sz[vertex];
    }

    for (auto next : graph[vertex]) {
        if (next != parent) {
            FindSumOfDistances(graph, dist, sz, next, vertex);
        }
    }
}

int main() {
    // Case 1: diameter and radius of the graph
    {
        int n;
        std::cin >> n;
        Graph graph(n);
        for (int u, v, i = 0; i < n - 1; ++i) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        auto [u, v, d] = FindDiameter(graph);

        printf("Diameter (%d - %d) = %d, radius = %d\n\n", u, v, d, (d + 1) / 2);
    }

    // Case 2: find eccentricities of all vertices
    {
        int n;
        std::cin >> n;
        Graph graph(n);
        for (int u, v, i = 0; i < n - 1; ++i) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        auto ecc = FindEccentricities(graph);

        for (int u = 0; u < n; ++u) {
            auto [e, v] = ecc[u];
            printf("ecc[%d] = d(%d, %d) = %d\n", u, u, v, e);
        }
        std::cout << '\n';
    }

    // Case 3: find sum of the distances to all other nodes
    {
        int n;
        std::cin >> n;
        Graph graph(n);
        for (int u, v, i = 0; i < n - 1; ++i) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        std::vector<int> sz(n);
        std::vector<int64_t> dist(n);
        dist[0] = SubtreeSizeDFS(graph, sz, 0);
        FindSumOfDistances(graph, dist, sz, 0);

        for (int u = 0; u < n; ++u) {
            printf("%d: sz = %d, dist = %ld\n", u, sz[u], dist[u]);
        }
    }

    return 0;
}
