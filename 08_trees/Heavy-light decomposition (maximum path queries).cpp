#include <algorithm>
#include <iostream>
#include <vector>

constexpr int kInf = 1e9;
constexpr int kNone = -1;

class Graph {
    std::vector<std::vector<int>> edges_;

public:
    explicit Graph(int size) : edges_(size) {
    }

    int Size() const {
        return edges_.size();
    }

    void AddEdge(int from, int to) {
        edges_[from].push_back(to);
        if (from != to) {
            edges_[to].push_back(from);
        }
    }

    const auto& operator[](int vertex) const {
        return edges_[vertex];
    }
};

// DFS for detecting heavy edges: O(V)
int DFS(const Graph& graph, std::vector<int>& depth,
        std::vector<int>& parent, std::vector<int>& heavy, int vertex) {
    int sz = 1, mx = 0;
    for (auto next : graph[vertex]) {
        if (next != parent[vertex]) {
            depth[next] = depth[vertex] + 1;
            parent[next] = vertex;
            int d_sz = DFS(graph, depth, parent, heavy, next);
            sz += d_sz;
            if (d_sz > mx) {
                mx = d_sz;
                heavy[vertex] = next;
            }
        }
    }
    return sz;
}

// Construct HLD (heavy-light decomposition): O(V)
void HLD(const Graph& graph, const std::vector<int>& heavy, std::vector<int>& head,
         std::vector<int>& pos, int& cur_pos, int cur_head,
         int vertex, int parent = kNone) {
    head[vertex] = cur_head;
    pos[vertex] = cur_pos++;
    if (heavy[vertex]) {
        HLD(graph, heavy, head, pos, cur_pos, cur_head, heavy[vertex], vertex);
    }
    for (auto next : graph[vertex]) {
        if (next != parent && next != heavy[vertex]) {
            HLD(graph, heavy, head, pos, cur_pos, next, next, vertex);
        }
    }
}

// Segment tree for maximum function
class SegTree {
    std::vector<int> tree_;
    int size_;

public:
    SegTree(const std::vector<int>& arr) {
        int size = arr.size();
        size_ = 1;
        while (size_ < size) {
            size_ <<= 1;
        }

        tree_.assign(2 * size_ - 1, -kInf);
        for (int i = 0; i < size; ++i) {
            tree_[i + size_ - 1] = arr[i];
        }
        for (int i = size_ - 2; i >= 0; --i) {
            tree_[i] = std::max(tree_[2 * i + 1], tree_[2 * i + 2]);
        }
    }

    void Update(int pos, int val, int idx = 0, int left = 0, int right = kNone) {
        if (right == kNone) {
            right = size_;
        }
        if (left + 1 == right) {
            tree_[idx] = val;
            return;
        }

        int mid = (left + right) / 2;
        if (pos < mid) {
            Update(pos, val, 2 * idx + 1, left, mid);
        } else {
            Update(pos, val, 2 * idx + 2, mid, right);
        }
        tree_[idx] = std::max(tree_[2 * idx + 1], tree_[2 * idx + 2]);
    }

    int GetMax(int from, int to, int idx = 0, int left = 0, int right = kNone) const {
        if (right == kNone) {
            right = size_;
        }
        if (left == from && to == right) {
            return tree_[idx];
        }

        int mid = (left + right) / 2, ans = -kInf;
        if (from < mid) {
            ans = std::max(ans, GetMax(from, std::min(mid, to), 2 * idx + 1, left, mid));
        }
        if (to > mid) {
            ans = std::max(ans, GetMax(std::max(from, mid), to, 2 * idx + 2, mid, right));
        }
        return ans;
    }
};

int main() {
    // Case 1: find max value on path between nodes + update single values in <O(n), O(log^2 n)>
    {
        int n, u, v, qs;
        std::string query;
        std::cin >> n >> qs;
        Graph graph(n);
        for (int i = 0; i < n - 1; ++i) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        std::vector<int> depth(n), parent(n), heavy(n), head(n), pos(n);
        int cur_pos = 0;
        DFS(graph, depth, parent, heavy, 0);
        HLD(graph, heavy, head, pos, cur_pos, 0, 0);

        std::vector<int> val(n);
        for (int u = 0; u < n; ++u) {
            std::cin >> val[pos[u]];
        }
        SegTree tree(val);

        while (qs--) {
            std::cin >> query >> u >> v;
            if (query == "set") {
                tree.Update(pos[u], v);
            } else if (query == "get") {
                int mx = -kInf;
                while (head[u] != head[v]) {
                    if (depth[head[u]] > depth[head[v]]) {
                        std::swap(u, v);
                    }
                    mx = std::max(mx, tree.GetMax(pos[head[v]], pos[v] + 1));
                    v = parent[head[v]];
                }
                if (depth[u] > depth[v]) {
                    std::swap(u, v);
                }
                mx = std::max(mx, tree.GetMax(pos[u], pos[v] + 1));
                std::cout << mx << '\n';
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
    }

    return 0;
}
