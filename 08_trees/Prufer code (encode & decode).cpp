#include <iostream>
#include <vector>

class Graph {
    std::vector<std::vector<int>> edges_;

public:
    explicit Graph(int size) : edges_(size) {
    }

    int Size() const {
        return edges_.size();
    }

    void AddEdge(int from, int to) {
        edges_[from].push_back(to);
        if (from != to) {
            edges_[to].push_back(from);
        }
    }

    const auto& operator[](int vertex) const {
        return edges_[vertex];
    }
};

// Ordinary DFS: O(V)
void DFS(const Graph& graph, std::vector<int>& parent, int vertex) {
    for (auto next : graph[vertex]) {
        if (next != parent[vertex]) {
            parent[next] = vertex;
            DFS(graph, parent, next);
        }
    }
}

// Encode a tree into a Prufer code: O(V)
auto ToPrufer(const Graph& graph) {
    int size = graph.Size(), leaf = 0;
    std::vector<int> parent(size), deg(size), code;
    DFS(graph, parent, size);
    for (int u = 0; u < size; ++u) {
        deg[u] = graph[u].size();
        if (deg[u] == 1 && !leaf) {
            leaf = u;
        }
    }

    int last = leaf;
    for (int i = 0; i < size - 2; ++i) {
        int u = parent[leaf];
        code.push_back(u);
        --deg[leaf];
        if (--deg[u] == 1 && u < last) {
            leaf = u;
        } else {
            do {
                ++last;
            } while (deg[last] != 1);
            leaf = last;
        }
    }

    return code;
}

// Decode a Prufer code: O(V)
auto FromPrufer(const std::vector<int>& code) {
    int size = code.size() + 2, leaf = 0;
    Graph graph(size);
    std::vector<int> deg(size, 1);
    for (auto u : code) {
        ++deg[u];
    }
    do {
        ++leaf;
    } while (deg[leaf] != 1);

    int last = leaf;
    for (auto u : code) {
        graph.AddEdge(u, leaf);
        --deg[leaf];
        if (--deg[u] == 1 && u < last) {
            leaf = u;
        } else {
            do {
                ++last;
            } while (deg[last] != 1);
            leaf = last;
        }
    }
    graph.AddEdge(size - 1, leaf);

    return graph;
}

int main() {
    // Case 1: construct the Prufer code
    {
        int n;
        std::cin >> n;
        Graph graph(n);
        for (int u, v, i = 0; i < n - 1; ++i) {
            std::cin >> u >> v;
            graph.AddEdge(u, v);
        }

        auto code = ToPrufer(graph);

        for (auto u : code) {
            std::cout << u << ' ';
        }
        std::cout << "\n\n";
    }

    // Case 2: decode the Prufer code
    {
        int n;
        std::cin >> n;
        std::vector<int> code(n - 2);
        for (int i = 0; i < n - 2; ++i) {
            std::cin >> code[i];
        }

        auto graph = FromPrufer(code);

        for (int u = 0; u < n; ++u) {
            for (auto v : graph[u]) {
                if (u < v) {
                    printf("%d - %d\n", u, v);
                }
            }
        }
    }

    return 0;
}
