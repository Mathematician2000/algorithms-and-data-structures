#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

// Construct Lyndon factorization using Duval algorithm: O(n)
auto LyndonFactorization(const std::string& str) {
    int len = str.length();
    int i = 0;
    std::vector<int> parts;
    while (i < len) {
        int j = i + 1, k = i;
        while (j < len && str[k] <= str[j]) {
            k = (str[k] == str[j] ? k + 1 : i);
            ++j;
        }
        while (i <= k) {
            parts.push_back(j - k);
            i += j - k;
        }
    }
    return parts;
}

// Find minimum cyclic shift: O(n)
int MinCyclicShift(const std::string& str) {
    int len = str.length();
    auto parts = LyndonFactorization(str + str);

    int total = 0;
    for (auto part : parts) {
        if (total + part >= len) {
            return total;
        }
        total += part;
    }
    return 0;
}

int main() {
    // Case 1: split a string into simple substrings
    {
        std::string str;
        std::cin >> str;

        auto parts = LyndonFactorization(str);

        int ptr = 0;
        for (auto part : parts) {
            std::cout << (ptr ? " | " : "") << str.substr(ptr, part);
            ptr += part;
        }
        std::cout << "\n\n";
    }

    // Case 2: find the smallest cyclic shift
    {
        std::string str;
        std::cin >> str;

        int len = str.length();
        int start = MinCyclicShift(str);
        std::string shift = str.substr(start, len - start) + str.substr(0, start);

        std::cout << shift << '\n';
    }
}
