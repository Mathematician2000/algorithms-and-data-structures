#include <algorithm>
#include <iostream>
#include <string>
#include <utility>
#include <vector>

// Find all palindrome substrings using Manacher's algorithm: O(|s|)
auto Manacher(const std::string& s) {
    int n = s.length();
    std::vector<int> d1(n), d2(n);

    for (int i = 0, l = 0, r = -1; i < n; ++i) {
        int k = (i > r ? 1 : std::min(r - i + 1, d1[l + r - i]));
        while (0 <= i - k && i + k < n && s[i - k] == s[i + k]) {
            ++k;
        }
        d1[i] = k--;
        if (i + k > r) {
            l = i - k, r = i + k;
        }
    }

    for (int i = 0, l = 0, r = -1; i < n; ++i) {
        int k = (i > r ? 0 : std::min(r - i, d2[l + r - i]));
        while (0 <= i - k && i + k + 1 < n && s[i - k] == s[i + k + 1]) {
            ++k;
        }
        d2[i] = k;
        if (i + k > r) {
            l = i - k + 1, r = i + k;
        }
    }

    return std::make_pair(d1, d2);
}

int main() {
    // Case 1: find maximum palindrome substrings
    {
        std::string s;
        std::cin >> s;

        auto [d1, d2] = Manacher(s);

        int n = s.length();
        for (int i = 0; i < n; ++i) {
            if (2 * d1[i] - 1 > 2 * d2[i]) {
                printf("%d: %s\n", i, s.substr(i - d1[i] + 1, 2 * d1[i] - 1).c_str());
            } else {
                printf("%d: %s\n", i, s.substr(i - d2[i] + 1, 2 * d2[i]).c_str());
            }
        }
        std::cout << '\n';
    }

    // Case 2: find number of palindrome substrings
    {
        std::string s;
        std::cin >> s;

        auto [d1, d2] = Manacher(s);

        int n = s.length(), ans = 0;
        for (int i = 0; i < n; ++i) {
            ans += d1[i] + d2[i];
        }
        std::cout << ans << '\n';
    }

    return 0;
}
