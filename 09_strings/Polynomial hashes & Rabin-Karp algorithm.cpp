#include <iostream>
#include <vector>

constexpr int kPrime = 31;
constexpr int kMod = 1'000'000'007;

// Calculate hash value of a string: O(|s|)
int GetHash(const std::string& s) {
    int hash = 0;
    int64_t pow = 1;
    for (auto c : s) {
        hash = (hash + pow * (c - 'a' + 1)) % kMod;
        pow = (pow * kPrime) % kMod;
    }
    return hash;
}

// Find all occurrences of a pattern in the text using Rabin-Karp algorithm: O(|T| + |P|)
auto RabinKarp(const std::string& text, const std::string& pattern) {
    int n = text.length(), m = pattern.length();
    std::vector<int> pref(n + 1), ids;
    std::vector<int64_t> pow(n + 1);
    pow[0] = 1;
    for (int i = 1; i <= n; ++i) {
        pow[i] = (pow[i - 1] * kPrime) % kMod;
        pref[i] = (pref[i - 1] + pow[i - 1] * (text[i - 1] - 'a' + 1)) % kMod;
    }

    int ph = (GetHash(pattern) * pow[n]) % kMod;
    for (int i = 0; i + m - 1 < n; ++i) {
        int th = ((pref[i + m] - pref[i] + kMod) * pow[n - i]) % kMod;
        if (th == ph) {
            ids.push_back(i);
        }
    }
    return ids;
}

int main() {
    // Case 1: calculate hash of a string
    {
        std::string s;
        std::cin >> s;

        std::cout << GetHash(s) << "\n\n";
    }

    // Case 2: calculate substring hashes in O(1)
    {
        int i, j, qs;
        std::string s;
        std::cin >> s >> qs;

        int n = s.length();
        std::vector<int> pref(n + 1), pow(n + 1);
        pow[0] = 1;
        for (int i = 1; i <= n; ++i) {
            pow[i] = (pow[i - 1] * 1LL * kPrime) % kMod;
            pref[i] = (pref[i - 1] + pow[i - 1] * (s[i - 1] - 'a' + 1LL)) % kMod;
        }

        while (qs--) {
            std::cin >> i >> j;
            std::cout << ((pref[j] - pref[i - 1] + kMod) % kMod * 1LL * pow[n - i]) % kMod << '\n';
        }
        std::cout << '\n';
    }

    // Case 3: find all occurrences of a pattern in the text
    {
        std::string text, pattern;
        std::cin >> text >> pattern;

        auto ids = RabinKarp(text, pattern);

        if (ids.empty()) {
            std::cout << "Not found :(\n";
        } else {
            std::cout << "Indices:";
            for (auto i : ids) {
                std::cout << ' ' << i;
            }
            std::cout << '\n';
        }
    }

    return 0;
}
