#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

// Calculating prefix function: O(|s|)
auto PrefixFunction(const std::string& s) {
    int n = s.length();
    std::vector<int> pref(n);
    for (int i = 1; i < n; ++i) {
        int j = i;
        do {
            j = pref[j - 1];
        } while (j && s[i] != s[j]);
        j += (s[i] == s[j]);
        pref[i] = j;
    }
    return pref;
}

// Calculating Z-function: O(|s|)
auto ZFunction(const std::string& s) {
    int n = s.length();
    std::vector<int> zfunc(n);
    for (int i = 1, l = 0, r = 0; i < n; ++i) {
        if (i <= r) {
            zfunc[i] = std::min(r - i + 1, zfunc[i - l]);
        }
        while (i + zfunc[i] < n && s[zfunc[i]] == s[i + zfunc[i]]) {
            ++zfunc[i];
        }
        if (i + zfunc[i] - 1 > r) {
            l = i, r = i + zfunc[i] - 1;
        }
    }
    return zfunc;
}

// Search for the pattern in the text using Knuth-Morris-Pratt algorithm: O(|T| + |P|)
auto KMP(const std::string& text, const std::string pattern) {
    auto pref = PrefixFunction(pattern + '#' + text);
    std::vector<int> ids;
    int n = pattern.length(), m = text.length();
    for (int i = n - 1; i < m; ++i) {
        if (pref[i + n + 1] == n) {
            ids.push_back(i - n + 1);
        }
    }
    return ids;
}

int main() {
    // Case 1: calculating prefix function and z-function
    {
        std::string s;
        std::cin >> s;

        auto pref = PrefixFunction(s);
        auto zfunc = ZFunction(s);

        std::cout << "p:";
        for (auto elem : pref) {
            std::cout << ' ' << elem;
        }
        std::cout << "\nz:";
        for (auto elem : zfunc) {
            std::cout << ' ' << elem;
        }
        std::cout << "\n\n";
    }

    // Case 2: find the pattern in the text using KMP algorithm
    {
        std::string text, pattern;
        std::cin >> text >> pattern;

        auto ids = KMP(text, pattern);

        if (ids.empty()) {
            std::cout << "Not found :(\n\n";
        } else {
            std::cout << "Indices:";
            for (auto i : ids) {
                std::cout << ' ' << i;
            }
            std::cout << "\n\n";
        }
    }

    // Case 3: find all periods of a string in O(|s|)
    {
        std::string s;
        std::cin >> s;

        auto zfunc = ZFunction(s);

        int n = s.length();
        for (int i = 1; i < n; ++i) {
            if (!(n % i) && i + zfunc[i] == n) {
                std::cout << i << ' ';
            }
        }
        std::cout << n << "\n\n";
    }

    // Case 4: find the number of distinct substrings in O(|s|^2)
    {
        std::string s;
        std::cin >> s;

        int n = s.length(), ans = 0;
        for (int i = 0; i < n; ++i) {
            std::string text = s.substr(0, i + 1);
            std::reverse(text.begin(), text.end());
            auto pref = PrefixFunction(text);
            ans += text.length() - *std::max_element(pref.begin(), pref.end());
        }

        std::cout << ans << "\n\n";
    }

    return 0;
}
