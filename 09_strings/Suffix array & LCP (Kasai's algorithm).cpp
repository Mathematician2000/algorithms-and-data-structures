#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <utility>
#include <vector>

constexpr int kInf = 1e9;
constexpr int kNone = -1;

// Reduced segment tree for minimum function
class SegTree {
    std::vector<int> tree_;
    int size_;

public:
    SegTree(const std::vector<int>& arr) {
        int size = arr.size();
        size_ = 1;
        while (size_ < size) {
            size_ *= 2;
        }

        tree_.assign(2 * size_ - 1, kInf);
        for (int i = 0; i < size; ++i) {
            tree_[i + size_ - 1] = arr[i];
        }
        for (int i = size_ - 2; i >= 0; --i) {
            tree_[i] = std::min(tree_[2 * i + 1], tree_[2 * i + 2]);
        }
    }

    int GetMin(int from, int to, int idx = 0, int left = 0, int right = kNone) const {
        if (right == kNone) {
            right = size_;
        }
        if (left == from && to == right) {
            return tree_[idx];
        }

        int mid = (left + right) / 2, ans = kInf;
        if (from < mid) {
            ans = std::min(ans, GetMin(from, std::min(mid, to), 2 * idx + 1, left, mid));
        }
        if (to > mid) {
            ans = std::min(ans, GetMin(std::max(from, mid), to, 2 * idx + 2, mid, right));
        }
        return ans;
    }
};

// Suffix array & LCP construction: O(|s| log |s|)
auto BuildSuffixArray(const std::string& s) {
    int n = s.length();

    // Phase 0
    int num_classes = 'z' + 1;
    std::vector<int> perm(n), classes(n), cnt(num_classes);
    for (auto c : s) {
        ++cnt[c];
    }
    for (int c = 'a'; c <= 'z'; ++c) {
        cnt[c] += cnt[c - 1];
    }
    for (int i = 0; i < n; ++i) {
        perm[--cnt[s[i]]] = i;
    }

    num_classes = 1;
    classes[perm[0]] = 0;
    for (int i = 1; i < n; ++i) {
        num_classes += (s[perm[i]] != s[perm[i - 1]]);
        classes[perm[i]] = num_classes - 1;
    }

    // Phase k
    std::vector<int> new_perm(n), new_classes(n);
    std::vector<int> lcp(n - 1), new_lcp(n - 1), left_pos(n), right_pos(n);
    for (int shift = 1; shift < n; shift *= 2) {
        for (int i = 0; i < n; ++i) {
            right_pos[classes[perm[i]]] = i;
        }
        for (int i = n - 1; i >= 0; --i) {
            left_pos[classes[perm[i]]] = i;
        }

        for (int i = 0; i < n; ++i) {
            new_perm[i] = perm[i] - shift;
            if (new_perm[i] < 0) {
                new_perm[i] += n;
            }
        }
        cnt.assign(num_classes, 0);
        for (int i = 0; i < n; ++i) {
            ++cnt[classes[new_perm[i]]];
        }
        for (int i = 1; i < num_classes; ++i) {
            cnt[i] += cnt[i - 1];
        }
        for (int i = n - 1; i >= 0; --i) {
            perm[--cnt[classes[new_perm[i]]]] = new_perm[i];
        }

        num_classes = 1;
        new_classes[perm[0]] = 0;
        for (int i = 1; i < n; ++i) {
            num_classes += (
                classes[perm[i]] != classes[perm[i - 1]] ||
                classes[(perm[i] + shift) % n] != classes[(perm[i - 1] + shift) % n]
            );
            new_classes[perm[i]] = num_classes - 1;
        }

        SegTree tree(lcp);
        for (int i = 0; i < n - 1; ++i) {
            int a1 = perm[i], b1 = perm[i + 1];
            if (classes[a1] != classes[b1]) {
                new_lcp[i] = lcp[right_pos[classes[a1]]];
            } else {
                int a2 = (a1 + shift) % n, b2 = (b1 + shift) % n;
                int lcp_value = shift + tree.GetMin(left_pos[classes[a2]],
                                                       right_pos[classes[b2]]);
                new_lcp[i] = std::min(n, lcp_value);
            }
        }
        lcp = new_lcp;
        classes = new_classes;
    }

    for (int i = 0; i < n - 1; ++i) {
        lcp[i] = std::min(lcp[i], std::min(n - perm[i], n - perm[i + 1]));
    }

    return std::make_pair(perm, lcp);
}

// LCP construction using Kasai's algorithm: O(|s|)
auto Kasai(const std::vector<int>& sa, const std::string& s) {
    int n = s.length(), idx = 0;
    std::vector<int> rank(n), lcp(n - 1);
    for (int i = 0; i < n; ++i) {
        rank[sa[i]] = i;
    }

    for (int i = 0; i < n - 1; ++i) {
        int rg = rank[i];
        if (rg == n - 1) {
            continue;
        }
        int j = sa[rg + 1];
        while (i + idx < n && j + idx < n && s[i + idx] == s[j + idx]) {
            ++idx;
        }
        lcp[rg] = idx;
        idx = std::max(0, idx - 1);
    }

    return lcp;
}

// Compare two strings: O(min{|s1|, |s2|})
int StrCmp(const std::string& s1, const std::string& s2) {
    int n = s1.length(), m = s2.length();
    for (int i = 0; i < std::min(n, m); ++i) {
        if (s1[i] != s2[i]) {
            return s1[i] - s2[i];
        }
    }
    return n - m;
}

// Find pattern in the text in O((|T| + |P|) log |T|)
// To find the index of the first occurrence: min(sa[lower_bound..upper_bound])
int FindSubstring(const std::vector<int>& sa, const std::string text, const std::string& pattern) {
    int n = text.length(), m = pattern.length();
    int left = 0, right = n - 1;
    while (left < right) {
        int mid = (left + right) / 2, idx = sa[mid];
        int cmp = StrCmp(text.substr(idx, m), pattern);
        if (!cmp) {
            return idx;
        } else if (cmp < 0) {
            left = mid + 1;
        } else {
            right = mid - 1;
        }
    }
    return text.substr(sa[left], m) == pattern ? sa[left] : kNone;
}

int main() {
    // Case 1: find suffix array and LCP array of a string
    {
        std::string s;
        std::cin >> s;

        s += ('a' - 1);  // ~ '$'
        auto [sa, lcp] = BuildSuffixArray(s);
        sa.erase(sa.begin());
        lcp.erase(lcp.begin());

        for (auto j : sa) {
            std::cout << j << ' ';
        }
        std::cout << '\n';
        for (auto j : lcp) {
            std::cout << j << ' ';
        }
        std::cout << "\n\n";
    }

    // Case 2: find the patterns in the text in O((n + M) log n)
    {
        int qs;
        std::string text, pattern;
        std::cin >> text >> qs;

        text += ('a' - 1);  // ~ '$'
        auto [sa, _] = BuildSuffixArray(text);
        sa.erase(sa.begin());

        while (qs--) {
            std::cin >> pattern;
            std::cout << FindSubstring(sa, text, pattern) << '\n';
        }
        std::cout << '\n';
    }

    // Case 3: find LCP array using Kasai's algorithm in O(n)
    {
        std::string s;
        std::cin >> s;

        s += ('a' - 1);  // ~ '$'
        auto [sa, lcp1] = BuildSuffixArray(s);  // O(n log n)
        auto lcp2 = Kasai(sa, s);  // O(n), needs suffix array :(
        lcp1.erase(lcp1.begin());
        lcp2.erase(lcp2.begin());

        std::cout << "LCP-1:";
        for (auto elem : lcp1) {
            std::cout << ' ' << elem;
        }

        std::cout << "\nLCP-2:";
        for (auto elem : lcp2) {
            std::cout << ' ' << elem;
        }
        std::cout << "\n\n";
    }

    // Case 4: count distinct substrings using LCP array in O(n)
    {
        std::string s;
        std::cin >> s;

        s += ('a' - 1);  // ~ '$'
        auto [_, lcp] = BuildSuffixArray(s);  // O(n log n)
        lcp.erase(lcp.begin());

        int n = s.length();
        std::cout << n * (n + 1) / 2 - std::accumulate(lcp.begin(), lcp.end(), 0) << '\n';
    }

    // Case 5: find k-th substring in O(|ans|)
    {
        std::string s;
        int k;
        std::cin >> s >> k;

        s += ('a' - 1);  // ~ '$'
        auto [sa, lcp] = BuildSuffixArray(s);  // O(n log n)
        sa.erase(sa.begin());
        lcp.erase(lcp.begin());

        int n = s.length();
        for (int i = 0; i < n; ++i) {
            int cnt = n - sa[i] - (i ? lcp[i - 1] : 0);
            if (k <= cnt) {
                std::cout << s.substr(sa[i], (i ? lcp[i - 1] : 0) + k) << '\n';
                break;
            }
            k -= cnt;
        }
    }

    return 0;
}
