#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

constexpr int kAlphabetSize = 'z' - 'a' + 1;
constexpr int kNone = -1;

// Suffix automaton
class SuffixAutomaton {
    struct State {
        std::vector<int> next;
        int64_t cnt = 0;
        int len, link, min_excl = 1;

        State(int len = 0, int link = kNone)
            : next(kAlphabetSize, kNone), len(len), link(link) {
        }
    };

    std::vector<State> states_;
    int last_ = 0;

    void Extend(char c) {
        c -= 'a';
        int cur = states_.size(), p;
        states_.emplace_back(states_[last_].len + 1);
        for (p = last_; p != kNone && states_[p].next[c] == kNone; p = states_[p].link) {
            states_[p].next[c] = cur;
        }
        if (p == kNone) {
            states_[cur].link = 0;
        } else {
            int q = states_[p].next[c];
            if (states_[p].len + 1 == states_[q].len) {
                states_[cur].link = q;
            } else {
                int clone = states_.size();
                states_.emplace_back(states_[p].len + 1, states_[q].link);
                states_[clone].next = states_[q].next;
                for (; p != kNone && states_[p].next[c] == q; p = states_[p].link) {
                    states_[p].next[c] = clone;
                }
                states_[q].link = states_[cur].link = clone;
            }
        }
        last_ = cur;
    }

    void DFS(int u) {
        states_[u].cnt = 1;
        for (int i = 0; i < kAlphabetSize; ++i) {
            int v = states_[u].next[i];
            if (v != kNone) {
                if (!states_[v].cnt) {
                    DFS(v);
                }
                states_[u].cnt += states_[v].cnt;
                states_[u].min_excl = std::min(states_[u].min_excl, states_[v].min_excl);
            } else {
                states_[u].min_excl = 0;
            }
        }
        ++states_[u].min_excl;
    }

public:
    SuffixAutomaton(const std::string& s = "") : states_(1) {  // O(|s|)
        for (auto c : s) {
            Extend(c);
        }
        DFS(0);
    }

    bool IsSubstring(const std::string& s) const {
        int u = 0;
        for (auto c : s) {
            int v = states_[u].next[c - 'a'];
            if (v == kNone) {
                return false;
            }
            u = v;
        }
        return true;
    }

    int64_t CountDistinctSubstrings() const {
        return states_[0].cnt - 1;
    }

    auto FindKthSubstring(int64_t k) const {
        std::string res;
        int u = 0;
        while (k) {
            for (int i = 0; i < kAlphabetSize; ++i) {
                int v = states_[u].next[i];
                if (v != kNone) {
                    if (k <= states_[v].cnt) {
                        res.push_back('a' + i);
                        --k;
                        u = v;
                        break;
                    }
                    k -= states_[v].cnt;
                }
            }
        }
        return res;
    }

    auto FindMinCyclicShift(int n) const {
        std::string res;
        int u = 0;
        while (n--) {
            for (int i = 0; i < kAlphabetSize; ++i) {
                int v = states_[u].next[i];
                if (v != kNone) {
                    res.push_back('a' + i);
                    u = v;
                    break;
                }
            }
        }
        return res;
    }

    auto FindMinNotAppearingString() const {
        std::string res;
        int u = 0;
        while (u != kNone) {
            for (int i = 0; i < kAlphabetSize; ++i) {
                int v = states_[u].next[i];
                if (v == kNone || states_[v].min_excl + 1 == states_[u].min_excl) {
                    res.push_back('a' + i);
                    u = v;
                    break;
                }
            }
        }
        std::reverse(res.begin(), res.end());
        return res;
    }

    auto LCS(const std::string& s) const {
        int u = 0, len = 0, n = s.length(), mx_len = 0, imax = 0;
        for (int i = 0; i < n; ++i) {
            char c = s[i] - 'a';
            while (u && states_[u].next[c] == kNone) {
                u = states_[u].link, len = states_[u].len;
            }
            if (states_[u].next[c] != kNone) {
                u = states_[u].next[c], ++len;
            }
            if (len > mx_len) {
                mx_len = len, imax = i;
            }

        }
        return s.substr(imax - mx_len + 1, mx_len);
    }
};

int main() {
    // Case 1: find patterns in the text in O(|T| + |P|)
    {
        int qs;
        std::string text, pattern;
        std::cin >> text >> qs;

        auto sa = SuffixAutomaton(text);

        while (qs--) {
            std::cin >> pattern;
            std::cout << (sa.IsSubstring(pattern) ? "Found\n" : "Not found\n");
        }
        std::cout << '\n';
    }

    // Case 2: find the k-th substring in O(|ans|)
    {
        int qs, k;
        std::string s;
        std::cin >> s >> qs;

        auto sa = SuffixAutomaton(s);

        while (qs--) {
            std::cin >> k;  // >= 1
            std::cout << sa.FindKthSubstring(k) << '\n';
        }
        std::cout << '\n';
    }

    // Case 3: find a minimal cyclic shift in O(|s|)
    {
        std::string s;
        std::cin >> s;

        auto sa = SuffixAutomaton(s + s);

        int n = s.length();
        std::cout << sa.FindMinCyclicShift(n) << "\n\n";
    }

    // Case 4: find the number of distinct substrings in O(|s|)
    {
        std::string s;
        std::cin >> s;

        auto sa = SuffixAutomaton(s);

        std::cout << sa.CountDistinctSubstrings() << "\n\n";
    }

    // Case 5: build the shortest non-appearing substring
    {
        std::string s;
        std::cin >> s;

        auto sa = SuffixAutomaton(s);

        std::cout << sa.FindMinNotAppearingString() << "\n\n";
    }

    // Case 6: find the longest common substring in O(|S| + |T|)
    {
        std::string s, t;
        std::cin >> s >> t;

        auto sa = SuffixAutomaton(s);

        std::cout << sa.LCS(t) << '\n';
    }

    return 0;
}
