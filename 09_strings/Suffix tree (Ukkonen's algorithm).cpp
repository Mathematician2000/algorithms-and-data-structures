#include <array>
#include <iostream>
#include <string>
#include <vector>

constexpr int kNone = -1;

// Ukkonen's algorithm for suffix tree construction
class SuffixTree {
    static constexpr int kMaxSize = 100'000;
    static constexpr int kStarPos = kMaxSize;
    static constexpr int kAlphabetSize = 'z' - 'a' + 1;

    struct Node {
        int left = 0, right = 0;
        int parent = kNone, link = kNone;
        std::array<int, kAlphabetSize> next;

        Node() {
            next.fill(kNone);
        }

        Node(int left, int right, int parent) : left(left), right(right), parent(parent) {
            next.fill(kNone);
        }

        int Length() const {
            return right - left;
        }

        int GetNext(char c) const {
            return next[c - 'a'];
        }

        void SetNext(char c, int idx) {
            next[c - 'a'] = idx;
        }
    };

    struct State {
        int node, shift;

        explicit State(int node = kNone, int shift = 0) : node(node), shift(shift) {
        }
    };

    std::string str_;
    std::vector<Node> nodes_;
    State ptr_;

    State Go(State state, int left, int right) const {
        while (left < right) {
            const Node& node = nodes_[state.node];
            int len = node.Length();
            if (state.shift == len) {
                state = State(node.GetNext(str_[left]));
                if (state.node == kNone) {
                    return state;
                }
            } else if (str_[node.left + state.shift] != str_[left]) {
                return State();
            } else if (state.shift + right - left < len) {
                return State(state.node, state.shift + right - left);
            } else {
                left += len - state.shift;
                state.shift = len;
            }
        }
        return state;
    }

    int Split(const State& state) {
        int idx = state.node;
        if (state.shift == nodes_[idx].Length()) {
            return idx;
        } else if (!state.shift) {
            return nodes_[idx].parent;
        }

        int last = nodes_.size();
        nodes_.emplace_back(nodes_[idx].left, nodes_[idx].left + state.shift, nodes_[idx].parent);
        nodes_[nodes_[idx].parent].SetNext(str_[nodes_[idx].left], last);
        nodes_[last].SetNext(str_[nodes_[idx].left + state.shift], idx);
        nodes_[idx].parent = last;
        nodes_[idx].left += state.shift;
        return last;
    }

    int GetLink(int idx) {
        if (nodes_[idx].link != kNone) {
            return nodes_[idx].link;
        } else if (nodes_[idx].parent == kNone) {
            return 0;
        }

        int moved = GetLink(nodes_[idx].parent);
        nodes_[idx].link = Split(Go(State(moved, nodes_[moved].Length()),
                                    nodes_[idx].left + (!nodes_[idx].parent), nodes_[idx].parent));
        return nodes_[idx].link;
    }

    void AppendChar(int idx) {
        int mid = 1;
        while (mid) {
            State next = Go(ptr_, idx, idx + 1);
            if (next.node != kNone) {
                ptr_ = next;
                return;
            }

            mid = Split(ptr_);
            int last = nodes_.size();
            nodes_.emplace_back(idx, kStarPos, mid);
            nodes_[mid].SetNext(str_[idx], last);

            ptr_.node = GetLink(mid);
            ptr_.shift = nodes_[ptr_.node].Length();
        }
    }

public:
    explicit SuffixTree(int max_size = kMaxSize) : nodes_(1), ptr_(0) {
        nodes_.reserve(max_size);
    }

    // O(|line|)
    void Append(const std::string& line) {
        int idx = str_.length();
        str_ += line;
        for (int j = 0; j < static_cast<int>(line.length()); ++j) {
            AppendChar(idx + j);
        }
    }

    // O(|line|)
    bool Contains(const std::string& line) const {
        State state(0);
        for (auto c : line) {
            int idx = state.node;
            if (state.shift == nodes_[idx].Length()) {
                state = State(nodes_[idx].GetNext(c));
                idx = state.node;
                if (idx == kNone) {
                    return false;
                }
            }

            int pos = nodes_[idx].left + state.shift;
            if (pos >= static_cast<int>(str_.length()) || str_[pos] != c) {
                return false;
            }
            ++state.shift;
        }
        return true;
    }

    const std::string& Print() const {
        return str_;
    }
};

int main() {
    // Case 1: process concat and substr queries
    {
        int qs;
        std::cin >> qs;

        std::string type, str;
        SuffixTree tree;
        while (qs--) {
            std::cin >> type >> str;
            if (type == "substr" || type == "?") {
                std::cout << (tree.Contains(str) ? "Yes\n" : "No\n");
            } else if (type == "concat" || type == "append" || type == "+") {
                tree.Append(str);
            } else {
                std::cout << "Unknown command :(\n";
            }
        }
        std::cout << "Total string: " << tree.Print() << '\n';
    }

    return 0;
}
