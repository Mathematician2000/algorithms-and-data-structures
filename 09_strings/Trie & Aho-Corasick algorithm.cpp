#include <array>
#include <algorithm>
#include <iostream>
#include <vector>

constexpr int ABC = 'z' - 'a' + 1;


// Trie and Aho-Corasick algorithm
class Trie {
    struct Node {
        std::array<int, ABC> next, go;
        std::vector<int> ids;
        int p, link = -1, out = -1;
        bool leaf = false;
        char c;

        Node(int p = -1, char c = 0) : p(p), c(c) {
            next.fill(-1);
            go.fill(-1);
        }

        void AddIndex(int idx) {
            ids.push_back(idx);
        }
    };

    std::vector<Node> trie;
    std::vector<int> lens;
    int sz = 1, total = 0;
public:
    Trie() : trie(1) {}

    void AddString(const std::string& s) {
        int idx = 0;
        for (auto c : s) {
            int j = c - 'a';
            if (trie[idx].next[j] == -1) {
                trie[idx].next[j] = sz++;
                trie.emplace_back(idx, c);
            }
            idx = trie[idx].next[j];
        }
        trie[idx].leaf = true;
        trie[idx].AddIndex(total++);
        lens.push_back(s.length());
    }

    int GetLink(int idx) {
        if (trie[idx].link == -1) {
            if (!idx || !trie[idx].p) {
                trie[idx].link = 0;
            } else {
                trie[idx].link = Go(GetLink(trie[idx].p), trie[idx].c);
            }
        }
        return trie[idx].link;
    }

    int Go(int idx, char c) {
        int j = c - 'a';
        if (trie[idx].go[j] == -1) {
            if (trie[idx].next[j] != -1) {
                trie[idx].go[j] = trie[idx].next[j];
            } else {
                trie[idx].go[j] = (idx ? Go(GetLink(idx), c) : 0);
            }
        }
        return trie[idx].go[j];
    }

    int Out(int idx) {
        if (trie[idx].out == -1) {
            int link = GetLink(idx);
            if (!link) {
                trie[idx].out = 0;
            } else {
                trie[idx].out = (trie[link].leaf ? link : Out(link));
            }
        }
        return trie[idx].out;
    }

    void CheckIndex(int idx, std::vector<std::vector<int>>& pos, int k) {
        for (int link = idx; link; link = Out(link)) {
            if (trie[link].leaf) {
                for (auto i : trie[link].ids) {
                    pos[i].push_back(k - lens[i] + 1);
                }
            }
        }
    }

    auto FindPatterns(const std::string& s) {
        std::vector<std::vector<int>> pos(total);
        int n = s.length(), idx = 0;
        for (int k = 0; k < n; ++k) {
            idx = Go(idx, s[k]);
            CheckIndex(idx, pos, k);
        }
        return pos;
    }

    auto FindMinNotMatching(int n) {
        std::string res;
        if (trie[0].leaf) {
            res = "IMPOSSIBLE";
            return res;
        }
        int u = 0;
        for (int i = 0; i < n; ++i) {
            for (char c = 'a'; c <= 'z'; ++c) {
                int v = Go(u, c);
                if (v != -1 && !trie[v].leaf) {
                    res.push_back(c);
                    u = v;
                    break;
                }
            }
            if (int(res.length()) != i + 1) {
                res = "IMPOSSIBLE";
                return res;
            }
        }
        return res;
    }
};


int main() {
    // Case 1: find all occurrences of patterns in the text in O(n + M)
    {
        int n;
        std::string text, pattern;
        std::cin >> text >> n;
        Trie trie;
        for (int i = 0; i < n; ++i) {
            std::cin >> pattern;
            trie.AddString(pattern);
        }
        auto pos = trie.FindPatterns(text);
        for (int i = 0; i < n; ++i) {
            if (pos[i].empty()) {
                std::cout << "Not found :(\n";
            } else {
                for (auto idx : pos[i]) {
                    std::cout << idx << ' ';
                }
                std::cout << '\n';
            }
        }
        std::cout << '\n';
    }

    // Case 2: find the smallest string of length k not matching with all the patterns in O(k)
    {
        int n, k;
        std::string pattern;
        std::cin >> n >> k;
        Trie trie;
        for (int i = 0; i < n; ++i) {
            std::cin >> pattern;
            trie.AddString(pattern);
        }
        std::cout << trie.FindMinNotMatching(k) << '\n';
    }

    return 0;
}
