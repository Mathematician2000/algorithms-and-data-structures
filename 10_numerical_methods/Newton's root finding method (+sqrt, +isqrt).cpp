#include <algorithm>
#include <functional>
#include <iomanip>
#include <iostream>

constexpr double kEps = 1e-15;

template <typename T>
using Operation = std::function<T(T)>;

// Solve equation f(x) = 0 using Netwon-Raphson method
// PS: x <- x - f(x) / f'(x)
// PPS: quadratic convergence rate
template <typename T>
T NewtonRaphson(const Operation<T>& func, const Operation<T>& deriv, T approx, T eps = kEps) {
    T prev;
    int iters = 0;
    do {
        prev = approx;
        approx -= func(approx) / deriv(approx);
        ++iters;
    } while (std::abs(approx - prev) > eps);
    std::cerr << "Total #iters: " << iters << '\n';
    return approx;
}

// Solve equation f(x) = 0 using Netwon-Raphson method
// PS: x <- x - f(x) / f'(x0)
// PPS: linear convergence rate
template <typename T>
T NewtonRaphson(const Operation<T>& func, T init_deriv, T approx, T eps = kEps) {
    T prev;
    int iters = 0;
    do {
        prev = approx;
        approx -= func(approx) / init_deriv;
        ++iters;
    } while (std::abs(approx - prev) > eps);
    std::cerr << "Total #iters: " << iters << '\n';
    return approx;
}

// Babylonian method for finding square roots
double Sqrt(double x, double eps = kEps) {
    double approx = 1, prev;
    int iters = 0;
    do {
        prev = approx;
        approx = (approx + x / approx) / 2;
        ++iters;
    } while (std::abs(approx - prev) > eps);
    std::cerr << "Total #iters: " << iters << '\n';
    return approx;
}

// Find the largest integer y s.t. y^2 <= x
int64_t Isqrt(int64_t x) {
    int64_t approx = 1, prev = 0;
    bool decreased;
    int iters = 0;
    do {
        decreased = (approx < prev);
        prev = approx;
        approx = (approx + x / approx) / 2;
        ++iters;
    } while (prev != approx && !(approx > prev && decreased));
    std::cerr << "Total #iters: " << iters << '\n';
    return prev;
}

int main() {
    std::cout << std::fixed << std::setprecision(6);

    // Case 1: solve equation f(x) = x^3 + 4x - 3 = 0 on range [0; 1]
    {
        auto func = [](double x) -> double { return (x * x + 4) * x - 3; };
        auto deriv = [](double x) -> double { return 3 * x * x + 4; };

        /* Assumptions:
        * 1) there is exactly one root on [a; b]
        * 2) f' is continuous and non-zero on [a; b]
        */

        double root = NewtonRaphson<double>(func, deriv, 1.);
        std::cout << "Root = " << root << "\n\n";
    }

    // Case 2: solve equation f(x) = x^3 + 4x - 3 = 0 on range [0; 1] using initial derivative approximation
    {
        auto func = [](double x) -> double { return (x * x + 4) * x - 3; };

        double root = NewtonRaphson<double>(func, 7., 1.);  // f'(1) = 7
        std::cout << "Root = " << root << "\n\n";
    }

    // Case 3: find sqrt(x)
    {
        double x;
        std::cin >> x;

        double root = Sqrt(x);
        std::cout << "sqrt(" << x << ") = " << root << "\n\n";
    }

    // Case 4: find isqrt(x)
    {
        int64_t x;
        std::cin >> x;

        int64_t root = Isqrt(x);
        std::cout << "isqrt(" << x << ") = " << root << '\n';
    }

    return 0;
}
