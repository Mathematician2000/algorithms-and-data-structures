#include <algorithm>
#include <cmath>
#include <functional>
#include <iomanip>
#include <iostream>

template <typename T>
using Operation = std::function<T(T)>;

// Numerical integraion using rectangle quadrature rule
template <typename T>
T RectangleRule(const Operation<T>& func, T left, T right, int n = 100) {
    T sum = {};
    T h = (right - left) / n;
    for (int i = 0; i < n; ++i) {
        sum += func(left + (i + 0.5) * h);
    }
    sum *= h;
    return sum;
}

// Numerical integraion using trapezoidal quadrature rule
template <typename T>
T TrapezoidalRule(const Operation<T>& func, T left, T right, int n = 100) {
    T sum = (func(left) + func(right)) / 2;
    T h = (right - left) / n;
    for (int i = 1; i < n - 1; ++i) {
        sum += func(left + i * h);
    }
    sum *= h;
    return sum;
}

// Numerical integraion using parabola (Simpson's) quadrature rule
template <typename T>
T SimpsonRule(const Operation<T>& func, T left, T right, int n = 100) {
    T sum = func(left) + func(right);
    T h = (right - left) / (2 * n);
    for (int i = 1; i < 2 * n; ++i) {
        sum += func(left + i * h) * ((i & 1) ? 4 : 2);
    }
    sum *= h / 3;
    return sum;
}

int main() {
    std::cout << std::fixed << std::setprecision(6);

    // Case 1: integrate f(x) = 5x * exp(-2x) over [0.1; 1.3] using rectangle rule
    {
        auto func = [](double x) -> double { return 5 * x * exp(-2 * x); };

        double integral = RectangleRule<double>(func, 0.1, 1.3, 5);  // 5 segments
        std::cout << "(Rectangle) integral = " << integral << "\n\n";
    }

    // Case 2: integrate f(x) = 5x * exp(-2x) over [0.1; 1.3] using trapezoidal rule
    {
        auto func = [](double x) -> double { return 5 * x * exp(-2 * x); };

        double integral = TrapezoidalRule<double>(func, 0.1, 1.3, 5);  // 5 segments
        std::cout << "(Trapezoidal) integral = " << integral << "\n\n";
    }

    // Case 3: integrate f(x) = 5x * exp(-2x) over [0.1; 1.3] using Simpson rule
    {
        auto func = [](double x) -> double { return 5 * x * exp(-2 * x); };

        double integral = SimpsonRule<double>(func, 0.1, 1.3, 5);  // 5 segments
        std::cout << "(Simpson) integral = " << integral << '\n';
    }

    return 0;
}
