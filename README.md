# Algorithms and Data Structures
Implementation of simple algorithms and data structures used in competitive programming contests.

Topics covered:
- Number theory
- Combinatorics
- Sorting and search
- Dynamic programming
- Geometry
- Data structures
- Graphs
- Trees
- Strings
